//this file looks like plain C, but it's actually -*- c++ -*-
#ifndef __TQAuxelementLorentzVectorDECORATIONOBSERVABLE__
#define __TQAuxelementLorentzVectorDECORATIONOBSERVABLE__
#include "CAFxAODUtils/TQEventObservable.h"

#ifndef __CINT__
#include "AthContainers/AuxElement.h"
#endif

class TQSGAuxElementLorentzVectorDecorationObservable : public TQEventObservable { // linkalltemplates
protected:
  enum LVelement {
    PT, E, M, ETA, Y, PX, PY, PZ, PHI
  };
  
  TString fExpression = "";
  TString fContainerName = ""; //!
  TString fDecoration = ""; //!
  LVelement fVectorElement; //!
  mutable TString fCachedExpression; //!
  TString fCachedContainerName = ""; //!
#ifndef __CINT__
  mutable SG::AuxElement const * mContainer = 0;
#endif

  void clearParsedExpression();
  bool parseExpression(const TString& expr);

public:
  virtual double getValue() const override;
  virtual bool hasExpression() const override;
  virtual const TString& getExpression() const override;
  virtual TString getActiveExpression() const override;
  virtual void setExpression(const TString& expr) override;
  TString getVectorElementString() const;
  
  
  virtual bool initializeSelf() override;
  virtual bool finalizeSelf() override;

  TQSGAuxElementLorentzVectorDecorationObservable();
  TQSGAuxElementLorentzVectorDecorationObservable(const TString& expression);
  virtual ~TQSGAuxElementLorentzVectorDecorationObservable();

  DECLARE_OBSERVABLE_FACTORY(TQSGAuxElementLorentzVectorDecorationObservable,TString expression);
  
  ClassDef(TQSGAuxElementLorentzVectorDecorationObservable, 0)
};

#include "CAFxAODUtils/Observables.h"

#endif

#include "CAFxAODUtils/TQSGAuxElementLorentzVectorDecorationObservable.h"
#include <limits>
#include <sstream>
#include <stdexcept>
#include "TLorentzVector.h"
#include "TMath.h"
#include <Math/GenVector/LorentzVector.h>
#include <Math/GenVector/PtEtaPhiE4D.h>

#include "QFramework/TQSample.h"

//#define _DEBUG_
#include "QFramework/TQLibrary.h"


/*@observable: [TQSGAuxElementLorentzVectorDecorationObservable] The TQSGAuxElementLorentzVectorDecorationObservable can be used to read the value of decorations of SG::AuxElement objects (such as the EventInfo object),
  It specifically uses decorations of type TLorentzVector. The element of the TLorentzVector can be accessed with a specific flag.  Expressions like 'SGAuxLVDec:nameOfAuxElement:nameOfDecoration:vectorElement' are parsed, where 'vectorElement' can be one of 'pt', 'e', 'eta', 'y', 'px', 'py', 'pz', 'm'
*/


ClassImp(TQSGAuxElementLorentzVectorDecorationObservable)

//______________________________________________________________________________________________

TQSGAuxElementLorentzVectorDecorationObservable::TQSGAuxElementLorentzVectorDecorationObservable(){
  // default constructor
  DEBUGclass("default constructor called");
}

//______________________________________________________________________________________________

TQSGAuxElementLorentzVectorDecorationObservable::~TQSGAuxElementLorentzVectorDecorationObservable(){
  // default destructor
  DEBUGclass("destructor called");
}

//______________________________________________________________________________________________


double TQSGAuxElementLorentzVectorDecorationObservable::getValue() const {
  // value retrieval function, called on every event for every cut and histogram
  DEBUGclass("entering function");

  this->retrieve(this->mContainer, this->fContainerName.Data());
  if (!mContainer) {
    ERRORclass("container '%s' not found",this->fContainerName.Data());
    return std::numeric_limits<double>::quiet_NaN();
  }

  DEBUGclass("reading aux-data (object-level)");
  if (mContainer->isAvailable< ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double>> >(fDecoration.Data())) {
    const auto vec = mContainer->auxdata< ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double>> >(fDecoration.Data());
    switch ( fVectorElement ){
    case LVelement::PT:
      return vec.Pt();
    case LVelement::E:
      return vec.E();
    case LVelement::M:
      return vec.M();
    case LVelement::ETA:
      return vec.Eta();
    case LVelement::Y:
      return vec.Y();      
    case LVelement::PX:
      return vec.Px();
    case LVelement::PY:
      return vec.Py();
    case LVelement::PZ:
      return vec.Pz();
    case LVelement::PHI:
      return vec.Phi();
    }
  }
  
  ERRORclass("%s:%s unavailable - returning NaN",this->fContainerName.Data(),this->fDecoration.Data());
  return std::numeric_limits<double>::quiet_NaN();
}
//______________________________________________________________________________________________

TQSGAuxElementLorentzVectorDecorationObservable::TQSGAuxElementLorentzVectorDecorationObservable(const TString& expression):
TQEventObservable(expression)
{
  // constructor with expression argument
  DEBUGclass("constructor called with '%s'",expression.Data());
  this->setExpression(expression);
  this->SetName(TQObservable::makeObservableName(this->getExpression()));
}

//______________________________________________________________________________________________

const TString& TQSGAuxElementLorentzVectorDecorationObservable::getExpression() const {
  return this->fExpression;
}

//______________________________________________________________________________________________

TString TQSGAuxElementLorentzVectorDecorationObservable::getActiveExpression() const {
  // retrieve the expression associated with this observable
  if(this->fCachedExpression.IsNull()){
    this->fCachedExpression = TString::Format("SGAuxLVDec:%s",this->fContainerName.Data());
    this->fCachedExpression.Append(":");
    this->fCachedExpression.Append(this->fDecoration);
    this->fCachedExpression.Append(":");
    this->fCachedExpression.Append(this->getVectorElementString());
  }
  return this->fCachedExpression;
}

//______________________________________________________________________________________________

bool TQSGAuxElementLorentzVectorDecorationObservable::hasExpression() const {
  // check if this observable type knows expressions
  return true;
}

//______________________________________________________________________________________________

bool TQSGAuxElementLorentzVectorDecorationObservable::initializeSelf(){
  // initialize self - compile container name, construct accessor
  if(!this->parseExpression(TQObservable::compileExpression(this->fExpression,this->fSample))){
    return false;
  }
  return true;
}

//______________________________________________________________________________________________

bool TQSGAuxElementLorentzVectorDecorationObservable::finalizeSelf(){
  // finalize self - delete accessor
  this->clearParsedExpression();
  return true;
}

//______________________________________________________________________________________________

void TQSGAuxElementLorentzVectorDecorationObservable::clearParsedExpression(){
  // clear the current expression
  this->fContainerName.Clear();
  this->fDecoration.Clear();
  this->fCachedExpression.Clear();
}

//______________________________________________________________________________________________

TString TQSGAuxElementLorentzVectorDecorationObservable::getVectorElementString() const {
  switch ( fVectorElement ){
  case LVelement::PT:
    return "pt";
  case LVelement::E:
    return "e";
  case LVelement::M:
    return "m";
  case LVelement::ETA:
    return "eta";
  case LVelement::Y:
    return "y";
  case LVelement::PX:
    return "px";
  case LVelement::PY:
    return "py";
  case LVelement::PZ:
    return "pz";
  case LVelement::PHI:
    return "phi";
  }
  return "UNKNOWN";
}

//______________________________________________________________________________________________

void TQSGAuxElementLorentzVectorDecorationObservable::setExpression(const TString& expr){
  this->fExpression = expr;
}

//______________________________________________________________________________________________

bool TQSGAuxElementLorentzVectorDecorationObservable::parseExpression(const TString& expr){
  TString input(expr);
  // set the expression to a given string
  this->clearParsedExpression();
  if(!TQStringUtils::readToken(input,this->fContainerName,TQStringUtils::alphanumvarext)){
    ERRORclass("unable to read container name from expression '%s'",input.Data());
    ERRORclass("unable to parse expression '%s'",expr.Data());
    return false;
  }
  if(TQStringUtils::removeLeading(input,":") != 1){
    ERRORclass("missing auxdata declaration in expression",expr.Data());
    return false;
  }
  if(!TQStringUtils::readToken(input,this->fDecoration,TQStringUtils::alphanumvarext)){
    ERRORclass("unable to read decoration name from expression '%s'",input.Data());
    ERRORclass("unable to parse expression '%s'",expr.Data());
    return false;
  }
  if(TQStringUtils::removeLeading(input,":") != 1){
    ERRORclass("missing vector element declaration in expression '%s'",expr.Data());
    return false;
  }
  input.ToLower();
  if(input == "pt"){
    this->fVectorElement = LVelement::PT;
  } else if(input ==  "e"){
    this->fVectorElement = LVelement::E;
  } else if(input ==  "m"){
    this->fVectorElement = LVelement::M;
  } else if(input ==  "eta"){
    this->fVectorElement = LVelement::ETA;
  } else if(input ==  "y"){
    this->fVectorElement = LVelement::Y;
  } else if(input == "px"){
    this->fVectorElement = LVelement::PX;
  } else if(input == "py"){
    this->fVectorElement = LVelement::PY;
  } else if(input == "pz"){
    this->fVectorElement = LVelement::PZ;
  } else if(input == "e"){
    this->fVectorElement = LVelement::M;
  } else {
    ERRORclass("failed to parse vector element from '%s'",input.Data());
    return false;
  }
      
  DEBUGclass("Initialized TQSGAuxElementLorentzVectorDecorationObservable with : %s, %s, %s",this->fContainerName.Data(),this->fDecoration.Data(), this->getVectorElementString());
  this->getExpression();
  return true;
}

#include "CAFxAODUtils/Observables.h"

DEFINE_OBSERVABLE_FACTORY(TQSGAuxElementLorentzVectorDecorationObservable,TString expression){
  // a factory for this observable type
  if(TQStringUtils::removeLeadingText(expression,"SGAuxLVDec:")){
    return new TQSGAuxElementLorentzVectorDecorationObservable(expression);
  }
  return NULL;
}


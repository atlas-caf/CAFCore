#ifndef __CAFxAODUtilsDICT__
#define __CAFxAODUtilsDICT__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Winconsistent-missing-override"
#include "CAFxAODUtils/Observables.h"
#include "CAFxAODUtils/TQCompositorObservable.h"
#include "CAFxAODUtils/TQContainerSizeObservable.h"
#include "CAFxAODUtils/TQCPDecorationObservable.h"
#include "CAFxAODUtils/TQCPConstituentVectorObservable.h"
#include "CAFxAODUtils/TQEventFlaggingAnalysisJob.h"
#include "CAFxAODUtils/TQEventObservable.h"
#include "CAFxAODUtils/TQIParticleDecorationObservable.h"
#include "CAFxAODUtils/TQIParticleDecorationVectorObservable.h"
#include "CAFxAODUtils/TQIParticleFourVectorObservable.h"
#include "CAFxAODUtils/TQIParticleFourVectorVectorObservable.h"
#include "CAFxAODUtils/TQTruthParticleInfoObservable.h"
#include "CAFxAODUtils/TQSGAuxElementDecorationObservable.h"
#include "CAFxAODUtils/TQSGAuxElementLorentzVectorDecorationObservable.h"
#include "CAFxAODUtils/TQSGAuxFlagCheckingObservable.h"
#include "CAFxAODUtils/TQToolObservable.h"
#include "CAFxAODUtils/TQxAODskimmingAlgorithm.h"
#include "CAFxAODUtils/TQxAODUtils.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ class TQCompositorObservable+;
#pragma link C++ class TQContainerSizeObservable+;
#pragma link C++ class TQEventFlaggingAnalysisJob+;
#pragma link C++ class TQEventObservable+;
#pragma link C++ class TQIParticleFourVectorObservable+;
#pragma link C++ class TQIParticleFourVectorVectorObservable+;
#pragma link C++ class TQTruthParticleInfoObservable+;
#pragma link C++ class TQToolObservable+;
#pragma link C++ class TQxAODskimmingAlgorithm+;
#pragma link C++ class TQCPConstituentVectorObservable+;
#pragma link C++ class TQCPDecorationObservable <float>+;
#pragma link C++ class TQCPDecorationObservable <double>+;
#pragma link C++ class TQCPDecorationObservable <char>+;
#pragma link C++ class TQCPDecorationObservable <unsigned int>+;
#pragma link C++ class TQCPDecorationObservable <unsigned short>+;
#pragma link C++ class TQCPDecorationObservable <unsigned long long>+;
#pragma link C++ class TQCPDecorationObservable <int>+;
#pragma link C++ class TQIParticleDecorationObservable <float>+;
#pragma link C++ class TQIParticleDecorationObservable <double>+;
#pragma link C++ class TQIParticleDecorationObservable <char>+;
#pragma link C++ class TQIParticleDecorationObservable <unsigned int>+;
#pragma link C++ class TQIParticleDecorationObservable <unsigned short>+;
#pragma link C++ class TQIParticleDecorationObservable <unsigned long long>+;
#pragma link C++ class TQIParticleDecorationObservable <int>+;
#pragma link C++ class TQIParticleDecorationVectorObservable <float>+;
#pragma link C++ class TQIParticleDecorationVectorObservable <double>+;
#pragma link C++ class TQIParticleDecorationVectorObservable <char>+;
#pragma link C++ class TQIParticleDecorationVectorObservable <unsigned int>+;
#pragma link C++ class TQIParticleDecorationVectorObservable <unsigned short>+;
#pragma link C++ class TQIParticleDecorationVectorObservable <unsigned long long>+;
#pragma link C++ class TQIParticleDecorationVectorObservable <int>+;
#pragma link C++ class TQSGAuxElementLorentzVectorDecorationObservable+;
#pragma link C++ class TQSGAuxElementDecorationObservable <float>+;
#pragma link C++ class TQSGAuxElementDecorationObservable <double>+;
#pragma link C++ class TQSGAuxElementDecorationObservable <char>+;
#pragma link C++ class TQSGAuxElementDecorationObservable <unsigned int>+;
#pragma link C++ class TQSGAuxElementDecorationObservable <unsigned short>+;
#pragma link C++ class TQSGAuxElementDecorationObservable <unsigned long long>+;
#pragma link C++ class TQSGAuxElementDecorationObservable <int>+;
#pragma link C++ class TQSGAuxFlagCheckingObservable <float>+;
#pragma link C++ class TQSGAuxFlagCheckingObservable <double>+;
#pragma link C++ class TQSGAuxFlagCheckingObservable <char>+;
#pragma link C++ class TQSGAuxFlagCheckingObservable <unsigned int>+;
#pragma link C++ class TQSGAuxFlagCheckingObservable <unsigned short>+;
#pragma link C++ class TQSGAuxFlagCheckingObservable <unsigned long long>+;
#pragma link C++ class TQSGAuxFlagCheckingObservable <int>+;
#pragma link C++ namespace TQObservableFactory;
#pragma link C++ namespace TQxAODUtils;
#pragma link C++ namespace asg;

#endif
#endif


import os
import math
#from ROOT import TFile
def getSubSamples(sampleFolders):
  allSamplesForSubjob = []
  for thisSF in sampleFolders:
    allSamplesForSubjob = allSamplesForSubjob + [x for x in thisSF.getListOfSamples() if not x.hasSubSamples()] #this could, in theory, contain duplicates, we'll resovle this later by converting the list to a set
    #the following lines efficiently remove duplicates whilst preserving the order. This is important since otherwise the creation of tasklists is not fully deterministic!!!
    alreadyAddedTemp = set()
    alreadyAddedTemp_add = alreadyAddedTemp.add #saves time w.r.t. resolution for each element
  return [x for x in allSamplesForSubjob if not (x in alreadyAddedTemp or alreadyAddedTemp_add(x))]

def makeSmartTaskList(args_, jobFile,config ,templateCommand, maxSampleCount=-1, maxSampleSize=-1, maxEventCount=0, setup=[], idOffset=0, inputs=[], outputs=[]):
  """
  Creates a list of tasks based on a SampleFolder (determined from config argument, expected format is a TQFolder as obtained from common.getConfigOptions) taking boundary conditions from a job definition file (joblist) into account

  The payload of the created tasks is created from the templateCommand replacing
  occurances of the substrings '{restrict}' and '{identifier}' with the SampleFolder
  path(s) the job should be restricted to and a corresponding identifier which can
  be used in file names. Example:
  templateCommand = "analyze.py flatNtuple/config/master/analyze.cfg --retrict {restrict} --options outputFile:analyzed/toMerge/samples_unmerged_{identifier}.root"

  Additional replacements that can be used:
  -'{uniqueID}': a number which is unique within the list of tasks generated during a single call (starting at idOffset (=0 unless specified as argument to this function) and incremented for each subsequent task created)
  -'{options}' : if the args namespace has an attribute 'options' this placeholder is replaced by the concatenation of the options attribute of the args namespace. The individual elements are concatenated with single spaces ' '.

  The optional arguments 'inputs' and 'outputs' are expected to be lists of input (config) files each individual job reads and output files it is expected to produce. The same replacements as in the templateCommand case are applied (to each element of the lists).

  Optionally a list of setup instructions can be provided which will be set on each individual task. Also for the elements of the setup list above mentioned replacements are applied.

  The joblist file is expected to contain one SampleFolder path per line, tasks will be created within
  the domain of that path, possibly splitting into multiple tasks (TODO: use taskArrays instead) to
  respect the maxSampleCount and maxSampleSize arguments if they are > 0. maxSampleCount refers to a maximum number of
  input files, maxSampleSize to a maximum cumulated size of input files for a single task.

  The file size of a sample is read from tags '.init.filestamp.size' and (if the first is not found) '.xsp.fileSize', defaulting to 0.

  ATTENTION: this function assumes that whereever a wildcard is used in the job definition file, the corresponding parts of the SampleFolder are also marked as wildcarded with the tag 'wildcarded' set to true!

  """
  import pickle
  import QFramework
  import CommonAnalysisHelpers as CAH
  from SubmissionHelpers.submit import adjustArgs
  sf = CAH.common.loadSampleFolder(config)
  if not sf:
    QFramework.BREAK("unable to load sample folder '{:s}' to create smart job list - please check input path".format(inputFileName))
  # patch sample folder so that the same sample folder in the submitted job has the same format as the sample folder while creating the task list
  CAH.common.patchSampleFolder(config.getTagVStandardString("patches"), sf)
  #make a deep-ish copy of args_ (as it might be modified further down the line)
  from argparse import Namespace
  args = Namespace(**vars(args_))

  listFileName = f"task_list_{args.identifier}.pkl"
  

  retList = []
  allPathsWildcarded = [] #list to keep track of what contribution is already used somewhere to prevent double counting/running
  if jobFile:
      joblist = []
      with open(QFramework.TQPathManager.findFileFromEnvVar(jobFile, "CAFANALYSISSHARE")) as f:
          joblist_ = f.readlines()
          joblist = [x.strip() for x in joblist_]
  else:
      joblist = ["?"]
  groupFriendsWildcarded = True # do we want to group samples using wildcards?

  # load previously saved list, if available and up to date
  if (os.path.isfile(listFileName)
      and (not jobFile or
           (os.path.getmtime(listFileName)
            > os.path.getmtime(QFramework.TQPathManager.findFileFromEnvVar(jobFile, "CAFANALYSISSHARE"))))):
    QFramework.INFO(f"Loading task list from {listFileName}")
    with open(listFileName,"rb") as listFile:
      return pickle.load(listFile)

  try:
    options = args.options
  except:
    options = []
  options.append("prettyPrint=false")
  options.append("lineUpdates=false")

  for jobElement in joblist:
    jobdefs = []
    #remove/skip comment lines:
    if jobElement == "" or jobElement[0]=="#": continue
    modline = jobElement.lstrip('!') #remove leading '!' character
    if len(modline) < len(jobElement): #we actually removed at least one leading '!'
      #hence, we adjust the current (local copy of the) argparse namespace
      if not args.allowArgChanges: continue
      (key,val) = modline.split("=",1)
      key = key.strip()
      val = val.strip()
      #we treat the following two cases differently as they are not part of the minimal argparser
      if key == "maxSampleCount":
        maxSampleCount = int(val)
      elif key == "maxSampleSize":
        maxSampleSize = float(val)
      elif key == "maxEventCount":
        maxEventCount =  int(float(val))
      elif key == "groupFriendsWildcarded":
        groupFriendsWildcarded = bool(int(val))
      else:
        adjustArgs(args,modline)
    else: #not a modline, so consider it as a job definition
      sampleFolders = sf.getListOfSampleFolders(jobElement)
      if not sampleFolders:
        QFramework.BREAK("No matching sample folders found for expression '{:s}', exiting".format(jobElement))

      allSamplesForSubjob = getSubSamples(sampleFolders)
      label=args.identifier+"_"+ ( jobElement.strip("/").replace("/","_").replace("?","X").replace(",","_") )
      if maxEventCount > 0:
        part = 0
        for sample in allSamplesForSubjob:
          if not sample: continue
          #nEvents = sample.getTagIntegerDefault(".init.nEvents",0) --> this is the DAOD-level number of events
          nEvents = sample.getTagIntegerDefault(".init.nEntries",0)
          #infile = TFile.Open(str(sample.getTagStringDefault(".init.filepath","")),"READ")
          #tree =infile.Get(str(sample.getTagStringDefault(".init.treename","")))
          #nEvents =tree.GetEntries()

          pathWildcarded = sample.getPathFriendsWildcarded(not groupFriendsWildcarded).Data().strip()
          if pathWildcarded in allPathsWildcarded: continue #check if the path is already used somewhere and skip it if needed to prevent double counting
          allPathsWildcarded.append(pathWildcarded)
          njobs = int(float(nEvents) / maxEventCount)+1
          bounds = [ maxEventCount * i for i in range(0,njobs) ]
          #no need to skip nEvents, beause there is zero event left for analyze
          #if bounds[-1] < nEvents:
          #  bounds.append(nEvents)
          for s in bounds:
            newjob = {"restrict":pathWildcarded,"label":label+".part{:d}".format(part),"options":["skipEvents={:d}".format(s),"maxEvents={:d}".format(maxEventCount)]}
            part = part+1
            jobdefs.append(newjob)
      else:
        localPaths = [[]] #sublists will contain all paths for one subjob
        localCount = 0
        localSize = 0.
        for sample in allSamplesForSubjob:
          if not sample: continue
          pathWildcarded = sample.getPathFriendsWildcarded(not groupFriendsWildcarded).Data().strip()
          if pathWildcarded in allPathsWildcarded: continue #check if the path is already used somewhere and skip it if needed to prevent double counting
          sampleSize = sample.getTagDoubleDefault(".init.filestamp.size",sample.getTagDoubleDefault(".xsp.fileSize",0.))
          if ( (maxSampleSize>0 and localSize+sampleSize>maxSampleSize) or (maxSampleCount>0 and localCount+1>maxSampleCount)  ): #check if we need to create a new batch of samples or we can add it to the current one
            if (len(localPaths[-1]) >0): #don't do anything if this is simply a single sample already exceeding the limit
              localPaths.append([])
              localSize = 0.
              localCount = 0

          allPathsWildcarded.append(pathWildcarded) #we are using this path now so we add it to the used paths
          localPaths[-1].append(pathWildcarded)
          localSize += sampleSize
          localCount += 1

        nPart = 0
        for pList in localPaths:
          if len(pList)==0: continue
          #combine the sample paths into --restrict arguments
          restrict = ",".join(pList)
          partLabel = label+(".part"+str(nPart) if len(localPaths)>1 else "")

          jobdefs.append({"restrict":restrict,"label":partLabel})
          nPart += 1 #id within this (sub)set of tasks

      for jobdef in jobdefs:
        if not isinstance(templateCommand,list): #turn into a list if this is not the case yet
          templateCommand = [templateCommand]

        thisOptions = [ o for o in options ]
        if "options" in jobdef.keys():
          thisOptions += jobdef["options"]
        optionstring = " ".join(thisOptions)

        payload = [x.format( identifier = jobdef["label"],
                                          uniqueID = idOffset,
                                          restrict = jobdef["restrict"],
                                          options = optionstring )
                   for x in templateCommand] #note: this also ensures we have a copy of the payload list and don't run into troubles with the mutable nature of lists!
        theseOutputs = [x.format( identifier = jobdef["label"],
                                          uniqueID = idOffset,
                                          restrict = jobdef["restrict"],
                                          options = optionstring )
                   for x in outputs] #note: this also ensures we have a copy of the payload list and don't run into troubles with the mutable nature of lists!
        theseInputs = [x.format( identifier = jobdef["label"],
                                          uniqueID = idOffset,
                                          restrict = jobdef["restrict"],
                                          options = optionstring )
                   for x in inputs] #note: this also ensures we have a copy of the payload list and don't run into troubles with the mutable nature of lists!
        thisSetup = [x.format( identifier = jobdef["label"],
                                          uniqueID = idOffset,
                                          restrict = jobdef["restrict"],
                                          options = optionstring )
                   for x in setup] #note: this also ensures we have a copy of the payload list and don't run into troubles with the mutable nature of lists!
        from os.path import join as pjoin
        logFile = pjoin(args.logpath,jobdef["label"]+".log")
        from SubmissionHelpers.task import task
        thisTask = task(jobdef["label"],payload, setup=thisSetup, args=args, inputs=theseInputs, outputs=theseOutputs, logFile=logFile, errFile=logFile)
        retList.append(thisTask)
        #perform some increments
        idOffset += 1 #global id for full list of tasks

  with open(listFileName,"wb") as listFile:
    pickle.dump(retList, listFile)
  
  return retList


def makeTaskList(args_, jobFile,config ,templateCommand, setup=[], idOffset=0, inputs=[], outputs=[]):
  """
  Creates a list of tasks based on a job definition file (joblist).
  One job will be scheduled for each line of the joblist file.
  Please refer to makeSmartTaskList for further documentation.
  """
  import QFramework
  import CommonAnalysisHelpers as CAH
  from SubmissionHelpers.submit import adjustArgs
  sf = CAH.common.loadSampleFolder(config)
  if not sf:
    QFramework.BREAK("unable to load sample folder '{:s}' to create smart job list - please check input path".format(inputFileName))
  #make a deep-ish copy of args_ (as it might be modified further down the line)
  from argparse import Namespace
  args = Namespace(**vars(args_))

  retList = []
  joblist = []
  with open(QFramework.TQPathManager.findFileFromEnvVar(jobFile, "CAFANALYSISSHARE")) as f:
    joblist_ = f.readlines()
    joblist = [x.strip() for x in joblist_]
  for jobElement in joblist:
    #remove/skip comment lines:
    if jobElement == "" or jobElement[0]=="#": continue

    modline = jobElement.lstrip('!') #remove leading '!' character
    if len(modline) < len(jobElement): #we actually removed at least one leading '!'
      #hence, we adjust the current (local copy of the) argparse namespace
      if not args.allowArgChanges: continue
      (key,val) = modline.split("=",1)
      key = key.strip()
      val = val.strip()
      #we treat the following two cases differently as they are not part of the minimal argparser
      adjustArgs(args,modline)
    else: #not a modline, so consider it as a job definition
      localPaths = [[]] #sublists will contain all paths for one subjob

      label=args.identifier+"_"+ ( jobElement.strip("/").replace("/","_").replace("?","X").replace(",","_") )
      #combine the sample paths into --restrict arguments

      try:
        options = args.options
      except:
        options = []
      options.append("prettyPrint=false")
      options.append("lineUpdates=false")

      optionstring = " ".join(options)

      # Simply add the job
      pList = [jobElement]
      restrict = ",".join(pList)

      if not isinstance(templateCommand,list):#turn into a list if this is not the case yet
        templateCommand = [templateCommand]
      partLabel = label
      payload = [x.format( identifier = partLabel,
                                        uniqueID = idOffset,
                                        restrict = restrict,
                                        options = optionstring )
                 for x in templateCommand] #note: this also ensures we have a copy of the payload list and don't run into troubles with the mutable nature of lists!
      theseOutputs = [x.format( identifier = partLabel,
                                        uniqueID = idOffset,
                                        restrict = restrict,
                                        options = optionstring )
                 for x in outputs] #note: this also ensures we have a copy of the payload list and don't run into troubles with the mutable nature of lists!
      theseInputs = [x.format( identifier = partLabel,
                                        uniqueID = idOffset,
                                        restrict = restrict,
                                        options = optionstring )
                 for x in inputs] #note: this also ensures we have a copy of the payload list and don't run into troubles with the mutable nature of lists!
      thisSetup = [x.format( identifier = partLabel,
                                        uniqueID = idOffset,
                                        restrict = restrict,
                                        options = optionstring )
                 for x in setup] #note: this also ensures we have a copy of the payload list and don't run into troubles with the mutable nature of lists!
      from os.path import join as pjoin
      logFile = pjoin(args.logpath,partLabel+".log")
      from SubmissionHelpers.task import task
      thisTask = task(partLabel,payload, setup=thisSetup, args=args, inputs=theseInputs, outputs=theseOutputs, logFile=logFile, errFile=logFile)
      retList.append(thisTask)
      #perform some increments
      idOffset += 1 #global id for full list of tasks
  return retList


def makePreMergeTaskList(args, preMergeDir, setup):
  from SubmissionHelpers.task import task
  """configure the set of merge tasks to be submitted"""
  inputFiles = []
  for thisInput in args.inputs:
    if os.path.isfile(thisInput):
      inputFiles.append(os.path.abspath(thisInput))
    elif os.path.isdir(thisInput):
      for r, d, f in os.walk(thisInput):
        for thisFile in f:
          inputFiles.append(os.path.join(os.path.abspath(thisInput), thisFile))
    else:
      print("Ignoring input %s, which appears to be neither a file nor a directory" %  thisInput)

  # Set number of files per premerge job to sqrt(total nFiles) if not user configured
  if args.maxFiles == -1: args.maxFiles = int(math.ceil(math.sqrt(len(inputFiles))))

  # Assign input files to pre-merge jobs
  jobFilesDict = {}
  assignedFiles = 0
  nParts=int(math.ceil(float(len(inputFiles))/args.maxFiles))
  for thisFile in inputFiles:
    nPart = int(assignedFiles)%nParts
    partLabel = args.identifier+".part"+str(nPart)
    if not partLabel in jobFilesDict.keys(): jobFilesDict[partLabel]=[]
    jobFilesDict[partLabel].append(thisFile)
    assignedFiles+=1

  # Configure premerge jobs and add them to list
  retList = []

  for key in jobFilesDict.keys():
    preMergedName = preMergeDir+'/'+key+'.root'
    payload = 'tqmerge '
    for thisFile in jobFilesDict[key]: payload=payload+thisFile+" "
    payload = payload+ '--output '+ preMergedName
    if(args.name):
      payload = payload+ ' -n ' + args.name
    if (args.traceid):
      payload = payload+ ' -t ' + args.traceid
    if(args.downmerge):
      payload = payload+ ' -m ' + args.downmerge
    if (len(args.patch) > 0):
      payload = payload+ ' -p '+' -p '.join(args.patch)
    if(args.sfname):
      payload = payload+ ' -s ' + args.sfname
    if(args.depth):
      payload = payload+ ' -d ' + str(args.depth)
    if(args.sum):
      payload = payload+ ' -Sum '
    if(args.quiet):
      payload = payload+ ' -q '
    if(args.verbose):
      payload = payload+ ' -v '

    logFile = os.path.join(args.logpath,key+".log")
    thisTask = task(key,payload, setup=setup, memory=args.memory, queue=args.queue, args=args, time=args.time, inputs=[], outputs=[preMergedName], logFile=logFile, errFile=logFile)
    retList.append(thisTask)

  return retList

def makeFinalMergeTask(args, retList, setup):
  from SubmissionHelpers.task import task
  """Configure the final merge job, which combines the premerged files, and add it to the list"""
  preMergedNames = []
  for thisTask in retList:
    preMergedNames.append(thisTask.output[0])
  payload = 'tqmerge '
  for thisPreMergedName in preMergedNames: payload=payload+thisPreMergedName+" "
  payload = payload+ '--output '+ os.path.realpath(args.output) + ' '
  if (args.traceid): payload = payload+ ' -t ' + args.traceid
  logFile = os.path.join(args.logpath,args.identifier+".final.log")
  finalTask = task(args.identifier+".final",payload, setup=setup, args=args, memory=args.memoryFinal, time=args.time, queue=args.queue, inputs=preMergedNames, outputs=[os.path.realpath(args.output)], dependencies=retList, logFile=logFile, errFile=logFile)

  return finalTask

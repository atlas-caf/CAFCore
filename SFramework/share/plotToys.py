#!/bin/env python

def main(args):
    import SFramework
    import ROOT
    ROOT.gROOT.SetBatch(True)
    files = ROOT.vector(ROOT.string)()
    for file in args.files:
        files.push_back(file)
    results = SFramework.TSUtils.collectToyLimits(files)

    print("Computed upper limit: {:g}".format(args.scale*results.UpperLimit()))

    print("Expected upper limits:")
    for arg,label in [(0,"median"),(1,"+1sig"),(-1,"-1sig"),(2,"+2sig"),(-2,"-2sig")]:
        print("  {:s}: {:g}".format(label,args.scale*results.GetExpectedUpperLimit(arg)))

    plot = ROOT.RooStats.HypoTestInverterPlot("HypoTestInverterPlot","",results)
    canvas = ROOT.TCanvas("controlplot","controlplot");
    plot.Draw("CLb 2CL");
    canvas.SaveAs(args.output)


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="make control plots from toys")
    parser.add_argument("files",nargs="+")
    parser.add_argument("-o","--output",default="Clb2CL.pdf",help="the name of the output file")
    parser.add_argument("--scale",default=1,help="scale the printed limits by some number (e.g. the expected cross-section if you are fitting mu)",type=float)
    args = parser.parse_args()
    main(args)

#!/bin/env python

def main(args):
    import QFramework,SFramework
    from os.path import join as pjoin

    model = QFramework.TQFolder.loadFolder(args.input)
    if args.histograms:
        model.setTagString(".HistogramFile",args.histograms)
    
    factory = SFramework.TSModelFactory()
    factory.writeHistograms(model,model.getTagStringDefault(".HistogramFile",pjoin(args.output,"histograms.root")))
    factory.createXML(model,args.output)




if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="convert a model to histfactory xmls")
    parser.add_argument("input",help="file of the model")
    parser.add_argument("output",help="xml directory to write")
    parser.add_argument("--histograms",help="root file to write the histograms to")    
    args =parser.parse_args()
    main(args)

#!/bin/env python
def gettag(e,i,k):
    return e.getFolder("p.{:d}".format(i)).getTagDoubleDefault(k)
    
def main(args):
    import QFramework
    f = QFramework.TQFolder.loadFromTextFile(args.infile)

    if not f:
        raise RuntimeError("unable to open "+args.infile)

    s2 = f.getFolder("Overlay.expected.band.2s")
    s1 = f.getFolder("Overlay.expected.band.1s")
    exp = f.getFolder("Overlay.expected.med")
    obs = f.getFolder("Overlay.observed.med")        

    xobs = [obs,"x"]
    xexp = [exp,"x"]
    
    obselems = [
        [obs,"x"],
        [obs,"y"]
    ]
    expelems = [
        [exp,"y"],
        [s1,"yn"],
        [s1,"yp"],
        [s2,"yn"],
        [s2,"yp"]
    ]

    iobs = 0
    iexp = 0    
    while True:
        try:
            obsrows = [ gettag(e,iobs,k) for e,k in obselems ]
        except ReferenceError:
            break
        try:
            exprows = [ gettag(e,iexp,k) for e,k in expelems ]            
        except ReferenceError:
            break

        if gettag(obs,iobs,"x") > gettag(exp,iexp,"x"):
            iexp = iexp+1
            obsrows = ["" for e in obselems]
        elif gettag(obs,iobs,"x") < gettag(exp,iexp,"x"):
            iobs = iobs+1
            exprows = ["" for e in expelems]
        else:
            iexp = iexp+1
            iobs = iobs+1            

        s = ",".join(map(str,obsrows+exprows))
        print(s)


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="convert a limit plot to a CSV file")
    parser.add_argument("infile",help="input txt file")
    args = parser.parse_args()
    main(args)

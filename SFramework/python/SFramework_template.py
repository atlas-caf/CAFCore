from QFramework import loadlib,TQLibrary
loadlib("libSFramework.so")

# attempt to load RooFitUtils
if TQLibrary.hasPackage("RooFitUtils"):
    loadlib("libRooFitUtils.so")

import ROOT


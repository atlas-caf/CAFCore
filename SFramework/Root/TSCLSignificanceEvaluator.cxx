#include "SFramework/TSCLSignificanceEvaluator.h"
#include "QFramework/TQIterator.h"
#include "TSystem.h"
#include "QFramework/TQSampleDataReader.h"
#include "QFramework/TQSampleFolder.h"
#include "TStopwatch.h"
#include "QFramework/TQHistogramUtils.h"
#include "QFramework/TQTHnBaseUtils.h"
#include "QFramework/TQGridScanBound.h"
#include "QFramework/TQStringUtils.h"
#include "QFramework/TQLibrary.h"
#include "QFramework/TQUtils.h"
#include "QFramework/TQPathManager.h"
#include "TFile.h"
#include "TMath.h"

#include <iostream>
#include <fstream>

ClassImp(TSCLSignificanceEvaluator)

//___________________________________________________________________________________________

double TSCLSignificanceEvaluator::getLuminosity(TString folderName, TString tagName){
	// retrieve the luminosity value from the given location (default info/luminosity)
	// within the sample folder structure (and save it internally)
	folderName = TQFolder::concatPaths(variations[0],folderName);
	if(this->baseFolder && this->baseFolder->getFolder(folderName))
		this->baseFolder->getFolder(folderName)->getTagDouble(tagName,this->luminosity);
	return this->luminosity;
}

//___________________________________________________________________________________________

TSCLSignificanceEvaluator::TSCLSignificanceEvaluator(TQSampleFolder* sf, TString discVariable, TString name) : 
	TQSignificanceEvaluator(name,NULL),
	baseFolder(TQSampleFolder::newSampleFolder("base")),
	workingFolder(TQSampleFolder::newSampleFolder(name)),
	m_config(new TQFolder("config")),
	tmpFileName("histograms_tmp.root"),
	tmpFileNameBase(""),
	outputdir(""),
	modelName(name),
	discriminantVariable(discVariable),
	engine(NULL),
	sampleFolderFileName("samples.root"),
	significanceFileName("significance.txt"),
	evaluationCommand(""),
	internalEvaluation(true),
	recyclingAllowed(false),
	supportRegionSetHandling(false),
	debug(false), // Add tag to control this!
	nPreparedRegionSets(0),
	sampleFolderMode(NONE),
	significanceMode(NONE)
{
  // specialized constructor taking the target sample folder
  // the discriminant variable (histogram variable)
  // and the name of the desired working folder
  // if(sf){
  // 	this->baseFolder->addObject(sf,"::Nominal");
  // 	this->variations.push_back("Nominal");
  // }
  // this->reader = new TQSampleDataReader(this->baseFolder);
  if (sf) {
    this->reader = new TQSampleDataReader(sf);
  }
  isSimpleEvaluator = false;
}

//___________________________________________________________________________________________

TSCLSignificanceEvaluator::TSCLSignificanceEvaluator(TList* folders, TString discVariable, TString name, TString folderKeyTag) : 
	TQSignificanceEvaluator(name,NULL),
	baseFolder(TQSampleFolder::newSampleFolder("base")),
	workingFolder(TQSampleFolder::newSampleFolder(name)),
	m_config(new TQFolder("config")),
	tmpFileName("histograms_tmp.root"),
	tmpFileNameBase(""),
	outputdir(""),
	modelName(name),
	discriminantVariable(discVariable),
	engine(NULL),
	sampleFolderFileName("samples.root"),
	significanceFileName("significance.txt"),
	evaluationCommand(""),
	internalEvaluation(true),
	recyclingAllowed(false),
	supportRegionSetHandling(false),
	debug(false),
	nPreparedRegionSets(0),
	sampleFolderMode(NONE),
	significanceMode(NONE)
{
  isSimpleEvaluator = false;
	// specialized constructor taking the list of target sample folders
	// the discriminant variable (histogram variable)
	// and the name of the desired working folder
	this->reader = new TQSampleDataReader(this->baseFolder);
	if(!folders || folders->GetEntries() < 1) return;
	if(folders->GetEntries() == 1){
		TQSampleFolder* sf = dynamic_cast<TQSampleFolder*>(folders->First());
		this->baseFolder->addObject(sf,"::Nominal");
		this->variations.push_back("Nominal");
		return;
	}
	TQIterator itr(folders);
	while(itr.hasNext()){
		TQSampleFolder* sf = dynamic_cast<TQSampleFolder*>(itr.readNext());
		if(!sf) continue;
		TString sfname = sf->getName();
		if(!folderKeyTag.IsNull()) sf->getTagString(folderKeyTag,sfname);
		this->baseFolder->addObject(sf,"::"+sfname);
		if(TQStringUtils::matches(sfname,"*Nominal*"))
			this->variations.insert(this->variations.begin(),sfname);
		else
			this->variations.push_back(sfname);
	}
}

//___________________________________________________________________________________________

TSCLSignificanceEvaluator::TSCLSignificanceEvaluator(TString discVariable, TString name) : 
	TQSignificanceEvaluator(name,NULL),
	baseFolder(TQSampleFolder::newSampleFolder("base")),
	workingFolder(TQSampleFolder::newSampleFolder(name)),
	m_config(new TQFolder("config")),
	tmpFileName("histograms_tmp.root"),
	tmpFileNameBase(""),
	outputdir(""),
	modelName(name),
	discriminantVariable(discVariable),
	engine(NULL),
	sampleFolderFileName("samples.root"),
	significanceFileName("significance.txt"),
	evaluationCommand(""),
	internalEvaluation(true),
	recyclingAllowed(false),
	supportRegionSetHandling(false),
	debug(false),
	nPreparedRegionSets(0),
	sampleFolderMode(NONE),
	significanceMode(NONE)
{
  isSimpleEvaluator = false;
	// default constructor 
	// taking the discriminant variable (histogram variable)
	// and the name of the desired working folder
	this->reader = new TQSampleDataReader(this->baseFolder);
}

void TSCLSignificanceEvaluator::setDebugOutputdir(TString outdir){

	this->outputdir = outdir;	
}


//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::deactivate(){
	// deactivate the evaluator, disabling all evaluation capabilities
	this->sampleFolderMode = NONE;
	this->significanceMode = NONE;
	this->internalEvaluation = false;
	this->evaluationCommand = "";
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::exportSampleFolder(bool doexport){
	// trigger exporting of the sample folder
	if(doexport)
		this->sampleFolderMode = EXPORT;
	else
		this->sampleFolderMode = NONE;
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::importSampleFolder(bool doimport){
	// trigger importing of the sample folder
	if(doimport){
		this->sampleFolderMode = IMPORT;
		this->setInternalEvaluation(true);
	} else
		this->sampleFolderMode = NONE;
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::exportSignificance(bool doexport){
	// trigger exporting of evaluation results
	if(doexport)
		this->significanceMode = EXPORT;
	else
		this->significanceMode = NONE;
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::importSignificance(bool doimport){
	// trigger importing of evaluation results
	if(doimport){
		this->significanceMode = IMPORT;
		this->setInternalEvaluation(false);
	} else 
		this->significanceMode = NONE;
}

void TSCLSignificanceEvaluator::setInternalEvaluation(bool eval){
	// set the evaluation to "internal" mode
	// currently not advised due to a rampant memory leak in HistFactory
	// https://sft.its.cern.ch/jira/browse/ROOT-5236
	this->internalEvaluation = eval;
	if(eval && this->significanceMode == IMPORT)
		this->significanceMode = NONE;
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::setExternalEvaluation(const TString& command, bool allowRecycling){
	// set the evaluation to "external" mode 
	// using the given command to be submitted to a console
	// currently the preferred way of evaluation
	if(command.IsNull()){
		this->internalEvaluation = true;
		return;
	}
	this->internalEvaluation = false;
	this->evaluationCommand = command;
	this->significanceMode = IMPORT;
	this->sampleFolderMode = EXPORT;
	this->recyclingAllowed = allowRecycling;
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::setSampleFolderFile(const TString& filename){
	// set the filename of the sample folder for import/export
	if(filename.IsNull())
		return;
	this->sampleFolderFileName = filename;
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::setRegionSetHandling(bool val){
	this->supportRegionSetHandling = val;
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::setSignificanceFile(const TString& filename){
	// set the filename for the import/export of results
	if(filename.IsNull())
		return;
	this->significanceFileName = filename;
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::keepTMP(TString tmpfilenamebase){
	// avoid deletion of the temporary files
	// instead, move them to a location specified by the given base name
	this->tmpFileNameBase = tmpfilenamebase;
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::addVariation(TQSampleFolder* sf, TString varname){
	// add a systematic variation to the evaluation
	this->reader->getSampleFolder()->addObject(sf,"::"+varname);
	this->variations.push_back(varname);
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::setInitialization(TClass* initClass){
	this->initialization = initClass;
}

//___________________________________________________________________________________________

bool TSCLSignificanceEvaluator::configure(const TString& configfilename, const TString& modelname){
	// configure the evaluator using the given stats configuration and model name
	// this function should be called only AFTER all other settings have been adjusted

	m_config->importFromTextFile(configfilename);
	m_config->importFromTextFile(TQPathManager::getPathManager()->getTargetPath(configfilename).c_str());
	this->evaluationCommand.ReplaceAll("$(CONFIG)",configfilename);
	if(modelname.Length() > 0)
		this->modelName = modelname;
	TQFolder* createmodels = m_config->getFolder("CreateModels."+this->modelName);
	if(!createmodels){
		m_config->deleteAll();
		return false;
	}
	createmodels->getTagString("name",this->modelName);
	TQFolder* createworkspaces = m_config->getFolder("CreateWorkspaces");
	if(!createworkspaces){
		m_config->deleteAll();
		return false;
	}
	createworkspaces->getTagString("histogramsFile",tmpFileName);
	// remove variations folder for which there is no sample folder present
	TQIterator vitr(m_config->getListOfFolders("?/ModelConfiguration/Variations/?"),true);
	while (vitr.hasNext()){
	    TQFolder* var = (TQFolder*)vitr.readNext();
		var->setTagString(".Variation",var->getName());
	    TString varsfname = var->replaceInText(var->getTagStringDefault("~SampleFolder",var->getName()));
		if(TQStringUtils::removeLeading(varsfname,":") != 1) continue;
	    bool found = false;
	    for(size_t i=0; i<this->variations.size(); i++){
			if(this->variations[i] == varsfname){
				found = true;
				break;
			}
	    }
	    if(!found){ 
              m_config->removeObject(var->getPath());
		}
	}
	// remove systematics folder for variations that are not present
	TQIterator sysitr(m_config->getListOfFolders("?/ModelConfiguration/Systematics/?"),true);
	TQIterator sysvaritr(m_config->getListOfFolders("?/ModelConfiguration/Variations/?"),true);
	while(sysitr.hasNext()){
		TQFolder* f = dynamic_cast<TQFolder*>(sysitr.readNext());
		TString up = f->getTagStringDefault("Up","");
		TString down = f->getTagStringDefault("Down","");
		sysvaritr.reset();
		int found = 0;
		while(vitr.hasNext()){
			TQFolder* var = (TQFolder*)sysvaritr.readNext();
			if(var->getNameConst() == up || var->getNameConst() == down) found++;
			if(found > 1) break;
		}
		if(found < 2) m_config->removeObject(f->getPath());
	}

	// create a list of all locations and histogram names
	if(!this->baseFolder){
		std::cout << "ERROR in TSCLSignificanceEvaluator(" << this->GetName() << ")::configure(...) : base sample folder is NULL, terminating now!" << std::endl;
		return false;
	}
	if(this->variations.size() < 1){
		std::cout << "ERROR in TSCLSignificanceEvaluator(" << this->GetName() << ")::configure(...) : no variations scheduled - not even 'Nominal'!" << std::endl;
		return false;
	}
	TQSampleFolder* nomfolder = this->baseFolder->getSampleFolder(this->variations[0]);
	if(!nomfolder){
		std::cout << "ERROR in TSCLSignificanceEvaluator(" << this->GetName() << ")::configure(...) : base sample folder does not contain '" << this->variations[0] << "' subfolder, terminating now!" << std::endl;
		return false;
	}
	TQSampleDataReader* nomreader = new TQSampleDataReader(nomfolder);
	// loop over all samples listed in the configuration file
	TQFolder* parameters = m_config->getFolder("?/ModelConfiguration/Parameters/");
	if(parameters)
		parameters->setTagDouble("lumi",this->luminosityScale);
	TQIterator sitr(m_config->getListOfFolders("?/ModelConfiguration/Samples/?"),true);
	TQIterator chitr(m_config->getListOfFolders("?/ModelConfiguration/Channels/?"),true);
	while (sitr.hasNext()) { 
		chitr.reset();
		// obtain the sample path
		TQFolder * f = (TQFolder*)sitr.readNext();
		TString pathorig; 
		f->getTagString("Path", pathorig);
		while (chitr.hasNext()) { 
			// loop over all channels listed in the configuration file
			// fill the channel information into the sample path string
			TQTaggable tags((TQFolder*)(chitr.readNext()));
			TString hName = tags.getTagStringDefault("Histogram",this->discriminantVariable);
			if(this->initialization == TQGridScanner::Class()){
				tags.getTagString("CounterGrid",hName);
			}
			bool foundHistogram = false;
			for(size_t k=0; k<m_histNames.size(); k++){
				if(m_histNames[k] == hName)
					foundHistogram = true;
			}
			if(!foundHistogram)
				this->m_histNames.push_back(hName);
			TString pathreplaced = tags.replaceInText(pathorig);
			TQIterator fitr(nomreader->getListOfSampleFolders(pathreplaced),true);
			// expand the corresponding sample folder paths and loop over them
			while (fitr.hasNext()){
				TQSampleFolder* sf = (TQSampleFolder*)(fitr.readNext());
				if(!sf)
					continue;
				// get a full variant (starting with "Nominal")
				// and a bare variant (without the head)
				TString pathfull = sf->getPath();
				TString pathbare = TQFolder::getPathWithoutHead(pathfull);
				// check if the corresponding folder already exists
				TQSampleFolder* newsf = this->workingFolder->getSampleFolder(pathfull);
				// if not, we create it
				// and save the location
				if(!newsf){
					newsf = this->workingFolder->getSampleFolder(pathfull+"+");
					this->locations.push_back(pathbare);
				}
			}
		}
	}
	delete nomreader;
	for(size_t i=1; i<variations.size(); i++){
		for(size_t j=0; j<locations.size(); j++){
			this->workingFolder->getSampleFolder(TQFolder::concatPaths(this->variations[i],this->locations[j])+"+");
		}
	}
	return true;
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::printMode(){
	// print the current evaluation mode/settings
	std::cout << TQStringUtils::makeBoldWhite("== Configuration for Significance Evaluator ");
	std::cout << TQStringUtils::makeBoldRed(this->GetName());
	std::cout << TQStringUtils::makeBoldWhite(" ==") << std::endl;
	if(this->sampleFolderMode == EXPORT){
		std::cout << "export sample folder to " << this->sampleFolderFileName << " (compression:" << this->getTagIntegerDefault("tmpFileCompression",0) << ")" << std::endl;
	} else if(this->sampleFolderMode == IMPORT){
		std::cout << "import sample folder from " << this->sampleFolderFileName << std::endl;
	}
	if(this->internalEvaluation){
		std::cout << "evaluate internally" << std::endl;
	} else if(!this->evaluationCommand.IsNull()){
		std::cout << "evaluate externally using '" << this->evaluationCommand << "' (recycle: " << (this->recyclingAllowed ? "yes" : "no") << ")" << std::endl;
	} else {
		std::cout << "do not perform evaluation" << std::endl;
	}
	if(this->initialization){
		std::cout << "initialized with '" << this->initialization->GetName() << "'" << std::endl;
	} else {
		std::cout << "not (yet) initialized" << std::endl;
	}
	if(this->supportRegionSetHandling){
		std::cout << "using combined fit for region-splitting" << std::endl;
	} else {
		std::cout << "using separate fits for split regions" << std::endl;
	}
	if(this->significanceMode == EXPORT){
		std::cout << "export results to " << this->significanceFileName << std::endl;
	} else if(this->significanceMode == IMPORT){
		std::cout << "import results from " << this->significanceFileName << std::endl;
	}
	std::cout << TQStringUtils::makeBoldWhite("-------------------------------------------------") << std::endl;
}

//___________________________________________________________________________________________

int TSCLSignificanceEvaluator::exportWorkspace(const TString& outFileName) {
  // Function to export a statistics model for the current cut configuration

  // Add ExportModel action to statistics configuration!
  TQFolder *runFolder = m_config->copy("exportModel");
  
  TQFolder *f1 = runFolder->getFolder("ExportWorkspaces+");
  TQFolder *f2 = f1->getFolder(this->modelName+"+");
  bool ok = f2->setTagString("outputFile", outFileName);
  
  // delete CalculateSignificance action if present
  TQFolder* cs = runFolder->getFolder("CalculateSignificance");
  if (cs) {
    cs->deleteAll();
    runFolder->deleteObject("CalculateSignificance");
  }
  
  if(!this->isPrepared()){
    if(!this->prepareNextRegionSet("")){ // prepareFromGridScanner is executed in here
      ERRORclass("Unable to prepare region!");
      return 0;
    }
  }
  
  // run the statistics code
  INFOclass("Running the statistics code and exporting model!");
  this->engine->setDefaultSampleFolder(this->workingFolder);
  this->engine->reset();
  this->engine->run(runFolder);

  delete runFolder;
  return ok;
}

std::vector<double> TSCLSignificanceEvaluator::evaluateMultiple(){

	// run the evaluation 
	TString sfname = this->sampleFolderFileName;
	sfname.ReplaceAll("$(ID)",this->fileIdentifier);
	TString sigfname = this->significanceFileName;
	sigfname.ReplaceAll("$(ID)",this->fileIdentifier);
	// clean up the tmp space
	TQSampleFolder* wf = NULL;
	gSystem->Exec("rm -f "+this->tmpFileName);
	bool canRecycle = this->recyclingAllowed && !this->internalEvaluation && TQUtils::fileExists(sfname) && TQUtils::fileExists(sigfname);

        // Import and Export needed for grid running! Ignore this for now and try internal evaluation mode!
	if(this->sampleFolderMode == IMPORT){
		// if we are in in IMPORT sampleFolder (i.e. IMPORT and INTERNAL) mode, we need to import it
		wf = TQSampleFolder::loadSampleFolder(sfname+":"+this->GetName());
		if(wf){
			std::cout << "ERROR in TSCLSignificanceEvaluator('" << this->GetName() << "')::evaluate() : error importing sample folder from " << sfname << ":" << this->GetName() << "!" << std::endl;
			return {};
		}
	}
        else if(!canRecycle){
            // check for a valid initialization
            if(!this->isPrepared()){
                if(!this->prepareNextRegionSet("")){ // prepareFromGridScanner is executed in here
                  ERRORclass("Unable to prepare region!");
                }
            }
            // We are in internal mode and use this->workingFolder
            wf = this->workingFolder;
	}
	this->nPreparedRegionSets = 0; // reset prepared regions 

        // TODO: Implement check cutoffs!
	// if(!canRecycle){
	// 	bool cutoffsPassed = this->checkCutoffs();
	// 	if(!cutoffsPassed){
	// 		this->cleanWorkingFolder();
	// 		return {};
	// 	}
	// }
        
	// if(this->sampleFolderMode == EXPORT && !(this->recyclingAllowed && TQUtils::fileExists(sfname))){
	// 	// if we are in in EXPORT sampleFolder (i.e. EXPORT or EXTERNAL) mode, we need to export it
	// 	if(!wf){
	// 		std::cout << "ERROR in TSCLSignificanceEvaluator('" << this->GetName() << "')::evaluate() : cannot export, invalid working folder!" << std::endl;
	// 		return {};
	// 	}
	// 	TFile* f = new TFile(sfname,"RECREATE",sfname,this->engine->getHistogramsFileCompression());
	// 	if(!f || !f->IsOpen()){
	// 		std::cout << "ERROR in TSCLSignificanceEvaluator('" << this->GetName() << "')::evaluate() : cannot export, unable to open file " << sfname  << "!" << std::endl;
	// 		return {};
	// 	}
	// 	f->Add(wf->copy());
	// 	f->Write();
	// 	f->Close();
	// 	delete f;
	// }

	// we initialize the significance to zero
	double fom = 0;
	TQFolder* results = NULL;
	TString evalcmd(this->evaluationCommand);
	evalcmd.ReplaceAll("$(ID)",this->fileIdentifier);
	if(this->internalEvaluation){
		// if we are in INTERNAL mode, we need to
		// reset the engine
          
		if(!this->engine){
                    std::cout << "ERROR in TSCLSignificanceEvaluator('" << this->GetName() << "')::evaluate() : cannot export, no statistics manager given! " << sfname  << "!" << std::endl;
                    if(wf != this->workingFolder) delete wf;
                    return {};
		}
		this->engine->setDefaultSampleFolder(wf);
		this->engine->reset();
		// apply verbosity
		if(!this->verbose) TQLibrary::redirect("/dev/null");
		// run the statistics code
                INFOclass("Running the statistics code");
                TQFolder *runFolder = m_config->copy("runConfig");
		this->engine->run(runFolder);
                delete runFolder;
		// restore verbosity
		if(!this->verbose) TQLibrary::restore();
		// obtain the results
                INFOclass("Getting the results");
		results = this->engine->getResults();
		// clean up the mess afterwards 
		// but only if the user didn't request to be left with the tmp file
		TString cmd;
		if(this->tmpFileNameBase.Length() > 0){
			cmd = "mv "+this->tmpFileName+" "+this->tmpFileNameBase+this->fileIdentifier+".root";
		} else {
			cmd = "rm -f "+this->tmpFileName;
		}
		gSystem->Exec(cmd);
        }
        // else if(!evalcmd.IsNull()){
	// 	if(canRecycle){
	// 		this->info += " recycled results";
	// 	} else {
	// 		// if we are in EXTERNAL mode, we just need to execute whatsoever
	// 		// external command was given to us
	// 		TStopwatch s;
	// 		s.Start();
	// 		if(gSystem->Exec(evalcmd.Data()) > 0){
	// 			std::cout << "ERROR in TSCLSignificanEvaluator::evaluate() : gSystem->Exec() reported an error on the following command:" << std::endl << evalcmd << std::endl;
	// 			return {};
	// 		} 
	// 		this->info += TString::Format(" (~%.2fs)",s.RealTime());
	// 	}
	// }
	// if(this->significanceMode == IMPORT){
	// 	// if we are in IMPORT significance (i.e. IMPORT or EXTERNAL) mode
	// 	// we need to read in the results afterwards
	// 	if(results) delete results;
	// 	results = new TQFolder("results");
	// 	results->importFromTextFile(sigfname);
	// } else if(this->significanceMode == EXPORT){
	// 	// if we are in EXPORT significance (i.e. EXPORT and INTERNAL) mode
	// 	// we need to write the results to a file
	// 	if(!results){
	// 		std::cout << "ERROR in TSCLSignificanceEvaluator::evaluate() : results pointer is NULL in EXPORT significance mode - this should never happen!" << this->modelName << "!" << std::endl;
	// 		if(wf != this->workingFolder) delete wf;
	// 		return {};
	// 	}
	// 	if(canRecycle) results->exportToTextFile(sigfname);
	// }
        m_nPointsProcessed++;
        
	if(!results){
		if(wf != this->workingFolder) delete wf;
		return {};
	}

        // for debugging save out intermediate result
        if (this->debug) {
        //   if (m_nPointsProcessed == 1) {
            
            TString baseFilePath = this->outputdir;
            TQUtils::ensureDirectory(TQPathManager::getPathManager()->getTargetPath(baseFilePath).c_str());
            results->SaveAs(TQPathManager::getPathManager()->getTargetPath(baseFilePath+"/resultsFit_" + std::to_string(m_nPointsProcessed) +".root").c_str());
        //   }
        }

        // --------------------------------------------------------------------
        // Extract desired results from fit here
        // Steered by tags (path.Z0_asimov, tagname.Z0_asimov, ...)
        // in model config to find desired results!
        
        bool foundResult = false;
        std::vector<double> foms;

        for (auto name : m_FOMNames) {
          fom = -999;
          TString fomPath;
          TString fomTag;
          TQFolder *significanceFolder = m_config->getFolder("CalculateSignificance/?/");
          significanceFolder->getTagString(TString::Format("path.%s", name.Data()), fomPath);
          if (fomPath.IsNull()) {
            WARNclass("Path of figure of merit with name '%s' not found in results folder! Check your model configuration e.g. for 'path.Z0_asimov' or 'tagname.Z0_asimov'!", name.Data());
          }
          TQFolder* resultFolder = results->getFolder(TQFolder::concatPaths(this->modelName, fomPath));
          if(resultFolder) {
            foundResult = true;
            
            significanceFolder->getTagString(TString::Format("tagname.%s", name.Data()), fomTag);
            // when uncertainty on mu should be extracted check if fit ran with Minos!
            if (fomTag.Contains("err") || name.Contains("Err")) {
              bool runMinos = false;
              m_config->getTagBool("fit.runMinos~", runMinos);
              if (runMinos) {
                double tmpErr = -1;
                resultFolder->getTagDouble("errHigh", tmpErr);
                if (tmpErr > 0) {
                  fom = std::abs(tmpErr);
                  resultFolder->getTagDouble("errLow", tmpErr);
                  fom += std::abs(tmpErr);
                  fom /= 2;
                } else { // try to fall back to err!
                  resultFolder->getTagDouble("err", fom);
                }
              } else {
                resultFolder->getTagDouble("err", fom);
              }
            } else {
              if (fomTag.IsNull()) {
                WARNclass("Tag name for figure of merit with name '%s' not found in results folder! Check your model configuration e.g. for 'path.Z0_asimov' or 'tagname.Z0_asimov'!", name.Data());
              }
              resultFolder->getTagDouble(fomTag, fom);
            }
            this->info += TString::Format("%s = %g, ", name.Data(), fom);
          }
          foms.push_back(fom);
        }
        this->info.Resize( this->info.Length()-2); // remove ', ' at end of string
        
        if (!foundResult) {
          ERRORclass("Unable to obtain results from %s", this->modelName.Data());
          if(!this->internalEvaluation && !evalcmd.IsNull()) INFOclass("tried to evaluate externally using '%s' ...", evalcmd.Data());
          INFOclass("Results folder content was:");
          results->print("rdt");
          if(this->significanceMode == IMPORT) delete results;
          if(wf != this->workingFolder) delete wf;
          return {};
        }

	// if everything went well, we can return
	if(this->significanceMode == IMPORT) delete results;
	if(wf != this->workingFolder) delete wf;

        return foms;
}

//___________________________________________________________________________________________

int TSCLSignificanceEvaluator::cleanWorkingFolder(){
  // Clean working folder from all TH1s
  
  int nCleaned = 0;
  for (auto ipath : m_samplePaths) {
    for (auto iregion : m_histRegionsInInput) {
      TString pathExtended = TQFolder::concatPaths(ipath, ".histograms", iregion);
      if (this->debug) {INFOclass("Looping over histogram folder: '%s'", pathExtended.Data());}
      TQFolder* f = this->workingFolder->getFolder(pathExtended);
      if (!f) {
        WARNclass("Sample folder with path '%s' not found in working folder! Skipping", pathExtended.Data());
        continue;
      }
      f->deleteAll();
      nCleaned++;
    }
  }
  
  return nCleaned;
}

//___________________________________________________________________________________________

bool TSCLSignificanceEvaluator::checkCutoffs(){
  // TODO: Reintroduce!
	if(!this->workingFolder) return false;
	TQSampleDataReader rd(this->workingFolder);
	TList* l = rd.getListOfHistogramNames();
	if(!l) return false;
	if(l->GetEntries() < 1) { delete l; return false; };
	l->SetOwner(true);
	TQIterator itr(l,true);
	for(size_t ivar=0; ivar<this->variations.size(); ivar++){
		double sum = 0;
		double sumError2 = 0;
		while(itr.hasNext()){
			TObject* name = itr.readNext();
			if(!name) continue;
			for(size_t iloc=0; iloc<this->locations.size(); iloc++){
				TH1F* h = dynamic_cast<TH1F*>(this->workingFolder->getObject(name->GetName(),TQFolder::concatPaths(this->variations[ivar],this->locations[iloc],".histograms")));
				Double_t err = 0;
				Double_t val = TQHistogramUtils::getIntegralAndError(h,err);
				sum += val;
				sumError2 += err*err;
				if(this->verbose) this->info += TString::Format(" %s:%s:%s = %.2f +/- %.2f     ", variations[ivar].Data(),locations[iloc].Data(),name->GetName(),val,err);
			}
		}
		itr.reset();
		double error = sqrt(sumError2);
		this->info += TString::Format(" %s: %.3f +/- %.3f ",variations[ivar].Data(),sum,error);
		if(sum < this->getTagDoubleDefault("cutoff",0)){
			this->info += " - evaluation skipped, absolute cutoff underrun";
			return false;
		}
		if(error/sum > this->getTagDoubleDefault("relErrCutoff",1.0)){
			this->info += " - evaluation skipped, rel. err. cutoff underrun";
			return false;
		}
	}
	return true;
}

//___________________________________________________________________________________________


int TSCLSignificanceEvaluator::configureBoundRegions() {
  // There is at least one region specified that has a bound range with a variable in the n-dim histogram.
  // Here, the axis of the n-dim histogram is added to the location in the working folder of this region
  // in order to retrieve the range settings later on in the prepare step!
   for (auto ipath : m_samplePaths) {
    for (auto iregion : m_histRegionsInInput) {
      TString pathExtended = TQFolder::concatPaths(ipath, ".histograms", iregion);
      TQFolder* f = this->workingFolder->getFolder(pathExtended);
      if (!f) {
        WARNclass("Sample folder with path '%s' not found in working folder! Skipping", pathExtended.Data());
        continue;
      }
      int index_Hist;
      if (f->getTagInteger("index_Hist", index_Hist)) {
        TString boundVariable;
        if (f->getTagString("BoundToVariable", boundVariable)) {
           // find correct axis of multidim hist! 
          // it does not matter which n-dim hist is checked, they all have the same axes ranges!
          TH1F *h = m_Hists[index_Hist];
          TAxis *OneDAxis = h->GetXaxis();
          THnBase *ndimHist = m_multiDimHists[0];
          TAxis * multiDimAxis = TQTHnBaseUtils::getAxis(ndimHist, boundVariable);
          if (!multiDimAxis) {
            BREAKclass("Histogram with name '%s' in region '%s' is configured to be bound to axis of n-dim histogram with name '%s' BUT axis in question cannot be found in n-dim histogram! Exiting now ...", h->GetName(), pathExtended.Data(), boundVariable.Data());
          }
          TString binningOneD = TQHistogramUtils::getBinningDefinition(OneDAxis);
          TString binningMultiDim = TQHistogramUtils::getBinningDefinition(multiDimAxis);
          if (!binningOneD.EqualTo(binningMultiDim)) {
            BREAKclass("Histogram with name '%s' in region '%s' is configured to be bound to axis of n-dim histogram with name '%s' BUT there is no matching binning definition! The 1-d histogram has a binning of '%s', the axis in the n-dim histogram of '%s'! Make sure the binnings are the same, otherwise you cannot use this feature! Exiting now ...", h->GetName(), pathExtended.Data(), boundVariable.Data(), binningOneD.Data(), binningMultiDim.Data());
          }
          m_boundMultiDimAxis[TString::Format("%i", index_Hist)] = multiDimAxis;
        }
      }
    }
   }
   return true;
}


int TSCLSignificanceEvaluator::prepareFromGridScanner(const TString& /*suffix*/){
  // Prepare the working folder from a TQGridScanner object.
  // should be called automatically on evaluation 
  // if initialized with a TQGridScanner
  // Extracts TH1Fs from the multiDim hists and adds to working folder
  // to prepare for the model building and the further statistical analysis!

  if (this->debug) {INFOclass("=> Prepare working folder!");}
  if (!this->isPrepared()) {
    if (this->debug) {INFOclass("clean working folder!");}
    this->cleanWorkingFolder();
    this->info = ""; 
  }
  
  if (this->debug) {
    this->info += TString::Format("\n ********************************* DEBUG OUTPUT *******************************");
  }
  
  int nHistograms = 0;
  for (auto ipath : m_samplePaths) {
    for (auto iregion : m_histRegionsInInput) {
      TString pathExtended = TQFolder::concatPaths(ipath, ".histograms", iregion);
      TQFolder* f = this->workingFolder->getFolder(pathExtended);
      if (!f) {
        WARNclass("Sample folder with path '%s' not found in working folder! Skipping", pathExtended.Data());
        continue;
      }
      
      int index_multiDimHist;
      int index_Hist;
      TH1F *h = new TH1F();
      h->Sumw2();
      if (f->getTagInteger("index_multiDimHist", index_multiDimHist)) {
        h = (TH1F*)(m_multiDimHists[index_multiDimHist]->Projection(m_discriminantVariableIndex));
        h->SetName(TString::Format("MTfit"));
        if (!h) {ERRORclass("Projecting multidimensional histogram failed!");}
        else {
          h->SetName(this->discriminantVariable);
          f->addObject(h);
          nHistograms++;
          if (this->debug) {
            if (!TQStringUtils::split(pathExtended, "/")[1].Contains("up") &&
                !TQStringUtils::split(pathExtended, "/")[1].Contains("down")) {// only print nominal
              this->info += TString::Format("\n * Location = %s: nevents=%g ", pathExtended.Data(), h->Integral(0, TQGridScanBound::BinExtrema::max));}
          }
        }
      } else if (f->getTagInteger("index_Hist", index_Hist)) { // usually CR histograms
        TH1F *h = m_Hists[index_Hist];
        TH1F *cuttedHist = NULL;
        TString boundVariable;
        if (f->getTagString("BoundToVariable", boundVariable)) {
          // in region which is bound to scan variable. This was already configured before so
          // simply retrieve the multidim axis to which we are bound.
          TAxis *multiDimAxis = m_boundMultiDimAxis[TString::Format("%i", index_Hist)];
          // cut histogram according to current range in multidim hist and consider specified flank!
          if (f->getTagBoolDefault("ConsiderUpperPart", false)) {
            int bin = multiDimAxis->GetLast()+1;
            cuttedHist = (TH1F*)TQHistogramUtils::cutHistogram(h, bin, h->GetXaxis()->GetNbins()+1);
          } else if (f->getTagBoolDefault("ConsiderLowerPart", false)) {
            int bin = multiDimAxis->GetFirst()-1;
            cuttedHist = (TH1F*)TQHistogramUtils::cutHistogram(h, 0, bin);
          } else {
            WARNclass("Histogram bound to variable '%s'  has no specification of flank! This will probably cause undesired behavior!", boundVariable.Data());
          }
        } else {
          cuttedHist = h;
        }        
        if (!cuttedHist) {ERRORclass("Preparing histogram failed!");}
        else {
          f->addObject((TH1F*)cuttedHist);
          nHistograms++;
          if (this->debug) {
            if (!TQStringUtils::split(pathExtended, "/")[1].Contains("up") &&
                !TQStringUtils::split(pathExtended, "/")[1].Contains("down")) {// only print nominal
              this->info += TString::Format("\n Location = %s: nevents=%g ", pathExtended.Data(), cuttedHist->Integral(0, TQGridScanBound::BinExtrema::max));}
          }
        }
      }
    }
  }
  if (this->debug) {
    this->info += TString::Format("\n");
  }
  
  // for debugging save out example of prepared sample folder
  if (this->debug) {
    // if (m_nPointsProcessed < 2) {
      TString baseFilePath = this->outputdir;
      TQUtils::ensureDirectory(TQPathManager::getPathManager()->getTargetPath(baseFilePath).c_str());
      this->workingFolder->SaveAs(TQPathManager::getPathManager()->getTargetPath(baseFilePath+TString::Format("/workingFolderPrepared_%i.root", m_nPointsProcessed)).c_str());
    // }
  }

  if (this->debug) {INFOclass("Working folder prepared!");}
  if(nHistograms > 0) this->nPreparedRegionSets++;
  return nHistograms;
}

//___________________________________________________________________________________________

TQSampleFolder* TSCLSignificanceEvaluator::getWorkingFolder(TString path){
	// return a pointer to the current working folder
	if(!this->workingFolder)
		return NULL;
	if(path.IsNull())
		return this->workingFolder;
	return this->workingFolder->getSampleFolder(path);
}

//___________________________________________________________________________________________

TQSampleFolder* TSCLSignificanceEvaluator::getBaseFolder(TString path){
	// return a pointer to the current base folder
	if(!this->baseFolder)
		return NULL;
	if(path.IsNull())
		return this->baseFolder;
	return this->baseFolder->getSampleFolder(path);
}

//___________________________________________________________________________________________

TQFolder* TSCLSignificanceEvaluator::getConfigFolder(TString path){
	// return a pointer to the configuration folder
	if(!m_config)
		return NULL;
	if(path.IsNull())
		return m_config;
	return m_config->getFolder(path);
}

//___________________________________________________________________________________________

void TSCLSignificanceEvaluator::printPaths(){
	// print the paths of all registered histograms
	std::cout << this->GetName() << " INFO: the following histograms have been booked" << std::endl;
	for(size_t i=0; i<variations.size(); i++){
		for(size_t j=0; j<locations.size(); j++){
			for(size_t k=0; k<m_histNames.size(); k++){
				std::cout << TQFolder::concatPaths(variations[i],locations[j]) << "\t" << m_histNames[k] << std::endl;
			}
		}
	}
}

//___________________________________________________________________________________________

bool TSCLSignificanceEvaluator::addFOM(const TString& fomName) {
  INFOclass("Adding FOM with name '%s'", fomName.Data());
  m_FOMNames.push_back(fomName);
  return true;
}

//___________________________________________________________________________________________

bool TSCLSignificanceEvaluator::initialize(const TString& /*importfilename*/, const TString& /*exportfilename*/){

	// initialize the evaluator for import/export only
	// CURRENTLY NOT IMPLEMENTED
	return true;
}


//___________________________________________________________________________________________

bool TSCLSignificanceEvaluator::initialize(TQGridScanner* scanner ){
  // Initialize the evaluator for use with a TQGridScanner
  // The model configuration is used to build the folder structure of the 'workingFolder'.
  // There are no input histograms added to the working folder at this stage, but
  // the used multidimensional histograms are read and saved in a vector only.
  // The relevant TH1Fs (which are projections of the n-dim histograms)
  // will be added (and cleanup up afterwards) to the working folder in each 'evaluate'
  // call, i.e. for each cut configuration (which is set on the n-dim hists
  // in the TQGridScanner), before the statistics code is run.
  
  if(!scanner){
    ERRORclass("no TQGridScanner appointed to initialization!");
    return false;
  }
  if(!reader){
    ERRORclass("no TQSampleDataReader available!");
    return false;
  }
  // regions equivalent to variations!
  // if(m_regions.size() < 1){
  //   ERRORclass("No regions set!");
  //   return false;
  // }
  this->initialization = scanner->Class();
  this->scanner = scanner;

  if(this->verbose) VERBOSEclass("Initializing likelihood fit evaluator");
  // set debug mode
  this->getTagBool("debug", debug);

  double luminosity;
  this->reader->getSampleFolder()->getTagDouble("luminosity", luminosity);
  this->workingFolder->setTagDouble("luminosity", luminosity);

  // check if it is possible to extract multiple FOMs 
  this->getTagBool("multipleFOMsCompatible", m_multipleFOMsCompatible);
  m_multipleFOMsCompatible = true;
  
  // ------------------------------------------------------------------------
  // Read in model configuration
  // TString modelConfigPath = scanner->getTagStringDefault("cl.modelConfig", "config/gridscanner/statistics/runChain.txt");
  TString modelConfigPath;
  this->getTagString("modelConfig", modelConfigPath);
  m_config->importFromTextFile(TQPathManager::getPathManager()->findConfigPath(modelConfigPath));
  
  TQFolder *modelFolder = m_config->getFolder("CreateModels/?");
  if (modelFolder) {this->modelName = modelFolder->GetName();} 
  else {WARNclass("No model folder name was found in the config, using the default name '%s'.", this->modelName.Data());}
  this->workingFolder->SetName(this->modelName); 
  
  // TODO: Tags for followings paths to Samples and Channels (?)
  // R: Seems to be set in stone from SFramework for now. Leave hardcoded
  TQIterator sitr(m_config->getListOfFolders("CreateModels/?/Samples/?"),true);
  TQIterator chitr(m_config->getListOfFolders("CreateModels/?/Channels/?"),true);
  TQIterator vitr(m_config->getListOfFolders("CreateModels/?/Variations/?"),true);

  // Read model configuration
  INFOclass("Reading model configuration!");
  
  //---------------------------------------------------------------------------------------------
  // First, fill member vectors to store locations and names of later on added TH1Fs:
  while (chitr.hasNext()) {
    TQFolder* channelFolder = (TQFolder*)chitr.readNext();
    TString regionAndHistName;    // example: 'CutGGF_0jet/MT'
    channelFolder->getTagString("Histogram", regionAndHistName);
    if (!regionAndHistName.IsNull()) { // if histogram specified (exclude Counter here!)
      TString regionInSF = TQFolder::getPathWithoutTail(regionAndHistName); // example: 'CutGGF_0jet'
      INFOclass("region in sf added: %s", regionInSF.Data());
      m_histRegionsInInput.push_back(regionInSF);
      m_histRegionsInModel.push_back(channelFolder->getName());  // example: 'SR_0j_DF_all'
    }
  }
  while (sitr.hasNext()) { // example: 'bkg/[em$(Variation)+me$(Variation)]/top/'
    vitr.reset();
    TQFolder * f = (TQFolder*)sitr.readNext();
    TString samplePathOrig;
    f->getTagString("Path", samplePathOrig);
    while (vitr.hasNext()) { // example: '__FT_EFF_Eigen_B_0__1up'
      TQTaggable vtags((TQFolder*)vitr.readNext());
      TString samplePathReplaced = vtags.replaceInText(samplePathOrig.Data()); // example: '/bkg/[em+me]/top/'
      // expand ([em+me] will become separated to em and me)
      TQIterator fitr(this->reader->getListOfSampleFolders(samplePathReplaced),true);
      while (fitr.hasNext()){
        TQSampleFolder* sf = (TQSampleFolder*)(fitr.readNext());
        if(!sf) continue;
        TString pathFull = sf->getPath(); // example:  '/bkg/me_FT_EFF_Eigen_B_0__1up/top/'
        m_samplePaths.push_back(pathFull);
      }
    }
  }
  
  chitr.reset();
  sitr.reset();
  
  // some basic checks
  if (m_samplePaths.empty() || m_histRegionsInInput.empty()) {
    ERRORclass("No sample paths or regions found! Please check your configuration and try again!");
    return false;
  }

  bool useBoundRegions = false;
  
  // //---------------------------------------------------------------------------------------------
  // // Now, add multi dim hists to list and add counters (if present) to the working folder
  TString nDimHistName;
  this->getTagString("nDimHistName", nDimHistName);
  m_discriminantVariableIndex = -1; // this is set when the first n-dim hist is loaded
  for (auto ipath: m_samplePaths) {
    if (this->debug) {INFOclass("Looping over sample with path: '%s'", ipath.Data());}
    
    // add folder with sample path if it not already exists!
    TQSampleFolder* newsf = this->workingFolder->getSampleFolder(ipath);
    if (!newsf) {newsf = this->workingFolder->getSampleFolder(ipath+"+");}
    
    // add .histogram folder if it not already exists
    TQFolder* folderHistograms = newsf->getFolder(".histograms");
    if (!folderHistograms) {folderHistograms = newsf->getFolder(".histograms+");}
    
    //-----------------------------------------
    // add histograms
    for (unsigned int i=0; i < m_histRegionsInInput.size(); i++) {
      
      if (this->debug) {INFOclass("Looping over channel/region (name in input): '%s' ", m_histRegionsInInput[i].Data());}

      // create folder for region
      TQFolder *folderRegion = folderHistograms->getFolder(m_histRegionsInInput[i]+"+");
      
      // get folder for channel definitions from config to read tags!
      TString channelConfigPath = TQFolder::concatPaths("CreateModels/?/Channels", m_histRegionsInModel[i]);
      TQFolder *channelConfigFolder = m_config->getFolder(channelConfigPath);
      if (!channelConfigFolder) {BREAKclass("Folder '%s' not found in statistics configuration. Please check and try again! Code is exiting now...", channelConfigPath.Data());}
      
      // -----------------------------------
      // Import n-dim histogram (usually used for SRs)
      if (channelConfigFolder->getTagBoolDefault("fromMultiDimHist", false)) {
        if (this->debug) {INFOclass("Adding Multidimensional histogram to list!");}
      
        // check if input exists for requested sample path and import multidimensional histogram
        TString regionAndMultiDimHistName = TQFolder::concatPaths(m_histRegionsInInput[i], nDimHistName);
        THnBase* orig_h = this->reader->getTHnBase(ipath.Data(), regionAndMultiDimHistName.Data());
        if (!orig_h) {
          ERRORclass("N-dimensional histogram not found when calling: 'reader->getTHnBase(%s, %s)'!", ipath.Data(), regionAndMultiDimHistName.Data());
        }
        // Process multidimensional histograms to only regard dimensions that are actually used in gridscan
        THnBase* h = TQTHnBaseUtils::projectionND(orig_h, scanner->obsNamesToScan());      
        // delete the original object
        delete orig_h;
        if (h) {
          if (this->debug) {INFOclass("Found N-dimensional histogram when calling: 'reader->getTHnBase(%s, %s)'!", ipath.Data(), regionAndMultiDimHistName.Data());}
          m_multiDimHists.push_back(h); // add multi dim hist
          // add region to working folder with region name from model AND (important!)
          // with index of multidim hist in vector
          folderRegion->setTagInteger("index_multiDimHist", m_multiDimHists.size()-1);
          // ---------------------------------------------------
          // get index of discriminant variable in multidimensional histogram if not already set
          if (m_discriminantVariableIndex < 0) {
            // get axis index
            if (!TQTHnBaseUtils::getAxisIndex(h, this->discriminantVariable, m_discriminantVariableIndex)) {
              BREAKclass("Discriminant variable '%s' could not be found in input n-dim histogram! Please check your input configuration and try again!", this->discriminantVariable.Data());
            }
          }
        }
        else {
          ERRORclass("Unable to project N-dimensional histogram when calling 'reader->getTHnBase(%s, %s)'!", ipath.Data(), regionAndMultiDimHistName.Data());
        }
      }
      else { // if no n-dim histogram is used in this region but a standard TH1F! (e.g. for CRs)

        if (this->debug) {INFOclass("Adding histogram to working folder!");}
        TString histName; // only used when counters are present
        if (channelConfigFolder->getTagString("Histogram", histName)) {
          TH1F* h = (TH1F*)this->reader->getHistogram(ipath.Data(), histName.Data());
          if (!h) {
            ERRORclass("Histogram not found when calling: 'reader->getHistogram(%s, %s)'!", ipath.Data(), histName.Data());
          } else {
            if (this->debug) {INFOclass("Found histogram when calling: 'reader->getHistogram(%s, %s)'!", ipath.Data(), histName.Data());}
            
            m_Hists.push_back(h); // add 1d hist
            // add region to working folder with region name from model AND (important!)
            // with index of multidim hist in vector
            folderRegion->setTagInteger("index_Hist", m_Hists.size()-1);

            TString boundVariable;
            if (channelConfigFolder->getTagString("BoundToVariable", boundVariable)) {
              // set necessary tags and enable configuration of bound variables later on
              useBoundRegions = true;
              folderRegion->setTagString("BoundToVariable", boundVariable);
              bool considerUpperPart = channelConfigFolder->getTagBoolDefault("ConsiderUpperPart", false);
              bool considerLowerPart = channelConfigFolder->getTagBoolDefault("ConsiderLowerPart", false);
              if ((!considerUpperPart && !considerLowerPart) || (considerUpperPart && considerLowerPart))  {
                BREAKclass("Histogram called with reader->getHistogram('%s', '%s') bound to variable but no flank to consider specified! Please set either tag 'ConsiderUpperPart' or 'ConsiderLowerPart' to true in your channel configuration. Exiting now ...", ipath.Data(), histName.Data());
              }
              folderRegion->setTagBool("ConsiderUpperPart", considerUpperPart);
              folderRegion->setTagBool("ConsiderLowerPart", considerLowerPart);
            }
              
          }
        }
      }
      
    }
    //-----------------------------------------
    // add counters if present
    chitr.reset(); // loop over all channels again and only check for counters!
    while (chitr.hasNext()) { 
      // check if counters are configured and direclty add them to working folder
      // Since counters are not (cannot be) modified in the scan they can be 
      // added already in the initialize step.
      // They are also not removed in cleanWorkingFolder()
      TQFolder* channelFolder = (TQFolder*)chitr.readNext();
      TString counterName; // only used when counters are present
      if (channelFolder->getTagString("Counter", counterName)) {
        // create cutflows folder
        TQFolder *folderCounters = this->workingFolder->getFolder(TQFolder::concatPaths(ipath, ".cutflow"));
        if (!folderCounters) {folderCounters = this->workingFolder->getFolder(TQFolder::concatPaths(ipath, ".cutflow+"));}
        TQCounter* counter = (TQCounter*)this->reader->getCounter(ipath.Data(), counterName);
        if (counter) {
          if (this->debug) {INFOclass("Found Counter when calling: 'reader->getCounter(%s, %s)'!", ipath.Data(), counterName.Data());}
          folderCounters->addObject(counter);
        } else {
          ERRORclass("Counter not found when calling: 'reader->getCounter(%s, %s)'!", ipath.Data(), counterName.Data());
        }
      } else {
        if (this->debug) {INFOclass("No counter specified in channel path '%s'!", channelFolder->getPath().Data());}
      }
    }
  }

  if (useBoundRegions) configureBoundRegions();

  if (this->debug) {
    TString baseFilePath = this->outputdir;
    TQUtils::ensureDirectory(TQPathManager::getPathManager()->getTargetPath(baseFilePath).c_str());
    this->workingFolder->SaveAs(TQPathManager::getPathManager()->getTargetPath(baseFilePath+"/workingFolderTemplate.root").c_str());
  }
  
  // TString pathreplaced = tags.replaceInText(samplePathOrig);
      
  // TQIterator fitr(nomreader->getListOfSampleFolders(pathreplaced),true);
  // // expand the corresponding sample folder paths and loop over them
  // while (fitr.hasNext()){
  //   TQSampleFolder* sf = (TQSampleFolder*)(fitr.readNext());
  //   if(!sf)
  //     continue;
  //   // get a full variant (starting with "Nominal")
  //   // and a bare variant (without the head)
  //   TString pathfull = sf->getPath();
  //   TString pathbare = TQFolder::getPathWithoutHead(pathfull);
  //   // check if the corresponding folder already exists
  //   TQSampleFolder* newsf = this->workingFolder->getSampleFolder(pathfull);
  //   // if not, we create it
  //   // and save the location
  //   if(!newsf){
  //     newsf = this->workingFolder->getSampleFolder(pathfull+"+");
  //     this->locations.push_back(pathbare);
  //   }

  // Not entirely sure what this next code does.
  // Except adding grids to the GridScanner...this is not needed anymore in the new implemenation
  // What is it with the getSampleFolder()->collapse() command?
  
        // for(size_t j=0; j<this->variations.size(); j++){
        // 	for(size_t i=0; i<this->locations.size(); i++){
        // 		TString path = TQFolder::concatPaths(variations[j],locations[i]);
        // 		for(size_t k=0; k<this->m_histNames.size(); k++){
        // 			TString histname = TQFolder::concatPaths(this->m_histNames[k],scanner->GetName());
        // 			TQCounterGrid* g = reader->getCounterGrid(path,histname);
        // 			if(!g){
        // 				std::cout << "ERROR in TSCLSignificanceEvaluator::initialize() : unable to retrieve TQCounterGrid " << histname << " from " << path << " in " << variations[i] << "!" << std::endl;
        // 				return false;
        // 			}
        // 			scanner->addGrid(g);
        // 			g->scale(this->luminosityScale);
        // 			g->compactify();
        // 		}
        // 	}
        // 	if(reader->getSampleFolder()->isLazy()){
        // 		TQFolder* f = reader->getSampleFolder()->getFolder(variations[j]);
        // 		if(f){
        // 			f->setExportName(f->getName());
        // 			reader->getSampleFolder()->collapse();
        // 		}
        // 	}
        // }

  m_nPointsProcessed = 0;
  this->engine = new TSStatisticsManager();
  this->engine->setHistogramsFileCompression(this->getTagIntegerDefault("tmpFileCompression",0));
  return true;
}

//___________________________________________________________________________________________

bool TSCLSignificanceEvaluator::hasNativeRegionSetHandling(){
	return this->supportRegionSetHandling;
}

//___________________________________________________________________________________________

bool TSCLSignificanceEvaluator::prepareNextRegionSet(const TString& suffix){
	if(0 == TQStringUtils::compare(this->initialization->GetName(),TQGridScanner::Class()->GetName())){
		if(this->prepareFromGridScanner(suffix) < 1){
			std::cout << "ERROR in TSCLSignificanceEvaluator('" << this->GetName() << "')::prepareNextRegionSet() : error preparing working folder from GridScanner!" << std::endl;
			return false;
		} 
		return true;
	} 
	std::cout<< "WARNING: TSCLSignificanceEvaluator('" << this->GetName() << "')::prepareNextRegionSet('" << suffix << "') : reached end of function without having found a matching initialization scheme. The initialization encountered was " << (this->initialization ? this->initialization->GetName() : "NULL") << std::endl;
	return false;
}

//___________________________________________________________________________________________

bool TSCLSignificanceEvaluator::isPrepared(){
  if(this->nPreparedRegionSets > 0){
    return true;
  } 
  return false;
}

void TSCLSignificanceEvaluator::setVerbose(bool v){
  // toggle verbosity
  this->verbose = v;
}

//___________________________________________________________________________________________

//-----------------------------------------------------------
// Dummy functions that shouldn't be used
double TSCLSignificanceEvaluator::getSignificance(size_t /*iregion*/) {
  return 0;
}

double TSCLSignificanceEvaluator::getSignificance2(size_t /*iregion*/) {
  return 0;
}

double TSCLSignificanceEvaluator::evaluate() {
  return {};
}
//-----------------------------------------------------------

#include "QFramework/TQFolder.h"
#include "QFramework/TQIterator.h"
#include "QFramework/TQUtils.h"
#include "SFramework/TSSystematicsManager.h"

#include "SFramework/TSStatisticsManager.h"

/*<cafdoc name=ProcessSystematics>
  
  ProcessSystematics
  ===========================
  
  Process systematic uncertainties in a model.  This action does
  nothing but invoke the `processSystematics` method of the
  TSSystematicsManager class. Please refer to the class documentation
  for details.

  Example usages iclude the following:
  ```
  # Prune all normalization systematics
  +ProcessSystematics.pruneNorm {
      +HWW_ggFVBF_01jDF {
        <select.Systematics={"*"}>
        <select.SystematicsTypes={"OverallSys"}> # default = {"OverallSys", "HistoSys"}
        <SysRelThreshold = 0.001> 
        <SysRelCutoff = 0.8>
        <SysSigMin = 0.2, SysSigMin.select.tag="isP4Sys"> 
        <SysAsymLimit = 0.5>
      }
  }
  
  # Perform smoothing on shapes, using the CommonSystSmoothingTool plugin
  +ProcessSystematics.smooth {
    +HWW_ggFVBF_01jDF {
      <histoSys.smooth = true>
      <select.Systematics = {"*"}>
      <histoSys.smoothingMethod = "smoothTtresDependent">
      <histoSys.smoothDirectionAndSymmetrize = "Up">
    }
  }

  # Force-symmetrize a few select systematics
  +ProcessSystematics.forceSymmetrizeSyst {
    +HWW_ggFVBF_01jDF {
      <select.Systematics={"ATLAS_JES_Flavor_Comp_top", "ATLAS_JES_PU_Rho_top"}>
      <histoSys.forceSymmetrizeMaxBins = true>
    }
  }

  # Automatically symmetrize all systematics
  +ProcessSystematics.symmetrizeSyst {
    +HWW_ggFVBF_01jDF {
      <histoSys.symmetrizeMaxBinsPrintWarnings = true> 
      <select.Systematics={"*"}>
      <histoSys.symmetrizeMaxBins = true>
     }
  }
  
  # Prune shape systematics
  +ProcessSystematics.pruneShapes {
    +HWW_ggFVBF_01jDF {
      <histoSys.prune = true>
      <histoSys.flatChi2pValMin = 0.05>
      <verbose=false>
      <select.Systematics={"ATLAS_*","HWW_FakeFactor*","theo_*"}>
      <select.SystematicsTypes={"HistoSys"}>    
    }
  }

  # Auto-prune shapes that are negilgible
  +ProcessSystematics.pruneNegligiblesVBF {
    +HWW_ggFVBF_01jDF {
      <select.Systematics={"theo_VBF_shower", "theo_VBF_matching", "ATLAS_JES*", "ATLAS_JER*", "ATLAS_MET*"}>
      <pruneNegligibles=true>
      <pruneNegligibles.Samples={"VBF_*"}>
      <pruneNegligibles.threshold = 0.05>        
    }
  }
```
</cafdoc> */

namespace TSBaseActions {

  class ProcessSystematics : public TSStatisticsManager::Action {

    bool execute(TQFolder * config) const override {

      TQFolder* model = models()->getFolder(config->GetName());
      if(!model){
        manager->error(TString::Format("no such model available: '%s'",config->GetName()));
        return false;
      }

      // get configured systematics
      std::vector<TString> selectedSystematics = config->getTagVString("select.Systematics");
      if(selectedSystematics.empty()) selectedSystematics.push_back("*");
      std::vector<TString> blacklistedSystematics = config->getTagVString("except.Systematics");
      std::vector<TString> selectedSamples = config->getTagVString("select.Samples");
      if(selectedSamples.empty()) selectedSamples.push_back("*");
      std::vector<TString> blacklistedSamples = config->getTagVString("except.Samples");

      TList* allSamples = model->getListOfFolders("?/?");
      if(!allSamples){
        manager->error(TString::Format("no samples in model '%s'",model->GetName()));
        return false;
      }
      
      TSSystematicsManager man;
      TQFolder * histos = model->getFolder(".Histograms+");
      man.setRepository(histos);
      if(config->getTagBoolDefault("includeSystematics",false)){
        man.includeAllSystematics(config,allSamples);
      } 
      if(config->getTagBoolDefault("processSystematics",true)){
        for(auto f : selectedSystematics){
          man.processAllSystematics(config,allSamples,f,blacklistedSamples, blacklistedSystematics);
        }
      }
      
      return true;
    }
  };
  namespace {
    bool available = TSStatisticsManager::registerAction(new ProcessSystematics(),"ProcessSystematics");
  }
}

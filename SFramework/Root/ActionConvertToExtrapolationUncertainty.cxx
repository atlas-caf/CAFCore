#include "QFramework/TQFolder.h"
#include "QFramework/TQIterator.h"
#include "QFramework/TQUtils.h"
#include "QFramework/TQTable.h"

#include "SFramework/TSStatisticsManager.h"

#include "TFile.h"
#include "TCanvas.h"
#include "TStyle.h"

#include <vector>
#include <algorithm>

/*<cafdoc name=ConvertToExtrapolationUncertainties>
  ConvertToExtrapolationUncertainties
  ===========================

  Usually, workspaces built with SFramework rely on the
  Normalization-Factor method of incorporating systematic
  uncertainties with normalization effects. This means that
  normalization effects of the systematic uncertainty are determined
  in both the signal and the control region, and it is left for the
  fit itself to resolve any cancellation between these from the
  observed correlations. However, under some circumstances, it can be
  desireable to explicitly convert normalization uncertainties to
  extrapolation factors by performing the cancellation "by hand" and
  removing the uncertainty in the control region. This is what this
  action is for.

  Usage:
  ---------------
  ```
  +ConvertToExtrapolationUncertainty.top {
    +HWWRun2GGF2J {
      +TopBTag {
        <source="Channel.ShapeTopCR_bTag">
        <sample="Sample.top">
	<sample.exactMatch=false> # default
	<verbose="true">
	<systematics={"theo_wz_*,"theo_ww_*"}>
	<writeTable="extrapolation_factors.txt">
      }
    }
  }
  ```


</cafdoc>*/

namespace{
  template<class X,class Y> bool in(const std::vector<X>& v, const Y& e){
    return std::find(v.begin(), v.end(), e) != v.end();
  }
}

namespace TSBaseActions {
  
  class ConvertToExtrapolationUncertainty : public TSStatisticsManager::Action {
    

    bool execute(TQFolder * config) const override {
      manager->info("ConvertToExtrapolationUncertainty(...): converting systematics");
      TQFolder* model = models()->getFolder(config->GetName());
      if(!model){
        manager->error(TString::Format("no such model available: '%s'",config->GetName()));
        models()->print();
        return false;
      }
      
      std::vector<TString> processedSystematics;
      
      TQFolderIterator itr(config->getListOfFolders());
      while (itr.hasNext()) {
        TQFolder* subconfig = itr.readNext();

        bool verbose = subconfig->getTagBoolDefault("verbose",false);
        
        TString sourceName;
        if (!subconfig->getTagString("source",sourceName)) {
          manager->error("Missing config option: 'source'");
          return false;
        }
        if(!sourceName.Contains("/")){
          TQStringUtils::ensureLeadingText(sourceName,"Channel.");
        }
        TQFolder* sourceChannel = model->getFolder(sourceName);
        if (!sourceChannel) {
          manager->error(TString::Format("Failed to retrieve source channel (region) '%s'!",sourceName.Data()));
          return false;
        }
        
        std::vector<TString> systematicNames = subconfig->getTagVString("systematic");
        if (systematicNames.size() < 1) {
          systematicNames = subconfig->getTagVString("systematics");
        }
        if (systematicNames.size() < 1 && (subconfig->hasTagString("systematic") || subconfig->hasTagString("systematics") ) ) {
          systematicNames.push_back(subconfig->getTagStringDefault("systematic",subconfig->getTagStringDefault("systematics","")));
        }
        if (systematicNames.size() < 1) {
          manager->error("Missing config option: 'systematic'/'systematics'");
          return false;
        }
        //safety check if systematic was already processed, e.g., for a different region (this would lead to unpredictable behavior!)
        for (size_t i=0; i<systematicNames.size(); i++) {
          TQStringUtils::ensureLeadingText(systematicNames[i],"OverallSys.");
          if (in(processedSystematics,systematicNames[i])) {
            manager->warn(TString::Format("Caught attempt to modify systematic '%s' more than once. carrying on for now, hoping that you know what you are doing!",systematicNames[i].Data()));
	  }
	  processedSystematics.push_back(systematicNames[i]);
        }
        
        bool sampleMatchExact = subconfig->getTagBoolDefault("sample.exactMatch",false);
        std::vector<TString> sampleNames = subconfig->getTagVString("sample");
        if (sampleNames.size() < 1) {
          sampleNames = subconfig->getTagVString("samples");
        }
        if (sampleNames.size() < 1 && (subconfig->hasTagString("sample") || subconfig->hasTagString("samples") ) ) {
          sampleNames.push_back(subconfig->getTagStringDefault("sample",subconfig->getTagStringDefault("samples","")));
        }
        if (sampleNames.size() < 1) sampleNames.push_back("*");
        for (size_t i=0; i<sampleNames.size(); i++) {
          TQStringUtils::ensureLeadingText(sampleNames[i],"Sample.");
        }

        bool symmetrize = subconfig->hasTagString("direction");
        TString direction;
        TString otherDirection;
        if (symmetrize) {
          subconfig->getTagString("direction",direction);
          if (!direction.EqualTo("High") && direction.EqualTo("Low")) {
            manager->error("Specified variation direction must be either 'High/Low' direction of systematic!");
          } 
          otherDirection = direction.EqualTo("High") ? "Low" : "High";
        }

        TString outTable;
        bool writeTable = subconfig->getTagString("writeTable",outTable);
        TQTable table;
        int row = 0;

        TQFolderIterator channelItr(model->getListOfFolders("Channel.*"));
        if(!channelItr.hasNext()){
          manager->error("unable to obtain channels!");
        }

	std::vector<TString> targets = subconfig->getTagVString("targets");
	if(targets.size() == 0) targets.push_back(subconfig->getTagStringDefault("target","*"));
	
        while(channelItr.hasNext()) {
          TQFolder* channel = channelItr.readNext();
          if (channel==sourceChannel || !channel) continue; //nothing to do YET on the source (CR) channel (we will delete the systematics there later on)
	  bool skip = true;
	  TString channelName (channel->GetName());
	  TQStringUtils::removeLeadingText(channelName,"Channel.");
	  for(const auto& target:targets){
	    if(TQStringUtils::matches(channelName,target)) skip=false;
	  }
	  if(skip) continue;
	  
          table.setEntry(row,0,"Systematic");
          table.setEntry(row,1,sourceChannel->getTagStringDefault("Channel",sourceChannel->GetName()));
          table.setEntry(row,3,channel->getTagStringDefault("Channel",channel->GetName()));
          table.setEntry(row,5,"Extrapolation");
          ++row;
          
          for (size_t sampleCounter=0; sampleCounter<sampleNames.size(); sampleCounter++) {
            TQFolderIterator sampleItr(channel->getListOfFolders(sampleNames[sampleCounter]));
            if(!sampleItr.hasNext()){
              manager->error(TString::Format("unable to obtain samples matching '%s'!",sampleNames[sampleCounter].Data()));
            }
            while(sampleItr.hasNext()) {
              TQFolder* sample = sampleItr.readNext();
              TString matchname = sampleMatchExact ? sample->GetName() : sampleNames[sampleCounter].Data();
              TQFolder* sourceSample = sourceChannel->getFolder(matchname);
              if (!sourceSample || !sample) continue;
              for (size_t systCounter=0; systCounter<systematicNames.size(); systCounter++) {
                TQFolderIterator systItr(sample->getListOfFolders(systematicNames[systCounter]));
                if(!systItr.hasNext()){
                  manager->error(TString::Format("unable to obtain systematics matching '%s' in '%s'!",systematicNames[systCounter].Data(),sample->getPath().Data()));
                }                
                while(systItr.hasNext()) {
                  TQFolder* systematic = systItr.readNext();
                  TQFolder* sourceSystematic = sourceSample->getFolder(systematic->GetName());
                  manager->info(TString::Format("Source systematic: '%s'",systematic->getPath().Data()));
                  if (!systematic || !sourceSystematic) continue;
                  // variation specified -- extrapolate, than symmetrie to other direction
                  if (symmetrize) {
                    double sVar = 1; 
                    double tVar = 1; 
                    double tOtherVar = 1; 
                    double newVar = 1; 
                    double newOtherVar = 1;
                    if (!sourceSystematic->getTagDouble(direction,sVar)) {
                      manager->error(TString::Format("Failed to retrieve source variations for '%s'!",sourceSystematic->GetName()));
                      continue;
                    }
                    if (sVar == 1.) {
                      manager->error(TString::Format("A source variation for '%s' is zero, skipping this variation.",sourceSystematic->GetName()));
                    } else {
                      if (systematic->getTagDouble(direction,tVar) && systematic->getTagDouble(otherDirection,tOtherVar)) {
                        newVar = tVar/sVar;
                        newOtherVar = 2-newVar;
                        systematic->setTagDouble(direction,newVar);
                        systematic->setTagDouble(TString::Format(".orig.%s",direction.Data()),tVar);                    
                        systematic->setTagDouble(otherDirection,newOtherVar);
                        systematic->setTagDouble(TString::Format(".orig.%s",otherDirection.Data()),tOtherVar);                    
                      }
                    }
                    double percent = 50*(fabs(newVar/sVar-1.)+fabs(newOtherVar/sVar-1.));                  
                    systematic->setTagDouble("Percent",percent);
                    table.setEntry(row,0,systematic->getTagStringDefault("Systematic",systematic->GetName()));
                    table.setEntryValue(row,1,100.*(sVar-1));
                    table.setEntryValue(row,2,100.*(1-sVar));
                    table.setEntryValue(row,3,100.*(tVar-1));
                    table.setEntryValue(row,4,100.*(tOtherVar-1));
                    table.setEntryValue(row,5,100.*(newVar-1));
                    table.setEntryValue(row,6,100.*(newOtherVar-1));                                                      
                    ++row;
                    sourceSystematic->setTagBool(".converted.extrapolation",true);
                    systematic->setTagBool(".converted.extrapolation",true);
                    manager->info(TString::Format("  Extrapolated systematic: '%s'",systematic->getPath().Data()));
                    if(verbose){
                      manager->info(TString::Format("    %s = %g (/) %g = %g",direction.Data(),tVar,sVar,newVar));
                      manager->info(TString::Format("    %s = %g (/) %g = %g",otherDirection.Data(),tOtherVar,sVar,newOtherVar));
                    }
                  // no variation specified -- extrapolate both directions separately
                  } else {
                    double sUp = 1;
                    double sDown = 1;
                    double tUp = 1;
                    double tDown = 1;
                    double newUp = 1;
                    double newDn = 1;
                    if (!sourceSystematic->getTagDouble("High",sUp) || !sourceSystematic->getTagDouble("Low",sDown)) {
                      manager->error(TString::Format("Failed to retrieve source variations for '%s'!",sourceSystematic->GetName()));
                      continue;
                    }
                    if (sUp == 1.) {
                      manager->error(TString::Format("A source variation for '%s' is zero, skipping this variation.",sourceSystematic->GetName()));
                    } else {
                      if (systematic->getTagDouble("High",tUp)) {
                        newUp = tUp/sUp;
                        systematic->setTagDouble("High",newUp);
                        systematic->setTagDouble(".orig.High",tUp);                    
                      }
                    }
                    if (sDown == 1.) {
                      manager->error(TString::Format("A source variation for '%s' is zero, skipping this variation.",sourceSystematic->GetName()));
                    } else {                  
                      if (systematic->getTagDouble("Low",tDown)) {
                        newDn = tDown/sDown;
                        systematic->setTagDouble("Low",newDn);
                        systematic->setTagDouble(".orig.Low",tDown);                                        
                      }
                    }
                    double percent = 50*(fabs(tDown/sDown-1.)+fabs(tUp/sUp-1.));                  
                    systematic->setTagDouble("Percent",percent);
                    table.setEntry(row,0,systematic->getTagStringDefault("Systematic",systematic->GetName()));
                    table.setEntryValue(row,1,100.*(sUp-1));
                    table.setEntryValue(row,2,100.*(sDown-1));
                    table.setEntryValue(row,3,100.*(tUp-1));
                    table.setEntryValue(row,4,100.*(tDown-1));
                    table.setEntryValue(row,5,100.*(newUp-1));
                    table.setEntryValue(row,6,100.*(newDn-1));                                                      
                    ++row;
                    sourceSystematic->setTagBool(".converted.extrapolation",true);
                    systematic->setTagBool(".converted.extrapolation",true);
                    manager->info(TString::Format("  Extrapolated systematic: '%s'",systematic->getPath().Data()));
                    if(verbose){
                      manager->info(TString::Format("    High = %g (/) %g = %g",tUp,sUp,newUp));
                      manager->info(TString::Format("    Low = %g (/) %g = %g",tDown,sDown,newDn));
                    }
                  }
                }
              }
            }
          }
        }
        manager->info("ConvertToExtrapolationUncertainty(...): cleaning up source systematics...");
        //cleanup of source systematics:        
        for (size_t sampleCounter=0; sampleCounter<sampleNames.size(); sampleCounter++) {
          TQFolderIterator sampleItr(sourceChannel->getListOfFolders(sampleNames[sampleCounter]));
          while(sampleItr.hasNext()) {
            TQFolder* sample = sampleItr.readNext();
            if (!sample) continue;
            for (size_t systCounter=0; systCounter<systematicNames.size(); systCounter++) {
              TQFolderIterator systItr(sample->getListOfFolders(systematicNames[systCounter]));
              while(systItr.hasNext()) {
                TQFolder* systematic = systItr.readNext();
                if (!systematic) continue;
                if (systematic->getTagBoolDefault(".converted.extrapolation",false)) {
                  systematic->getFolder("../.converted+"); //ensure a ".converted" folder exists
                  systematic->moveTo("../.converted"); //move systematic
                }
              }
            }
          }
        }

        if(writeTable){
          table.write(outTable);
        }
      }
      return true;
    }
  };
  namespace {
    bool available = TSStatisticsManager::registerAction(new ConvertToExtrapolationUncertainty(),"ConvertToExtrapolationUncertainty");
  }
}
    

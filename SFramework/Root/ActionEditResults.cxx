#include "QFramework/TQFolder.h"
#include "QFramework/TQIterator.h"
#include "QFramework/TQUtils.h"

#include "SFramework/TSStatisticsManager.h"

/*<cafdoc name=EditResults>
  EditResults
  ===========================
  
  Edit a result using some `TQFolder` edit commands. These can either be
  supplied in-place using the tag `commands`, or from some internal
  file using the tag `file`.
  
  The edits will be made in-place on the result.

</cafdoc> */

namespace TSBaseActions {

  class EditResult : public TSStatisticsManager::Action {

    bool execute(TQFolder * config) const override {

      TQFolder* result = results()->getFolder(config->GetName());
      if(!result){
        manager->error(TString::Format("no such result available: '%s'",config->GetName()));
        return false;
      }

      auto edits = config->getTagVString("applyEdits");
      for(auto edit:edits){
        manager->info(TString::Format("applying edit '%s'",edit.Data()));
        result->importFromText(edit);
      }

      result->sortByNameRecursive();      
      return true;
    }
  };
  namespace {
    bool available = TSStatisticsManager::registerAction(new EditResult(),"EditResults");
  }
}

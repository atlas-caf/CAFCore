#include "QFramework/TQFolder.h"
#include "QFramework/TQIterator.h"
#include "QFramework/TQUtils.h"

#include "SFramework/TSUtils.h"
#include "SFramework/TSStatisticsManager.h"

#include "RooRealVar.h"
#include "RooStats/ModelConfig.h"


/*<cafdoc name=TakeSnapshot>
  TakeSnapshot
  ===========================
  
  Take a snapshot of the current status of a `RooFit` workspace.
  
  Usage:
  ---------------
  ```
  +TakeSnapshot {
    +Morphing {
      +SnSh_SM {
        <base="SnSh_AllVars_Nominal">
        <setPars.AL=1.0>
        <setPars.AT=1.0>
      }
    }
  }
  ```
  
  One or more snapshots can be given as subfolders. Optionally, a
  `base` snapshot can be given. If this is omitted, the current status
  is used (and a warning is emitted).
  
  The tags `setPars`, `constPars` and `floatPars` can be used to edit
  parameters before taking the snapshot.

  The tag `fitResult` can be used to load the values of an existing
  fit result (stored as a result to this or any other workspace in
  memory) and save it as a snapshot. This feature can be used to
  transfer fit results from one workspace to another, by loading a
  different, unrelated fit result and importing it to a workspace as a
  snapshot.

  The tag `writeFitResult` can be used to write a reflection of the
  snapshot as a fit result.

</cafdoc> */

class ActionTakeSnapshot : public TSStatisticsManager::Action {
  
  bool execute(TQFolder * config) const override {
    RooWorkspace * workspace = dynamic_cast<RooWorkspace*>(workspaces()->getObject(config->GetName()));
    if(!workspace){
      manager->error(TString::Format("unable to take snapshot for '%s': no such workspace!",config->GetName()));
      return false;
    }
    //RooStats::ModelConfig* mc = dynamic_cast<RooStats::ModelConfig*>(workspace->obj(config->getTagStringDefault("modelConfig","ModelConfig")));
    RooArgSet allVars(workspace->allVars());
    auto old = TSUtils::getParameterValues(allVars);
    std::map<TString,bool> isConst;
    for(auto& p:allVars){
      isConst[p->GetName()] = (dynamic_cast<const RooRealVar*>(p))->isConstant();
    }
    TQFolderIterator itr(config->getListOfFolders("?"));
    int count = 0;
    while(itr.hasNext()){
      TQFolder* snsh = itr.readNext();
      if(!snsh) continue;
      TString name = snsh->getTagStringDefault("name",snsh->GetName());
      TString base;
      if(!snsh->getTagString("base",base)){
        manager->warn(TString::Format("no 'base' given for snapshot '%s', using current workspace status instead",name.Data()));
      } else {
        if(!workspace->loadSnapshot(base)){
          manager->error(TString::Format("cannot load base '%s' for snapshot '%s'",base.Data(),name.Data()));
          continue;
        }
      }
      
      TSUtils::setParametersConstant(&allVars,snsh->getTagVString("constPars"),true);
      TSUtils::setParametersConstant(&allVars,snsh->getTagVString("floatPars"),false);
      TSUtils::setParameterValues(&allVars,snsh,"setPars",true);
      TString fitresult;
      if(snsh->getTagString("fitResult",fitresult)){
        TQFolder* result = results()->getFolder(config->GetName());
        TQFolder* myresult = result ? result->getFolder(fitresult) : results()->getFolder(fitresult);
        if(myresult){
          TQFolder* newresult = TSUtils::applyFitResult(&allVars,myresult);
          TString writeFitResult;
          if(snsh->getTagString("writeFitResult",writeFitResult)){
            TString name = TQFolder::getPathTail(writeFitResult);
            newresult->SetName(name);
            TQFolder* target = results()->getFolder(writeFitResult+"+");
            target->addFolder(newresult);
          } else {
            delete newresult;
          }
        } else {
          manager->error(TString::Format("unable to find result '%s'",fitresult.Data()));
        }
      } 
      if ( TSUtils::takeSnapshot(workspace, name, snsh, manager) ){
	TSUtils::convertParameterList(&allVars,manager->getResults()->getFolder(TQFolder::concatPaths(workspace->GetName(),"Snapshots",name)+"+"));
	++count;
      }
      TSUtils::setParameterValues(allVars,old);
      for(auto& p:isConst){
	(dynamic_cast<RooRealVar*>(allVars.find(p.first.Data())))->setConstant(p.second);
      }
    }
    if(count == 0){
      manager->error("no valid configurations found!");
      return false;
    }
    return true;
  }
};
namespace {
  bool available = TSStatisticsManager::registerAction(new ActionTakeSnapshot(),"TakeSnapshot");
}

  

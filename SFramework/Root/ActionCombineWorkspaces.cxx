#include "QFramework/TQFolder.h"
#include "QFramework/TQIterator.h"
#include "QFramework/TQUtils.h"
#include "QFramework/TQPathManager.h"

#include "SFramework/TSStatisticsManager.h"
#include "SFramework/TSUtils.h"

#include <stdexcept>

#include "RooWorkspace.h"
#include "RooStats/ModelConfig.h"
#include "RooSimultaneous.h"
#include "RooDataSet.h"
#include "RooCategory.h"


//#ifdef ROOTCORE
//#include <RootCore/Packages.h>
//#endif
#ifdef HAS_RooFitUtils
#include "RooFitUtils/AbsMeasurement.h"
#include "RooFitUtils/CorrelationScheme.h"
#include "RooFitUtils/CombinedMeasurement.h"
#include "RooFitUtils/Measurement.h"
#endif

/*<cafdoc name=CombineWorkspaces>
  CombineWorkspaces
  ===========================
  
  Combine several workspaces to a combined measurement.
  
  Usage:
  ---------------
  ```
  +CombineWorkspaces {
    +HWWRun2 {
      <verbose=true,asimov=false>
      <correlationSchemes = {
        "config/statistics/comb/snippets/correlations.ggF.txt",
        "config/statistics/comb/snippets/correlations.vbf.txt"
      }>
      +HWWRun2GGF {
        <SnapshotName="SnSh_AllVars_Nominal",DataName="obsData">
      }
      +HWWRun2VBF {
        <SnapshotName="NominalParamValues",DataName="obsData">
      }
      <BinnedLikelihood=true> @ ?;
    }
  }
  ```
  
  This Action internally uses the `RooFitUtils` extension package and
  will fail with a runtime error if that is not present.

  Options include:

    * `correlationSchemes`: List of correlation schemes. These are
       text files with lines in the format
       ```
       WorkspaceName::OldParameterName>>NewParameterName
       ```
       These define which parameters from which workspaces should be
       correlated with each other by renaming them.
    * `autocorrelation`: Set to true to auto-correlate all parameters
       with the same name from different input workspaces.
    * `verbose`: Set to true to get verbose output.
    * `asimov`: Set to true to generate an asimov dataset on the output workspace.
    * `writeTable`: Provide a file name to write a table of which
      parameters were correlated in the end.

  Each workspace to be combined should be provided as one
  subfolder. Options on the input workspaces include
    * `SnapshotName`: Set the Snapshot name to be loaded before combining.
    * `DataName`: Set the name of the dataset the be used for the combination.
    * `BinnedLikelihood`: Activate BinnedLikelihood for the input measurement.


</cafdoc> */

namespace TSBaseActions {

  class CombineWorkspaces : public TSStatisticsManager::Action {

    RooArgSet filter(const RooArgSet& args, RooWorkspace* ws) const {
      RooArgSet result;
      for(auto arg:args){
	if(ws->var(arg->GetName())){
	  result.add(*arg);
	}
      }
      return result;
    }
    
#ifdef HAS_RooFitUtils
    RooFitUtils::CombinedMeasurement::SnapshotName getSnapshot(TString name) const {
      name.ToLower();
      if(name == "nominal"){
        return RooFitUtils::CombinedMeasurement::nominal;
      }
      if(name == "umcles"){
        return RooFitUtils::CombinedMeasurement::ucmles;
      }
      if(name == "background"){
        return RooFitUtils::CombinedMeasurement::background;
      }
      throw std::runtime_error(TString::Format("unknown snapshot name: %s",name.Data()).Data());
    }
#endif
    
    bool execute(TQFolder * config) const override {
      bool useRooFitUtils = config->getTagBoolDefault("useRooFitUtils",true);
      if(useRooFitUtils){
#ifndef HAS_RooFitUtils
	manager->error("unable to combine workspaces -- this feature relies on the RooFitUtils package!");
	return false;
#else
        // Create a new combined measurement
        TString combwsname(config->GetName());
        TString combdataname(config->getTagStringDefault("dataName","obsData"));
        TString combmcname("ModelConfig");
        TString combcatname(config->getTagStringDefault("categoryName","channelCat"));
  
        bool verbose = config->getTagBoolDefault("verbose",false);
        
        RooFitUtils::CombinedMeasurement combined(combwsname.Data(),combwsname.Data(),combmcname.Data(),combdataname.Data());
        combined.SetNameCategory(combcatname.Data());
  
        // Define a correlation scheme that should be used when combining the specified
        // measurements. Parameters in the scheme that are not present in the
        // measurements will be ignored.
        RooFitUtils::CorrelationScheme correlation("CorrelationScheme");
        bool autocorr = config->getTagBoolDefault("autocorrelation",false);
        if(autocorr){
	  manager->warn("using autocorrelation to combine!");
        }
        correlation.SetAutoCorrelation(autocorr);

	std::vector<std::string> measurements;

	std::map<std::string,double> values;
        TQFolderIterator wsconfigs(config->getListOfFolders("?"));
        while(wsconfigs.hasNext()){
          TQFolder* wsconfig = wsconfigs.readNext();
          if(!wsconfig) continue;
  
          TString wsname = wsconfig->getTagStringDefault("WorkspaceName",wsconfig->GetName());
  
          bool binned;
          if(!wsconfig->getTagBool("BinnedLikelihood",binned)){
            manager->error(TString::Format("for measurement '%s', please specify 'BinnedLikelihood' (bool)",wsconfig->GetName()));
            continue;
          }

          RooWorkspace* ws = dynamic_cast<RooWorkspace*>(workspaces()->getObject(wsname));
          if(!ws){
            manager->error(TString::Format("unable to obtain workspace '%s'!",wsname.Data()));
            continue;
          }
	  
          TString snapshotName;
          if(!wsconfig->getTagString("SnapshotName",snapshotName)){
            manager->error(TString::Format("for measurement '%s', please specify 'SnapshotName' (string)",wsconfig->GetName()));
            continue;
          } else {
            manager->info(TString::Format("for measurement '%s', loading snapshot '%s'",wsconfig->GetName(), snapshotName.Data()));
	  }
	  
          TString modelconfig = wsconfig->getTagStringDefault("ModelConfigName","ModelConfig");
          TString dataname;
          if(!wsconfig->getTagString("DataName",dataname)){
            manager->error(TString::Format("for measurement '%s', please specify 'DataName' (string)",wsconfig->GetName()));
            continue;
          }
  
	  RooStats::ModelConfig* mc = dynamic_cast<RooStats::ModelConfig*>(ws->obj(modelconfig));
          if(!mc){
            manager->error(TString::Format("workspace '%s' has not ModelConfig!",wsname.Data()));
            continue;
          }
          if(!mc->GetPdf()){
            manager->error(TString::Format("ModelConfig in workspace '%s' has no Pdf assigned!",wsname.Data()));
            continue;
          }
  
          manager->info(TString::Format("creating measurement for '%s'",wsconfig->GetName()));
          RooFitUtils::Measurement* measurement = new RooFitUtils::Measurement (wsconfig->GetName(),ws,modelconfig.Data(),dataname.Data(),snapshotName.Data(),binned);
          if(!measurement){
            manager->error(TString::Format("unable to create measurement from workspace config '%s'!",wsconfig->GetName()));
            continue;
          }

          TString channelFilter;
          if (wsconfig->getTagString("ChannelFilter",channelFilter))
          {
            manager->info(TString::Format("filtering channels matching '%s'",channelFilter.Data()));
            measurement->SetChannelFilter(std::string(channelFilter.Data()));
          }

          combined.AddMeasurement(measurement);
          measurements.push_back(wsconfig->GetName());
  
          TQIterator corrtags(wsconfig->getListOfKeys("merge.*.0"));
          while(corrtags.hasNext()){
            TString key(corrtags.readNext()->GetName());
            TQStringUtils::removeLeadingText(key,"merge.");
            TString pname;
            TQStringUtils::readUpTo(key,pname,".");
            std::vector<TString> params = wsconfig->getTagVString("merge."+pname);
            manager->info(TString::Format("obtaining merged parameter '%s' from %s",pname.Data(),TQStringUtils::concat(params,",").Data()));
            for(auto& p:params){
              p.Prepend("::");
              p.Prepend(wsconfig->GetName());            
            }
            TString paramlist(TQStringUtils::concat(params,","));
            correlation.CorrelateParameter(paramlist,pname);
          }
	  
	  ws->loadSnapshot(snapshotName);
	  for(auto* var: ws->allVars()){
	    values[var->GetName()] = static_cast<RooRealVar*>(var)->getVal();
	  }
        }
  
        manager->info("applying correlation scheme");
  
        // Define parameters of interest for the combined measurement. Only parameters
        // present in the final workspace will be considered.
        std::vector<TString> pois = config->getTagVString("POI");
        if(pois.empty()) pois.push_back("mu");
        TString poilist = TQStringUtils::concat(pois,",");
        correlation.SetParametersOfInterest(poilist.Data());
	
        std::vector<TString> correlate = config->getTagVString("correlate");
	
        for(auto file_path:config->getTagVString("correlationSchemes")){
	  bool ok = TQStringUtils::readFileLines(&correlate,TQPathManager::getPathManager()->findConfigPath(file_path),2048,true);
	  if(ok){
	    manager->info("importing correlations from "+file_path);
	  } else {
	    manager->warn("did not find any correlations in "+file_path);
	  }
        }

	manager->info(TString::Format("correlating %d parameters", (int)correlate.size()));
	
        for(auto it:correlate){
	  if(it.Contains(">")){
	    TString orig;
	    TQStringUtils::readUpTo(it,orig,">>");
	    TQStringUtils::removeLeading(it,">");
	    correlation.CorrelateParameter(orig,it);
	  } else {
	    correlation.CorrelateParameter(it,it);
	  }
        }
	
        if(verbose){
          correlation.Print();
        }
  
        // Use the correlation scheme for the combined measurement.
        combined.SetCorrelationScheme(&correlation);

        manager->info("combining measurements");

        try {
          combined.CollectMeasurements();
          combined.CombineMeasurements();
        } catch(std::exception& e){
          manager->error(e.what());
          return false;
        }
  
        if(verbose){
          combined.Print();
        }
        
        if(config->getTagBoolDefault("asimov",false)){
          manager->info("creating asimov data");        
          combined.MakeAsimovData(config->getTagBoolDefault("asimov.conditional",false),
                                   getSnapshot(config->getTagStringDefault("asimov.profile","nominal")),
                                   getSnapshot(config->getTagStringDefault("asimov.generate","nominal")));
        }
  
        manager->info("obtaining combined workspace");              
        RooWorkspace* ws = combined.GetWorkspace();
        if(!ws){
          manager->error("unable to combine workspaces!");
          return false;
        }

	for(auto v:values){
	  RooRealVar* var = ws->var(v.first.c_str());
	  if(var){
	    var->setVal(v.second);
	  }
	}
	for(auto v:ws->allVars()){
	  auto* var = static_cast<RooRealVar*>(v);
	  var->setVal(var->getVal());
	}
	ws->saveSnapshot("CombinedMeasurement",ws->allVars());

        RooStats::ModelConfig* newMC = (RooStats::ModelConfig*)(ws->obj("ModelConfig"));
  
        TSUtils::applySettings(config,ws->allVars(),newMC);

        workspaces()->addObject(ws);
  
        TString outfile;
        if(config->getTagString("writeTable",outfile)){
          manager->info(TString::Format("writing correlation table to %s",outfile.Data()));
          correlation.printToFile(outfile,true);
        }

	TString outscheme;
        if(config->getTagString("writeScheme",outscheme)){
          manager->info(TString::Format("writing correlation scheme to %s",outscheme.Data()));
	  std::ofstream out(outscheme);
	  for(const auto& meas:measurements){
	    for(const auto& par:correlation.GetRenamingMap(meas).GetRenamingMap()){
	      out << meas << "::" << par.first << ">>" << par.second << "\n";
	    }
	  }
        }
  
        return true;
#endif
      } else {
	manager->info("performing a poor-man's combination, just merging the workspaces");

        RooWorkspace* combined = new RooWorkspace(config->GetName());
        TQFolderIterator wsconfigs(config->getListOfFolders("?"));
	std::map<std::string,RooAbsPdf*> components;
	std::map<std::string, RooDataSet *> datasets;		
	RooCategory cat("indexCat","indexCat");
	RooStats::ModelConfig* combMC = new RooStats::ModelConfig("ModelConfig","ModelConfig");

	RooArgSet nps;
	RooArgSet pois;
	RooArgSet obs;
	RooArgSet globs;			
	
	combMC->SetWS(*combined);
        while(wsconfigs.hasNext()){
          TQFolder* wsconfig = wsconfigs.readNext();
          if(!wsconfig) continue;
	  TString name(wsconfig->GetName());
	  TString dataname = wsconfig->getTagStringDefault("DataName","obsData");
	  RooWorkspace* ws = (RooWorkspace*)workspaces()->getObject(name);
	  if(!ws){
	    manager->info(TString::Format("unable to get workspace '%s'",name.Data()));
	  }
	  RooStats::ModelConfig* mc = (RooStats::ModelConfig*)(ws->obj("ModelConfig"));
	  if(mc->GetParametersOfInterest()) pois.add(*mc->GetParametersOfInterest());
	  if(mc->GetNuisanceParameters()) nps.add(*mc->GetNuisanceParameters());
	  if(mc->GetObservables()) obs.add(*mc->GetObservables());
	  if(mc->GetGlobalObservables()) globs.add(*mc->GetGlobalObservables());	  
	  RooAbsPdf* pdf = mc->GetPdf();
	  if(pdf->InheritsFrom(RooSimultaneous::Class())){
	    RooSimultaneous* simPdf = dynamic_cast<RooSimultaneous*>(pdf);

	    TList* dataList = ws->data(dataname.Data())->split(simPdf->indexCat(), true);
	    int bins = simPdf->indexCat().numBins("");
	    for(int i=0; i<bins; ++i){
	      const_cast<RooAbsCategoryLValue&>(simPdf->indexCat()).setBin(i);
#if ROOT_VERSION_CODE < ROOT_VERSION(6,24,0)
	      auto label = simPdf->indexCat().getLabel();
#else
	      auto label = simPdf->indexCat().getCurrentLabel();
#endif
	      RooAbsPdf* thispdf = simPdf->getPdf(label);
	      components[label] = thispdf;
	      datasets[label] = (RooDataSet*)(dataList->At(i));
	      cat.defineType(label);
	    }
	    delete dataList;
	  } else {
	    components[name.Data()] = pdf;
	    datasets[name.Data()] = (RooDataSet*)(ws->data(dataname.Data()));
	    cat.defineType(name);
	  }
	}
	RooSimultaneous simPdf("simPdf","simPdf",components,cat);
	combined->import(simPdf,RooFit::RecycleConflictNodes());
	combMC->SetParametersOfInterest(filter(pois,combined));
	combMC->SetNuisanceParameters(filter(nps,combined));
	combMC->SetObservables(filter(obs,combined));
	combMC->SetGlobalObservables(filter(globs,combined));
	
	combMC->SetPdf(*(combined->pdf("simPdf")));
	combined->import(*combMC);
	workspaces()->addObject(combined);


	RooArgSet obs_cat_weight(*combMC->GetObservables());
	obs_cat_weight.add(simPdf.indexCat());
	obs_cat_weight.add(*combined->factory("weightVar[0.]"));
	RooDataSet data("obsData","obsData", obs_cat_weight,
			RooFit::Index(*combined->cat("indexCat")), RooFit::Import(datasets),
			RooFit::WeightVar("weightVar"));
	combined->import(data);
	return true;
      }
    }
  };
  namespace {
    bool available = TSStatisticsManager::registerAction(new CombineWorkspaces(),"CombineWorkspaces");
  }
}

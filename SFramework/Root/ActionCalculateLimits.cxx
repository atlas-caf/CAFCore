#include "QFramework/TQFolder.h"
#include "QFramework/TQIterator.h"
#include "QFramework/TQPathManager.h"
#include "QFramework/TQLibrary.h"
#include "QFramework/TQUtils.h"
#include "SFramework/TSStatisticsManager.h"

#include "SFramework/TSLimitCalculator.h"
#include "SFramework/TSUtils.h"

#include "TCanvas.h"
#include "TLatex.h"
#include "RooStats/SamplingDistPlot.h"

/*<cafdoc name=CalculateLimit>
  CalculateLimit
  ===========================
  
  Perform a hypothesis test using the asymptotic-formulae approach
  (https://arxiv.org/abs/1007.1727), calculating the significiance of
  a discovery.

  The result of the calculation will be stored in the results folder
  and printed to the console.

  Usage:
  ---------------
  ```
  +CalculateLimit.myCalc {
    # The name of the object to be written can be set as the suffix of
    #  the action name, or with a `name` tag.
    +Test{
      +expected {
        <dataset="asimovData">
        <nPoints=50>
        <poi.min=0.>
        <poi.max=6.>
        # define the null hypothesis
        +Parameters/H0 {
          <mu=0>
        }
      }    
    }
  }
  ```

  The options include:
    * `dataset`: The name of the dataset to fit.
    * `confidenceLevel`: (default=`0.9`) The confidence level for which to calculate the limit. 
    * `useCLs`: (default=`true`) toggle whether to use CLs
    * `oneSided`: (default=`true`) Calculate one-sided limits
    * `poi.min`: minimum value of the POI to be used
    * `poi.max`: minimum value of the POI to be used
    * `nPoints`: number of points to evalute the POI for
  
</cafdoc>
*/


namespace TSBaseActions {

  class ActionCalculateLimit : public TSStatisticsManager::Action {

    bool execute(TQFolder * config) const override {

      TString wsname = config->getTagStringDefault("workspace",config->GetName());
      RooWorkspace * workspace = dynamic_cast<RooWorkspace*>(workspaces()->getObject(wsname));
      if(!workspace){
        manager->error(TString::Format("no such workspace available: '%s'",config->GetName()));
        return false;
      }
      
      TString resultname = config->getTagStringDefault("result",config->GetName());
      TQFolder * result = results()->getFolder(TString::Format("%s+",resultname.Data()));
      
      TSLimitCalculator lc(workspace);
      
      TQFolderIterator itr(config->getListOfFolders("?"));
      while(itr.hasNext()){
        TQFolder* cfg = itr.readNext();
        manager->info(TString::Format("Calculating limit '%s' on workspace '%s'",cfg->GetName(),config->GetName()));
        
        TString name = cfg->getTagStringDefault("name",cfg->GetName());
        
        TQFolder* limit = lc.runCalculation(cfg);
        if (limit){
          limit->SetName(name);
          result->getFolder("Limits+")->addObject(limit);
        } else {
          manager->error(TString::Format("%s: Limit calculation failed",config->GetName()));
        }
      }
      return true;
    }
  };
  namespace {
    bool available = TSStatisticsManager::registerAction(new ActionCalculateLimit(),"CalculateLimit");
  }
}

#ifdef HAS_CommonStatTools
#include "AsymptoticsCLsRunner.h"
#pragma message "using CommonStatTools AsymptoticCLsRunner"


/*<cafdoc name=CalculateLimitAsymptoticCLs>
  CalculateLimitAsymptoticCLs
  ===========================
  
  Perform a hypothesis test using the asymptotic-formulae approach
  (https://arxiv.org/abs/1007.1727), calculating the significiance of
  a discovery.

  The result of the calculation will be stored in the results folder
  and printed to the console.

  Usage:
  ---------------
  ```
  +CalculateLimit.myCalc {
    # The name of the object to be written can be set as the suffix of
    #  the action name, or with a `name` tag.
    +Test{
      +expected {
        <dataset="asimovData">
        <nPoints=50>
        <poi.min=0.>
        <poi.max=6.>
        # define the null hypothesis
        +Parameters/H0 {
          <mu=0>
        }
      }    
    }
  }
  ```

  The options include:
    * `dataset`: The name of the dataset to fit.
    * `asimov` (default='' means "create new asimov dataset"): The name of the asimov dataset to fit.  
    * `confidenceLevel`: (default=`0.9`) The confidence level for which to calculate the limit. 
    * `blinded`: (default=`false`) run the analysis blinded
    * `injection`: (default=`false`) Perform injection
    * `injection.value`: value to be injected
</cafdoc>
*/

namespace TSAdvancedActions {
 class ActionRunAsymptoticCLs : public TSStatisticsManager::Action {
    bool execute(TQFolder * config) const override {

      TString wsname = config->getTagStringDefault("workspace",config->GetName());
      RooWorkspace * workspace = dynamic_cast<RooWorkspace*>(workspaces()->getObject(wsname));
      if(!workspace){
        manager->error(TString::Format("no such workspace available: '%s'",config->GetName()));
        return false;
      }
      
      TString resultname = config->getTagStringDefault("result",config->GetName());
      TQFolder * result = results()->getFolder(TString::Format("%s+",resultname.Data()))->getFolder("Limits+");
      
      manager->info(TString::Format("Running asymptotics on '%s' on workspace '%s'",config->GetName(),config->GetName()));

      TString mcname = config->getTagStringDefault("modelConfig","ModelConfig");
      TString dataname = config->getTagStringDefault("dataset","obsData");
      TString asimovname = config->getTagStringDefault("asimov","");
      double confidenceLevel = config->getTagDoubleDefault("confidenceLevel",.9);	
      
      TString outfile;
      bool redirect=config->getTag("fit.logToFile",outfile);
      if(redirect){
	TString fname = config->replaceInText(outfile);
	fname = TQPathManager::getPathManager()->getTargetPath(fname);
	TQUtils::ensureDirectoryForFile(fname);
	manager->info(TString::Format("writing log of minimization to '%s'",fname.Data()));
	TQLibrary::redirect(fname,true);
      }
      
      EXOSTATS::AsymptoticsCLsRunner::Config cfg;
      cfg.m_doBlind = config->getTagBoolDefault("blinded",false);
      cfg.m_doInj = config->getTagBoolDefault("injection",false);
      cfg.m_muInjection = config->getTagBoolDefault("injection.value",false);      
      cfg.m_debugLevel = config->getTagIntegerDefault("verbose",1);
      EXOSTATS::AsymptoticsCLsRunner runner(cfg);
        
      runner.prepareLimit(workspace, mcname.Data(), dataname.Data(), asimovname.Data());
      auto clsLimit = runner.computeLimit(confidenceLevel);

      TQLibrary::restore();
      
      clsLimit.point = config->getTagDoubleDefault("point",0.);

      TQFolder* obslimit = result->getFolder("observed+");
      TQFolder* explimit = result->getFolder("expected+");      
      obslimit->setTagDouble("point",clsLimit.point);
      explimit->setTagDouble("point",clsLimit.point);      
      
      explimit->setTagDouble("CLb_med",clsLimit.CLb_med);
      explimit->setTagDouble("CLs_med",clsLimit.CLs_med);
      explimit->setTagDouble("CLsplusb_med",clsLimit.CLsplusb_med);
      explimit->setTagDouble("pb_med",clsLimit.pb_med);
      
      obslimit->setTagDouble("CLb_obs",clsLimit.CLb_obs);
      obslimit->setTagDouble("CLs_obs",clsLimit.CLs_obs);
      obslimit->setTagDouble("CLsplusb_obs",clsLimit.CLsplusb_obs);
      obslimit->setTagDouble("pb_obs",clsLimit.pb_obs);
      
      obslimit->setTagDouble("upper",clsLimit.obs_upperlimit);
      explimit->setTagDouble("inj_upper",clsLimit.inj_upperlimit);
      
      explimit->setTagDouble("exp_upper_med",clsLimit.exp_upperlimit);
      explimit->setTagDouble("exp_upper_p1s",clsLimit.exp_upperlimit_plus1);
      explimit->setTagDouble("exp_upper_p2s",clsLimit.exp_upperlimit_plus2);
      explimit->setTagDouble("exp_upper_m1s",clsLimit.exp_upperlimit_minus1);
      explimit->setTagDouble("exp_upper_m2s",clsLimit.exp_upperlimit_minus2);
      
      obslimit->setTagDouble("muhat_obs",clsLimit.muhat_obs);
      explimit->setTagDouble("muhat_exp",clsLimit.muhat_exp);
      
      explimit->setTagDouble("fit_status",clsLimit.fit_status);
      obslimit->setTagDouble("fit_status",clsLimit.fit_status);      
      return true;
    }
  };
  namespace {
    bool available_asymptotics = TSStatisticsManager::registerAction(new ActionRunAsymptoticCLs(),"RunAsymptoticCLs");
    bool available_asymptotics2 = TSStatisticsManager::registerAction(new ActionRunAsymptoticCLs(),"CalculateLimitAsymptoticCLs");    
  }
}

#endif



#ifdef HAS_CommonStatTools
#include "StandardHypoTestInvDemo.h"
#pragma message "using CommonStatTools StandardHypoTestInvDemo"

namespace {
  void analyseResults(TQFolder* config,RooStats::HypoTestInverterResult* r,TQFolder* result){
      double invalidThreshold = 49;
      config->getTagDouble("invalidLimitThreshold",invalidThreshold);
      double lowerLimit = 0;
      double llError = 0;
#if defined ROOT_SVN_VERSION &&  ROOT_SVN_VERSION >= 44126
      if (r->IsTwoSided()) {
	lowerLimit = r->LowerLimit();
	llError = r->LowerLimitEstimatedError();
      }
#else
      lowerLimit = r->LowerLimit();
      llError = r->LowerLimitEstimatedError();
#endif
      
      double upperLimit = r->UpperLimit();
      double ulError = r->UpperLimitEstimatedError();

      if (lowerLimit < upperLimit*(1.- 1.E-4) && lowerLimit != 0)
	std::cout << "The computed lower limit is: " << lowerLimit << " +/- " << llError << std::endl;
      std::cout << "The computed upper limit is: " << upperLimit << " +/- " << ulError << std::endl;


      // compute expected limit
      std::cout << "Expected upper limits, using the B (alternate) model : " << std::endl;
      std::cout << " expected limit (median) " << r->GetExpectedUpperLimit(0) << std::endl;
      std::cout << " expected limit (-1 sig) " << r->GetExpectedUpperLimit(-1) << std::endl;
      std::cout << " expected limit (+1 sig) " << r->GetExpectedUpperLimit(1) << std::endl;
      std::cout << " expected limit (-2 sig) " << r->GetExpectedUpperLimit(-2) << std::endl;
      std::cout << " expected limit (+2 sig) " << r->GetExpectedUpperLimit(2) << std::endl;
           
      double point = config->getTagDoubleDefault("point",result->getTagDoubleDefault("point",0.));
      
      TQFolder* obslimit = result->getFolder("observed+");
      TQFolder* explimit = result->getFolder("expected+");
      int n = r->GetNullTestStatDist(0)->GetSize();
      obslimit->setTagDouble("point",point);
      obslimit->setTagInteger("n", n);
      explimit->setTagDouble("point",point);
      explimit->setTagInteger("n", n);

      if(TQUtils::isNum(upperLimit) && upperLimit < invalidThreshold){
	obslimit->setTagDouble("upper",upperLimit);
      }
      if(TQUtils::isNum(lowerLimit)){
	obslimit->setTagDouble("lower",lowerLimit);
      }

      explimit->setTagDouble("exp_upper_med", r->GetExpectedUpperLimit(0) );
      explimit->setTagDouble("exp_upper_p1s", r->GetExpectedUpperLimit(1));
      explimit->setTagDouble("exp_upper_p2s", r->GetExpectedUpperLimit(2) );
      explimit->setTagDouble("exp_upper_m1s", r->GetExpectedUpperLimit(-1));
      explimit->setTagDouble("exp_upper_m2s", r->GetExpectedUpperLimit(-2) );

      const int nScan = r->ArraySize();
      for (int i=0; i<nScan; i++) {
	//gPad->SetLogy(false);
	std::vector<double> points = r->GetExpectedPValueDist(i)->GetSamplingDistribution();
	std::vector<double> weights = r->GetExpectedPValueDist(i)->GetSampleWeights();
	TH1F* hist=new TH1F(("CLs_"+std::to_string(i)).c_str(),";CLs;",50,TMath::MinElement(points.size(),&points[0]),TMath::MaxElement(points.size(),&points[0]));
	hist->SetDirectory(0);
	hist->SetLineWidth(2);
	for(size_t i=0; i<points.size();i++) hist->Fill(points[i],weights[i]);
	explimit->addObject(hist);
      }
      
      //plot toy distribution of upper limits
      std::vector<double> points = r->GetUpperLimitDistribution()->GetSamplingDistribution();
      std::vector<double> weights = r->GetUpperLimitDistribution()->GetSampleWeights();
      TH1F* hist=new TH1F("UpperLimit",";#mu upper limit;Frequency",50,TMath::MinElement(points.size(),&points[0]),TMath::MaxElement(points.size(),&points[0]));
      hist->SetDirectory(0);      
      hist->SetLineWidth(2);
      for(size_t i=0; i<points.size();i++) hist->Fill(points[i],weights[i]);
      hist->Scale(1/hist->Integral());
      explimit->addObject(hist);
  }

  double estimateLimit(TQFolder* config, RooWorkspace* workspace, double safetymargin){
    double confidenceLevel = config->getTagDoubleDefault("confidenceLevel",.9);
    TString mcname = config->getTagStringDefault("modelConfig","ModelConfig");
    TString dataname = config->getTagStringDefault("dataset","obsData");    
    TString asimovname = config->getTagStringDefault("asimov","asimovData");     

    try {
      EXOSTATS::AsymptoticsCLsRunner::Config cfg;
      cfg.m_doBlind = false;
      cfg.m_doInj = false;
      cfg.m_muInjection = false;
      cfg.m_NumCPU = config->getTagDoubleDefault("numCPU",1);
      cfg.m_debugLevel = config->getTagIntegerDefault("verbose",1);
      EXOSTATS::AsymptoticsCLsRunner runner(cfg);
      
      runner.prepareLimit(workspace, mcname.Data(), dataname.Data(), asimovname.Data());
      auto clsLimit = runner.computeLimit(confidenceLevel);
      
      double limit = safetymargin * clsLimit.exp_upperlimit_plus2;
      return limit;
    } catch(const std::runtime_error& err){
      // for some reason, this failed, we'll just use some rough estimate
      RooStats::ModelConfig* mc = (RooStats::ModelConfig*)(workspace->obj(mcname));
      RooAbsPdf* pdf = mc->GetPdf();
      RooAbsData* data = workspace->data(asimovname);
      RooRealVar* poi = (RooRealVar*)mc->GetParametersOfInterest()->first();
      poi->setConstant(false);
      pdf->fitTo(*data,RooFit::Minos(*poi));
      double error = poi->getError();
      if(error < poi->getVal()) error = poi->getVal();
      double limit = poi->getVal() + safetymargin * error;
    return limit;
  }
}
}


/*<cafdoc name=CalculateLimitToys>
  CalculateLimitToys
  ===========================
  
  Perform a hypothesis test using the asymptotic-formulae approach
  (https://arxiv.org/abs/1007.1727), calculating the significiance of
  a discovery.

  This action does the same as `CalculateLimit`, but uses the
  CommonStatTools AsymptoticsCLsRunner as a backend.  It is only
  available when CommonStatTools are available.

  Usage:
  ---------------
  ```
  +CalculateLimitToys {
  	+HWWHighMass {
  		<nPoints = 20, nToys = 50000, point = "1000">
  	}
  }
  ```

  The options include:
    * `dataset`: The name of the dataset to fit.
    * `point`: name of the point (e.g. mass value)
    * `modelConfig` (default=`ModelConfig`): The name of the ModelConfig to be used.
    * `calculatorType`: (default=`0`) Type of the calculator to be used
       * 0: Freq calculator
       * 1: Hybrid calculator
       * 2: Asymptotic calculator
       * 3: Asymptotic calculator using nominal Asimov data sets (not using fitted parameter values but nominal ones)
    * `testStatType`: (default=2). Type of the test statistic to be used
       * 0: LEP
       * 1: Tevatron
       * 2: Profile Likelihood two sided
       * 3: Profile Likelihood one sided (i.e. = 0 if mu < mu_hat)
       * 4: Profile Likelihood signed ( pll = -pll if mu < mu_hat)
       * 5: Max Likelihood Estimate as test statistic
       * 6: Number of observed event as test statistic
    * `poi.min`: minimum value of the POI to be used
    * `poi.max`: minimum value of the POI to be used
    * `nPoints`: number of points to evalute the POI for
    * `useCLs`: (default=`true`) toggle whether to use CLs    
    * `confidenceLevel`: (default=`0.95`) The confidence level for which to calculate the limit
    * `optimize` (default=`true`): optimize evaluation of test statistic
    * `useVectorStore` (default=true): convert data to use new roofit data store
    * `generateBinned` (default=true):generate binned data sets
    * `nToysRatio` (default=2): ratio Ntoys S+b/ntoysB
    * `poi.min` (default=-1): minimum of POI to be used
    * `poi.max` (default=-1): maximum of POI to be used
    * `useProof` (default=false): use Proof to parallelize
    * `useProofWorkers` (default=0): number of Proof workers
    * `detailed` (default=false): enable detailed output
    * `reuseAltToys` (default=false): reuse same toys for alternate hypothesis (if set one gets more stable bands)
    * `rebuildToys` (default=false): re-do extra toys for computing expected limits and rebuild test stat distributions (N.B this requires much more CPU (factor is equivalent to nToyToRebuild)    
    * `nToysRebuild` (default=100): number of toys to rebuild
    * `rebuildParamValues` (default=0): 
    * `minimizerType` (default=""): minimizer type (default is what is in ROOT::Math::MinimizerOptions::DefaultMinimizerType()
    * `initialFit` (default=-1): do a first  fit to the model (-1 : default, 0 skip fit, 1 do always fit)
    * `asimovBins` (default=0): number of bins in observables used for Asimov data sets (0 is the default and it is given by workspace, typically is 100)
</cafdoc>
*/


namespace TSAdvancedActions {
 class ActionRunToyLimits : public TSStatisticsManager::Action {
    bool execute(TQFolder * config) const override {

      TString wsname = config->getTagStringDefault("workspace",config->GetName());
      RooWorkspace * workspace = dynamic_cast<RooWorkspace*>(workspaces()->getObject(wsname));
      if(!workspace){
        manager->error(TString::Format("no such workspace available: '%s'",config->GetName()));
        return false;
      }
      
      TString resultname = config->getTagStringDefault("result",config->GetName());
      TQFolder * result = results()->getFolder(TString::Format("%s+",resultname.Data()))->getFolder("Limits+");
      
      TString mcname = config->getTagStringDefault("modelConfig","ModelConfig");
      TString dataname = config->getTagStringDefault("dataset","obsData");
      int calcType = config->getTagIntegerDefault("calculatorType",0);
      int testStatType = config->getTagIntegerDefault("testStatType",2);
      int nPoints = config->getTagIntegerDefault("nPoints",20);
      int nToys = config->getTagIntegerDefault("nToys",1000);            
      bool useCLs = config->getTagBoolDefault("useCLs",true);
      bool useNumberCounting = config->getTagBoolDefault("useNumberCounting",false);	                        
      
      double point = config->getTagDoubleDefault("point",0.);
      int seed = config->getTagIntegerDefault("randomSeed",config->getTagIntegerDefault("RandomSeed",0));
      manager->info(TString::Format("Running toys on '%s' on workspace '%s' using seed '%d'",config->GetName(),config->GetName(),seed));;
      bool rebuild = config->getTagBoolDefault("rebuildToys",false);
      
      TString outfile;
      bool redirect=config->getTag("fit.logToFile",outfile);
      if(redirect){
	TString fname = config->replaceInText(outfile);
	fname = TQPathManager::getPathManager()->getTargetPath(fname);
	TQUtils::ensureDirectoryForFile(fname);
	manager->info(TString::Format("writing log of minimization to '%s'",fname.Data()));
	TQLibrary::redirect(fname,true);
      }
      
      RooStats::HypoTestInvTool calc;
      double poiMin = config->getTagDoubleDefault("poi.min",0);
      double poiMax = 5;
      if(!config->getTagDouble("poi.max",poiMax)){
	TString asimovname = config->getTagStringDefault("asimov","asimovData");
	if(workspace->data(asimovname.Data())){
	  poiMax = estimateLimit(config,workspace,config->getTagDoubleDefault("poi.max.margin",5));
	  manager->info(TString::Format("using poi range %g - %g (%d steps)",poiMin,poiMax,nPoints));
	} else {
	  manager->info(TString::Format("cannot estimate poi range without asimov data, using %g - %g (%d steps)",poiMin,poiMax,nPoints));	  
	}
      }
      
     
      // set parameters
      calc.SetParameter("PlotHypoTestResult", false);
      calc.SetParameter("WriteResult", false);
      calc.SetParameter("Optimize", config->getTagBoolDefault("optimize",true));
      calc.SetParameter("UseVectorStore", config->getTagBoolDefault("useVectorStore",true));
      calc.SetParameter("GenerateBinned", config->getTagBoolDefault("generateBinned",true));
      calc.SetParameter("NToysRatio", config->getTagDoubleDefault("nToysRatio",2));
      calc.SetParameter("MaxPOI", config->getTagDoubleDefault("poi.max",-1));
      calc.SetParameter("MinPOI", config->getTagDoubleDefault("poi.min",-1));      
      calc.SetParameter("UseProof", config->getTagBoolDefault("useProof",false));
      calc.SetParameter("EnableDetailedOutput", config->getTagBoolDefault("detailed",false));
      calc.SetParameter("NWorkers", config->getTagIntegerDefault("useProofWorkers",0));
      calc.SetParameter("Rebuild", rebuild);
      calc.SetParameter("ReuseAltToys", config->getTagBoolDefault("reuseAltToys",false));
      calc.SetParameter("NToyToRebuild", config->getTagIntegerDefault("nToysRebuild",100));
      calc.SetParameter("RebuildParamValues", config->getTagBoolDefault("rebuildParamValues",0));
      calc.SetParameter("MassValue", TString::Format("%g",point));
      calc.SetParameter("MinimizerType", config->getTagStringDefault("minimizerType",""));
      calc.SetParameter("PrintLevel", config->getTagStringDefault("printLevel",""));
      calc.SetParameter("InitialFit", config->getTagIntegerDefault("initialFit",-1));
      calc.SetParameter("RandomSeed", seed);
      calc.SetParameter("AsimovBins", config->getTagIntegerDefault("asimovBins",0));
      
      RooStats::HypoTestInverterResult * r = 0;


      if(nToys > 0){
	const char* nuisPriorName = 0;
	r = calc.RunInverter(workspace, mcname, "",
			     dataname, calcType, testStatType, useCLs,
			     nPoints, poiMin, poiMax,
			     nToys, useNumberCounting, nuisPriorName );
      }
	
      TQLibrary::restore();


	
      if (!r) {
	manager->error("Error running the HypoTestInverter - Exit");
	return false;
      }
   
      // save the result if requested
      bool saveResult = config->getTagBoolDefault("streamResult",false);
      if(saveResult){
	TQFolder* resultfolder = result->getFolder("results+");  
	resultfolder->setTagDouble("point",point);
	r->SetName("HypoTestInverterResult");
	resultfolder->addObject(r->Clone());
	if(rebuild) resultfolder->addObject(calc.GetLimitDist());
      } else {
	r->ExclusionCleanup();
	TSUtils::fixToys(r);
	analyseResults(config,r,result);
      }
      
      return true;
    }
 };
  namespace {
    bool available_toys = TSStatisticsManager::registerAction(new ActionRunToyLimits(),"CalculateLimitToys");
  }
}


/*
  <cafdoc name=WriteCalculateLimitToys>
  WriteCalculateLimitToys
  ===========================

  This is a batch-parallelized version of the `CalculateLimitToys` action.
  Instead of running all the toys locally, it will write out a
  directory with a copy of the workspaces as well as configs to submit
  jobs running the toys on a batch system.
  
  
  Usage:
  ---------------
  ```
  +WriteCalculateLimitToys {
  	+HWWHighMass {
  		<nPoints = 20, nToys = 50000, nJobs = 500, poi.min = 0, point = "1000", outputPath = "workspaces/highmass-toys/NWA_GGF_1000/Batch", jobID = "NWA_GGF_1000">
  	}
  }
  ```


<cafdoc name=CollectScanResults>
  CollectScanResults
  ===========================
  
  This is the result collection for the batch-parallelized version of the `CalculateLimitToys` action.
  
  Usage:
  ---------------
  ```
  +CollectLimitToys {
  	+HWWHighMass {
	        <inputPath="./workspaces/highmass-toys/NWA_GGF_1000/Batch/">
        }
  }
  ```

  The results are produced with the `WriteCalculateLimitToys` action.

</cafdoc> */


namespace TSAdvancedActions {  
  class ActionWriteToyLimitConfig : public TSStatisticsManager::Action {

  public:
    bool execute(TQFolder * config) const override {
      RooWorkspace * workspace = dynamic_cast<RooWorkspace*>(workspaces()->getObject(config->GetName()));

      if(!workspace){
        manager->error(TString::Format("no such workspace available: '%s'",config->GetName()));
        return false;
      }
      
      TQFolder * result = results()->getFolder(TString::Format("%s+",config->GetName()));

      manager->info(TString::Format("Preparing toy limit calculation on workspace '%s'",workspace->GetName()));
      return writeConfigs(workspace,config,result);
    }

			
    bool writeConfigs(RooWorkspace* ws, TQFolder* config, TQFolder* result) const {
      double point = config->getTagDoubleDefault("point",0.);      
      TString output = TQFolder::concatPaths("batch",TString::Format("ToyLimits.x%g",point));
      config->getTagString("~outputPath",output);
      output = TQPathManager::getPathManager()->getTargetPath(output);
      TQUtils::ensureDirectory(TQFolder::concatPaths(output,"fitlogs"));
      TQUtils::ensureDirectory(TQFolder::concatPaths(output,"configs"));
      TQUtils::ensureDirectory(TQFolder::concatPaths(output,"results"));
      manager->info(TString::Format("creating configuration for '%s' in '%s'",config->GetName(),output.Data()));

      TString jobIDbase = config->getTagStringDefault("jobID","CalculateLimitToys");
      
      TString workspaceFileName = "workspace.root";
      config->getTagString("~workspaceFileName",workspaceFileName);
      TString filename = TQFolder::concatPaths(output,workspaceFileName);

      ws->writeToFile(TQPathManager::getPathManager()->getTargetPath(filename).c_str());

      TQFolder* outconfig = new TQFolder("config");
      outconfig->setTagString("expectedInput",filename);      
      TQFolder* import = outconfig->getFolder(TQFolder::concatPaths("ImportWorkspaces",ws->GetName())+"+");
      import->setTagString("inputFile",filename+":"+ws->GetName());
      import->setTagBool("infile",true);
				
      TQFolder* action = outconfig->getFolder(TQFolder::concatPaths("CalculateLimitToys",ws->GetName())+"+");
      
      TQFolder* exprt = outconfig->getFolder(TQFolder::concatPaths("ExportResults",ws->GetName())+"+");
			
      TQTaggable options(config);

      int njobs = config->getTagIntegerDefault("nJobs",100);
      int nToys = config->getTagIntegerDefault("nToys",50000);
      int chunksize = nToys / njobs;
      

      bool randomize = config->getTagBoolDefault("randomize",false);
      int offset = config->getTagIntegerDefault("firstJob",0);
      int seedoffset = config->getTagIntegerDefault("randomSeed.offset",0);
      
      for(int i=0; i<njobs; ++i){
	int id = offset + i;
	
	int seed = randomize ? 0 : id + seedoffset;
	TString name = TString::Format("chunk%d",id);
	  
	action->clearTags();
        exprt->clearTags();
	
        action->importTags(options);
	
        TString resultfile = TQFolder::concatPaths(output,"results",name)+".root";
	exprt->setTagString("outputFile",resultfile);
	outconfig->setTagString("expectedOutput",resultfile);
	
        action->setTagString("fit.logToFile",TQFolder::concatPaths(output,"fitlogs",name)+".log");
	action->setTagInteger("randomSeed",seed);
	action->setTagInteger("nToys",chunksize);	
	action->setTagInteger(".jobID",id);
	action->setTagBool("streamResult",true);
	
        TString outconfigfile = TQFolder::concatPaths(output,"configs",name)+".txt";
        outconfig->setTagString("identifier",TString::Format("%s_%s",jobIDbase.Data(),name.Data()));        
        if(outconfig->exportToTextFile(TQPathManager::getPathManager()->getTargetPath(outconfigfile).c_str())){
          manager->info(TString::Format("wrote %s",outconfigfile.Data()));
        } else {
          manager->warn(TString::Format("error writing %s",outconfigfile.Data()));
        }
      }
      delete outconfig;
      return true;
    }
  };

  class ActionCollectToyLimits : public TSStatisticsManager::Action {
  public:
    bool execute(TQFolder * config) const override {
			
      TString configname(config->GetName());     
      TString limitname = config->getTagStringDefault("path","Limits");
      TString resultname = config->getTagStringDefault("result","HypoTestInverterResult");      
      
      TQFolder* resultset = results()->getFolder(TQFolder::concatPaths(configname,limitname)+"+");
      if(!resultset){
        manager->error(TString::Format("no such result available: '%s'",config->GetName()));
        return false;
      }

      
      RooStats::HypoTestInverterResult * allresults = 0;
      
      TString path;
      if(!config->getTagString("inputPath",path)){
	manager->error(TString::Format("need to provide input path for '%s', skipping!",config->GetName()));
	return false;
      }
      
      manager->info(TString::Format("reading input from '%s'",path.Data()));
      
      double point = 0;

      TString resultfiles = TQFolder::concatPaths(TQPathManager::getPathManager()->getTargetPath(path),"results");
      TQStringUtils::ensureTrailingText(resultfiles,"/*.root");
      TCollection* list = TQUtils::ls(resultfiles);
      if(!list || list->GetEntries() == 0) {
	manager->error(TString::Format("no files found under '%s'!",resultfiles.Data()));
	if(list) delete list;
	return false;
      }
      TQIterator itr(list,true);
      while(itr.hasNext()){
	TString err;
	TString fname(itr.readNext()->GetName());
	
	TQFolder* toyset = TQFolder::loadFolder(fname,err);
	if(!toyset){
	  manager->error(TString::Format("unable to load file '%s': %s",fname.Data(),err.Data()));
	  continue;
	} else {
	  manager->info(TString::Format("loaded file '%s'",fname.Data()));
	}
	TString path(TQFolder::concatPaths(limitname,"results"));
	TQFolder* limit = toyset->getFolder(path);
	if(!limit){
	  manager->error(TString::Format("cannot find limit at '%s'",path.Data()));
	  continue;
        }
	RooStats::HypoTestInverterResult* hypores = (RooStats::HypoTestInverterResult*) limit->getObject(resultname);
	if(!hypores){
	  manager->error(TString::Format("cannot find limit result named '%s'",resultname.Data()));
	  continue;
	}
	if(!allresults){
	  allresults = (RooStats::HypoTestInverterResult*)hypores->Clone();
	  limit->getTagDouble("point",point);
	}
	else allresults->Add(*hypores);
	delete toyset;
      }

      int nfailed = TSUtils::fixToys(allresults);
      allresults->ExclusionCleanup();
      manager->info(TString::Format("removed %d failed toys",nfailed));

      if(config->getTagBoolDefault("plot",true)){
	RooStats::HypoTestInverterPlot *plot = new RooStats::HypoTestInverterPlot("HypoTestInverterPlot","",allresults);
	TCanvas* canvas = new TCanvas("controlplot","controlplot");
	plot->Draw("CLb 2CL");

	TLatex *t = new TLatex(.05,.925,TString::Format("%s: x=%g",configname.Data(),point));
	t->SetNDC();
	t->Draw();
	
	canvas->SaveAs(TQFolder::concatPaths(path,"CLb2CL.pdf"));
	delete canvas;
	delete t;

	const int nScan = allresults->ArraySize();
	for (int i=0; i<nScan; i++) {
	  TCanvas* canvas = new TCanvas("testStatPlot","testStatPlot");	  
	  RooStats::SamplingDistPlot * pl = plot->MakeTestStatPlot(i);
	  TString name = TString::Format("testStatPlot%d.pdf",i);
	  pl->SetLogYaxis(true);
	  pl->Draw();
	  canvas->SaveAs(TQFolder::concatPaths(path,name));
	  delete canvas;
	}
	delete plot;	
	}
      
      resultset->setTagDouble("point",point);
      analyseResults(config,allresults,resultset);
      resultset->getFolder("results+")->addObject(allresults);
      
      return true;
    }
  };
  namespace {
    bool available_write   = TSStatisticsManager::registerAction(new ActionWriteToyLimitConfig(),"WriteCalculateLimitToys");
    bool available_collect = TSStatisticsManager::registerAction(new ActionCollectToyLimits(),"CollectLimitToys");
  }
}


#endif

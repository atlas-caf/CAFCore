//this file looks like plain C, but it's actually -*- c++ -*-
#ifndef __TS_SIGNIFICANCEEVALUTATOR__
#define __TS_SIGNIFICANCEEVALUTATOR__

#include "QFramework/TQSignificanceEvaluator.h"
#include "QFramework/TQGridScanner.h"
#include "TSStatisticsManager.h"
#include "QFramework/TQFolder.h"

class TSCLSignificanceEvaluator : public TQSignificanceEvaluator {
 public:
  void keepTMP(TString tmpfilenamebase="histograms_");
  
  void printMode();
  void exportSampleFolder(bool doexport = true);
  void importSampleFolder(bool doimport = true);
  void exportSignificance(bool doexport = true);
  void importSignificance(bool doimport = true);
  void setSampleFolderFile(const TString& filename);
  void setSignificanceFile(const TString& filename);
  void setExternalEvaluation(const TString& command, bool allowRecycling = false);
  void setInternalEvaluation(bool eval = true);
  void setDebugOutputdir(TString outdir);

  void deactivate();
  
  TQSampleFolder* getWorkingFolder(TString path = "");
  int cleanWorkingFolder();
  TQSampleFolder* getBaseFolder(TString path = "");
  TQFolder* getConfigFolder(TString path = "");
  void printPaths();
  virtual double getLuminosity(TString folderName="info",TString tagName="luminosity") override;

  bool addFOM(const TString& fomName);
  
  virtual double evaluate() override;
  virtual std::vector<double> evaluateMultiple() override;

  virtual int exportWorkspace(const TString& outFileName = "model-raw.root") override;
  
  void addVariation(TQSampleFolder* sf, TString varname);
  void setInitialization(TClass* initClass);
  virtual bool initialize(TQGridScanner* scanner) override;
  bool initialize(const TString& importfilename = "", const TString& exportfilename = "");
  bool configure(const TString& configfilename, const TString& modelname = "");
  TSCLSignificanceEvaluator(TQSampleFolder* sf=NULL, TString targetVar="", TString name="CL");
  TSCLSignificanceEvaluator(TList* folders, TString targetVar="", TString name="CL", TString folderKeyTag=".key");
  TSCLSignificanceEvaluator(TString targetVar, TString name="CL");

  virtual double getSignificance(size_t iregion) override;
  virtual double getSignificance2(size_t iregion) override;

 protected:
  TQSampleFolder* baseFolder;       //!   
  TQSampleFolder* workingFolder;    //!
  TQFolder* m_config;		    //!
  TString tmpFileName;		    //!
  TString tmpFileNameBase;	    //!
  TString outputdir;	    //!
  TString modelName;		    //!
  TString discriminantVariable;	    //!
  int m_discriminantVariableIndex; //! // index of discriminant variable in n-dim hist
  TSStatisticsManager* engine;	    //!
  std::vector<TString> m_histRegionsInInput;   //!
  std::vector<TString> m_histRegionsInModel;   //!
  std::vector<TString> m_samplePaths;   //!
  std::vector<TString> locations;   //! // deprecated! delete once ready to do so
  std::vector<TString> m_histNames;  //!
  std::vector<TString> variations;  //!
  TString sampleFolderFileName;	    //!
  TString significanceFileName;	    //!
  TString evaluationCommand;	    //!
  bool internalEvaluation;          //!
  bool recyclingAllowed;          //!
  bool supportRegionSetHandling;   //!
  bool debug; //!

  std::vector<TH1F*> m_Hists; //!
  std::map< TString, TAxis* > m_boundMultiDimAxis; //!
  
  int nPreparedRegionSets; //!
  int m_nPointsProcessed; //!

 public:

  bool checkCutoffs();
  void setRegionSetHandling(bool val);
  void setVerbose(bool v);
  virtual bool hasNativeRegionSetHandling() override;
  virtual bool prepareNextRegionSet(const TString& suffix = "") override;
  virtual bool isPrepared() override;

  bool m_multipleFOMsCompatible = false;
  
  enum Mode { IMPORT, EXPORT, NONE };
  
 protected:
  Mode sampleFolderMode;
  Mode significanceMode;
  int configureBoundRegions();
  Int_t prepareFromGridScanner(const TString& /*suffix = ""*/);
  
  std::vector <TString> m_FOMNames;

  ClassDefOverride(TSCLSignificanceEvaluator,1)
};

#endif

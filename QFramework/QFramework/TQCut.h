//this file looks like plain C, but it's actually -*- c++ -*-
#ifndef __TQCut__
#define __TQCut__

#include "TNamed.h"
#include "TObjArray.h"
#include "TTree.h"
#include "TString.h"

#include "QFramework/TQIterator.h"

class TQAnalysisJob;
class TQSample;
class TQFolder;
class TQObservable;
class TQAnalysisJob;
class TQToken;
class TQAnalysisSampleVisitorBase;

class TQCut : public TNamed {
public: 
  //various auxilliary data structures to handle "variants"
  struct VariantDefinition { // nested
    TString matchingPattern; //pattern to be matched by cuts to be replaced
    TString cutExpression; //new cut expression (ignored if empty string)
    TString weightExpression; //new weight expression (ignored if empty string)
  };
  //                           < suffix, vec< definition > >
  typedef std::vector<std::pair<TString,std::vector<VariantDefinition>>> VariantDefinitionList;
  
  
  struct VariantHandle { // nested
    TString cutExpression = "";
    TString weightExpression = "";
    TQObservable* cutObs = nullptr;
    TQObservable* weightObs = nullptr;
    std::vector<TQAnalysisJob*> analysisJobs;
    std::vector<TQSampleFolder*> variantSFs;
    std::vector<TQSampleFolder*> nominalSFs;
  };
  
  struct VariantStackElement { // nested
    //represents one layer of variants, 
    //the entries of vectors below represent the variants in the layer
    std::vector<VariantHandle> payload;
    std::vector<TQSampleFolder*> variantRoots; //first layer of the sampleFolder that is variant-specific
  };
  
  struct VariantResults { // nested
    std::vector<bool> passed;
    std::vector<double> weight;
    void resize(size_t size);
    void reset(double baseWeight=1.);
    bool passAny();
  };
  
  struct LazyWeight {
    TQCut* instance;
    double value = 0;
    LazyWeight () = delete;
    LazyWeight ( TQCut* inst) : instance(inst) {};
    operator double() {
      if (this->instance) {
        value = this->instance->getWeight(false);
        this->instance=nullptr;
      }
      return value;
    }
  };
  
protected:

  TString fCutExpression = "";
  TQObservable * fCutObservable = NULL; //!
 
  TString fWeightExpression = "";
  TQObservable * fWeightObservable = NULL; //!

  TQCut * fBase = NULL;
  //TODO: we may want to eventually repalce these TObjArray by stl containers
  //      this also allows us to easily get rid of the iterator members below
  TObjArray * fCuts = new TObjArray();
  TObjArray * fAnalysisJobs = new TObjArray(); //!

  mutable TQCutIterator fCutItr; //!
  mutable TQAnalysisJobIterator fJobItr; //!
  
  TQToken * fTreeToken = NULL; //!
  TQSample * fSample = NULL; //!
  std::vector<TQSampleFolder*> fInitializationHistory; //!
  TTree * fTree = NULL; //!
  
  //variant initialization stack. 
  std::vector<VariantStackElement*> fVariantStack; //!
  //handle to pass on dicisions for several variants at once (created at init, destroyed at finalize. sizes of vectors to be fixed at initialization)
  VariantResults* fVariantResults = nullptr; //!
 
  bool fSkipAnalysisJobs = false; //set based on tags on sample folders
  bool fSkipAnalysisJobsGlobal = false; //set based on tags during cut creation (from TQFolder)
  
  int fPrintResult = 0; // max number of events for which cut decision (and weight value) should be printed
  int fPrintResultCount = 0; //! transient member keeping track of how many decisions have already been printed
  
  void setBase(TQCut * base_);

  bool skipAnalysisJobs(TQSampleFolder * sf);

  virtual bool initializeObservables();
  virtual bool finalizeObservables();
  virtual bool initializeSelfSampleFolder(TQSampleFolder* sf);
  virtual bool finalizeSelfSampleFolder  (TQSampleFolder* sf);
  
  bool initializeVariantsSampleFolder(TQSampleFolder* sf, bool hasNewVariantLayer);
  bool initializeVariants(TQSample* sample, bool hasNewVariantLayer);
  
  bool verifyVariantSetup(const std::vector<VariantHandle>& parentHandles);
  
  bool finalizeVariantsSampleFolder(TQSampleFolder* sf);
  bool finalizeVariants();
  
  virtual TObjArray* getOwnBranches();

  bool printEvaluationStep(size_t indent) const;

  void printInternal(const TString& options, int indent);

  static TQCut* createFromFolderInternal(TQFolder* folder, TQTaggable* tags);
  void importFromFolderInternal(TQFolder * folder, TQTaggable* tags);
  
  int createVariantFolders(TQSampleFolder* sf, const TQCut::VariantDefinitionList* variantDefs);//, std::vector<TQSampleFolder*>& variantFolders);
  VariantStackElement* createVariantLayer(const std::vector<VariantHandle>& preceedingHandles, TQSampleFolder* nextSF, const VariantDefinitionList* variantDefs);
  bool createVariants(TQSampleFolder* nextSF, const VariantDefinitionList* variantDefs);
  
  void analyseVariants(const VariantResults* preceedingResults, bool useWeights, double baseWeight=1.); //base weight can be given if no previous variant result is available
public:

  int Compare(const TObject* obj) const override;
  void sort();
  int getDepth() const;
  int getWidth() const;

  void printEvaluation() const;
  void printEvaluation(Long64_t iEvent) const;
  void printWeightComponents() const;
  void printWeightComponents(Long64_t iEvent) const;
 
  static void writeDiagramHeader(std::ostream & os, TQTaggable& tags);
  static void writeDiagramFooter(std::ostream & os, TQTaggable& tags);
  int writeDiagramText(std::ostream& os, TQTaggable& tags, TString pos = "");
  bool writeDiagramToFile(const TString& filename, const TString& options = "");
  bool writeDiagramToFile(const TString& filename, TQTaggable& tags);
  bool printDiagram(TQTaggable& options);
  bool printDiagram(const TString& options);
  TString writeDiagramToString(TQTaggable& tags);
  TString getNodeName();

  TList * exportDefinitions(bool terminatingColon = false);

  static bool isValidName(const TString& name_);

  static bool parseCutDefinition(const TString& definition_, TString * name_, 
                                 TString * baseCutName_, TString * cutExpr_, TString * weightExpr_);

  static bool parseCutDefinition(TString definition_, TString& name_, 
                                 TString& baseCutName_, TString& cutExpr_, TString& weightExpr_);

  static TQCut * createCut(const TString& definition_);
  static TQCut * createCut(const TString& name, const TString& cutExpr, const TString& weightExpr = "");

  static TQCut * importFromFolder(TQFolder * folder, TQTaggable* tags = NULL);


  TQCut();
  TQCut(const TString& name_, const TString& title_ = "", const TString& cutExpression = "", const TString& weightExpression = "");

  TQCut * getBase();
  TString getPath();
  const TQCut * getBaseConst() const;
  TQCut * getRoot();

  bool isDescendantOf(TString cutName);

  static bool isTrivialTrue(const TString& expr);
  static bool isTrivialFalse(const TString& expr);

  TString getActiveCutExpression() const;
  TString getActiveWeightExpression() const;

  void printActiveCutExpression(size_t indent = 0) const;

  void setCutExpression(const TString& cutExpression);
  const TString& getCutExpression() const;
  TString getCompiledCutExpression(TQTaggable* tags);
  TString getGlobalCutExpression(TQTaggable* tags = NULL);
 
  void setWeightExpression(const TString& weightExpression);
  const TString& getWeightExpression() const;
  TString getCompiledWeightExpression(TQTaggable* tags);
  TString getGlobalWeightExpression(TQTaggable* tags = NULL);
  
  inline void setSkipAnalysisJobsGlobal(bool skip=true) {this->fSkipAnalysisJobsGlobal = skip;}
  inline bool getSkipAnalysisJobsGlobal() const {return this->fSkipAnalysisJobsGlobal;}
  
  inline void setPrintResults(int maxCount=0) {this->fPrintResult = maxCount;}
  inline bool getPrintResults() const {return this->fPrintResult;}
  
  TQObservable* getCutObservable();
  TQObservable* getWeightObservable();

  bool addCut(const TString& definition_);
  bool addCut(TQCut * cut);
  bool addCut(TQCut * cut, const TString& baseCutName);
 
  TQCut * addAndReturnCut(const TString& definition_);

  bool includeBase();
  bool removeCut(const TString& name);

  void printCuts(const TString& options = "r");
  void printCut(const TString& options = "r");
  void print(const TString& options = "r");
  void printAnalysisJobs(const TString& options = "");
  
  int dumpToFolder(TQFolder * folder);

  virtual bool isMergeable() const;

  void consolidate();

  TQCut * getCut(const TString& name);
  void getMatchingCuts(TObjArray& matchingCuts, const TString& name);
  std::vector<TString> getCutNames(const TString& cutName);
  void getMatchingCuts(TObjArray& matchingCuts, const TObjArray& name_sep, int offset=0);
  void propagateMatchingCuts(TObjArray& matchingCuts, const TObjArray& name_sep, int offset=0);
  bool isResidualMatchingSegmentOptional(const TObjArray& name_segments, int offset=0);
  TObjArray * getCuts();
  TObjArray * getListOfCuts();
  virtual TObjArray * getListOfBranches();
  TQCut * getSingleCut(TString name, TString excl_pattern = "PATTERNYOUWON'TSEE");
  
  void setCuts(TObjArray* cuts);
  
protected:
  bool initialize(TQSample* sample, const TQCut::VariantDefinitionList* variantDefs); //should not be valled from outside TQCut
public:
  virtual bool initialize(TQSample * sample); //public interface
  virtual bool finalize();
  TQSample* getSample();
  inline const std::vector<TQSampleFolder*>& getInitializationHistory() const { return this->fInitializationHistory;} 
  
  bool canInitialize(TQSampleFolder* sf) const;
  bool canFinalize(TQSampleFolder* sf) const;
protected:
  bool initializeSampleFolder(TQSampleFolder* sf, const TQCut::VariantDefinitionList* variantDefs); //should not be valled from outside TQCut
public:
  bool initializeSampleFolder(TQSampleFolder* sf); //public interface
  bool finalizeSampleFolder (TQSampleFolder* sf);
 
  bool addAnalysisJob(TQAnalysisJob * newJob_, const TString& cuts_ = "");
  int getNAnalysisJobs();
  void clearAnalysisJobs();
  TObjArray* getJobs();

  TQCut* getClone();
  TQCut* getCompiledClone(TQTaggable* tags);

  virtual bool passed(bool doPrint=false) const;
  virtual double getWeight(bool doPrint=false) const;

  virtual bool passedGlobally() const;
  virtual double getGlobalWeight() const;

  bool executeAnalysisJobs(double weight);
  void analyse(double weight, bool useWeights);
  
  virtual ~TQCut();
 
  ClassDefOverride(TQCut, 1); // a cut to be applied during an analysis

};

#endif

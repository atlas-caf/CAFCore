#!/bin/env python

def main(args):
    import QFramework
    samples = QFramework.TQSampleFolder.loadSampleFolder(args.samples)
    cutinfo = samples.getFolder(args.infocuts)
    if args.cuts:
        cutinfo.exportToTextFile(args.cuts)

    if args.cutflow_cuts:
        with open(args.cutflow_cuts,"wt") as outfile:
            allfolders = cutinfo.getListOfFolders("?/?/*")
            lastfolder = None
            for folder in allfolders:
                if len(folder.getTagStringDefault(".title","")) > 0:
                    outfile.write('.name="{:s}"\n'.format(folder.GetName()))
                if lastfolder and not lastfolder.isBaseOf(folder) and not lastfolder.getBase().isBaseOf(folder):
                    outfile.write('.name="||"\n')
                lastfolder = folder

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="extract cuts from a samplefolder")
    parser.add_argument("samples",help="samplefolder")
    parser.add_argument("--cut-info-folder",dest="infocuts",help="path to the cut info folder",default="info/cuts")
    parser.add_argument("--cuts",help="cut definitions")
    parser.add_argument("--cutflow-cuts",help="cutflow cut list")

    main(parser.parse_args())

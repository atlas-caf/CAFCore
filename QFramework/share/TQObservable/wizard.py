#!/usr/bin/env python
"""Wizard for making C++ files for TQObservables."""
import sys
import os
from os.path import join
import argparse
from dataclasses import dataclass
import wizard_helpers as wh

# Before starting the wizard, check that CAFCore is setup
if "CAFANALYSISBASE" not in os.environ:
    print("CAFANALYSISBASE Enviroment variable not set. Please run 'source setupAnalysis.sh' in your build directory for this project to use this script.")
    exit()

try:
    input = raw_input
except NameError:
    pass

HELPTEXT = "******* Welcome to the TQObservable wizard!*******\n" +\
    "* If you are using the wizard for the first time, \n" +\
    "* please get help from the tutorial here: \n" +\
    "* https://gitlab.cern.ch/atlas-caf/CAFExample/blob/master/Tutorial/WritingCustomObservables.md.\n" +\
    "* You can run this wizard with CLI arguments.\n" +\
    "* Try using --help to see all available arguments.\n" +\
    "**************************************************"

VALID_TYPES = ["Event", "Tree", ""]  # the types of observables one can create

class Arguments:
    """Made a class so we can double-check the arguments provided make sense."""
    def __init__(self, parsed_args):
        self.package = parsed_args.package
        self.name = parsed_args.name
        self.type_name = parsed_args.type_name
        self.vector = parsed_args.vector
        self.expression = parsed_args.expression
        self.advanced = parsed_args.advanced
        self.factory = parsed_args.factory
        self.linkdef = parsed_args.linkdef
        self.no_confirm = parsed_args.no_confirm

        if self.advanced and not self.expression:
            raise ValueError("Why do you have advanced=True when expression=False?")
        if self.type_name is not None and self.type_name not in VALID_TYPES:
            raise ValueError("Invalid observable type, must be one of "+",".join(VALID_TYPES))
        if self.package == "QFramework" and not self.name.startswith("TQ"):
            raise ValueError("QFramework observable names must start with 'TQ'.")


def main(args):
    """Run the wizard, taking command line args if needed."""
    this_file_dir = os.path.dirname(os.path.realpath(__file__))

    print(HELPTEXT)

    alrbdir = os.environ.get("ATLAS_LOCAL_ROOT_BASE")
    if alrbdir:
        print("Found an ASG setup in %s" % alrbdir)
    else:
        print("You do not currently have ASG setup. "
              "If you want to add your new observable to a package, "
              "you should first setup an ASG release. Please choose:")
        if input("Continue and write files to current working directory (type 'c') "
                 "or abort (type anything else)? ") == "c":
            pkgname = ""
        else:
            print("aborting!")
            sys.exit()
        pkgdir = os.getcwd()

    # check if RootCore setup is used:
    is_root_core_setup = False
    if 'ROOTCOREBIN' in os.environ:
        rcdir = os.path.dirname(os.environ['ROOTCOREBIN'])
        # if cmake is used RootCoreBin will be set to an cvmfs path, take care of this:
        if rcdir and not "/cvmfs/atlas.cern.ch/" in rcdir:
            is_root_core_setup = True

    # Obtain the base directory 
    basedir = os.getenv("CAFANALYSISBASE")
    print("Found base directory of your analysis framework setup in", basedir)

    # Search for the packages in this list in base directory
    print("These are your current local packages (querying for packages...): ")
    packages = wh.find_packages(basedir)
    pkg_names = [item[0] for item in packages]
    pkg_paths_from_basedir = [os.path.relpath(item[1], basedir) for item in packages]
    
    for pkg_name, pkg_path in zip(pkg_names, pkg_paths_from_basedir):
        print("> {:20} (Found in {:})".format(pkg_name, os.path.join(os.path.basename(basedir), pkg_path)))

    if args.package is None:
        print("Should the observable wizard put the files into your current working directory "
          "(leave empty) or into some package (type package name)?")
        pkgname = input("Type your choice: ")
    else:
        print("Using provided package name:", args.package)
        pkgname = args.package

    if (pkgname not in pkg_names) and (pkgname != ""):
        print("Package '%s' not found, aborting!" % pkgname)
        exit()
    if pkgname != "":
        pkgdir_relative = [pkg_paths_from_basedir[i]
                           for i, s in enumerate(pkg_names) if pkgname in s]
        pkgdir = join(basedir, pkgdir_relative[0])
    else:
        pkgdir = join(os.getcwd())

    # ask for classname (for QFramework classes, new observable class should start with TQ)
    if args.name is None:
        classname = ""
        if pkgname == "QFramework":
            while not classname.startswith("TQ"):
                classname = input("What is the name of the observable you would like to create? "
                                "\nSince you selected QFramework as a target package, "
                                "the name is required to start with 'TQ'. "
                                "Enter the name of your observable: ")
        else:
            classname = input('What is the name of the observable you would like to create? ')
    else:
        classname = args.name

    # ask for observable type (select template file)
    if args.type_name is None:
        print("What type of observable would you like to create?")
        print("If you want to read xAODs using the xAOD::TEvent mechanism, please type 'Event'")
        print("If you want to access the TTree pointer and use TTreeFormula, please type 'Tree'")
        print("If your new observable does not need direct data access "
            "but uses some custom mechanism to work, please leave empty.")
        obs_type = input("Please specify the type of observable, choose  from {Event,Tree,<empty>}: ")
    else:
        obs_type = args.type_name
    if obs_type == "Event":
        typename = "TQEventObservable"
    elif obs_type == "Tree":
        typename = "TQTreeObservable"
    else:
        typename = "TQObservable"

    if args.vector is None:
        vector_observable = wh.input_is_y(
            "Do you want to create a vector observable that can return multiple values?")
    else:
        vector_observable = wh.text_is_affirmative(args.vector)

    # ask if expression handling is requested
    if args.expression is None:
        print("Some observable classes have an 'expression' member variable "
            "that allows to alter the configuration based on sample tags, "
            "but complicates identifying the right observable.")
        include_expression = wh.input_is_y(
            "Should your class have an 'expression' member variable?")
    else:
        include_expression = wh.text_is_affirmative(args.expression)

    if args.advanced is None:
        advanced_expression = False
        if include_expression:
            advanced_expression = wh.input_is_y(
                "Does your class rely on advanced string parsing capabilities for "
                "the 'expression' member variable?")
    else:
        advanced_expression = wh.text_is_affirmative(args.advanced)

    # check for factory
    if args.factory is None:
        has_factory = wh.input_is_y(
            "Are you planning to provide a factory for your observable class?")
    else:
        has_factory = wh.text_is_affirmative(args.factory)

    # print summary and ask for confirmation
    print("Your choices:")
    print("- Class name: %s" % classname)
    print("- Inherits from: %s" % typename)
    print("- In package: %s" % pkgname)
    print("- Write to directory: %s" % pkgdir)
    if include_expression:
        if advanced_expression:
            print("- Including advanced expression member")
        else:
            print("- Including expression member")
    else:
        print("- Not including expression member")
    if has_factory:
        print("- Configured for factory use")
    else:
        print("- Not configured for factory use")

    if not args.no_confirm:
        if not wh.input_is_y("Build this observable now?"):
            print("Aborted")
            sys.exit()

    # return header and source file
    hcode, cxxcode = wh.header_and_source_file_text(
        this_file_dir, pkgname, classname, typename, vector_observable,
        include_expression, advanced_expression, has_factory)

    # write the new source files to the target directory
    if pkgname:
        cxxfile = join(pkgdir, "Root", classname+".cxx")
        hfile = join(pkgdir, pkgname, classname+".h")
    else:
        cxxfile = join(pkgdir, classname+".cxx")
        hfile = join(pkgdir, classname+".h")
    wh.write_file(hfile, hcode)
    wh.write_file(cxxfile, cxxcode)

    # look for a linkdeffile, ask if user wants us to patch (Only needed with RootCore setup)
    if is_root_core_setup:
        linkdeffile = join(pkgdir,"Root","LinkDef.h")
        if os.path.exists(linkdeffile):
            print("The wizard has detected a LinkDef.h file in the target package")
            if args.linkdef is None:
                if wh.input_is_y("Do you want to add an entry for your observable class?"):
                    wh.make_link_def_entry(linkdeffile, pkgname, classname)
            else:
                if wh.text_is_affirmative(args.linkdef):
                    print("Making LinkDef.h entry based on command line argument --linkdef")
                    wh.make_link_def_entry(linkdeffile, pkgname, classname)

    print("Your observable skeleton was successfully created. The created files can be found in")
    print(" "+join(pkgdir, pkgname, classname+".h"))
    print(" "+join(pkgdir, "Root", classname+".cxx"))
    print("After configuring the observable as desired, don't forget to do the following: ")
    print("- Execute cmake and recompile (calling the alias 'cafcompile' takes care of both)!")
    print("- Add a python snippet in the appropriate observables/ folder in your analysis package!")


if __name__ == "__main__":
    # get arguments
    parser = argparse.ArgumentParser("TQObersvable Wizard", description=HELPTEXT)
    # required
    parser.add_argument("-p", "--packageName", dest="package",
                        help="Name of package in which to put this observable.")
    parser.add_argument("-n", "--nameOfObs", dest="name",
                        help="Name of the observable you want to create.")
    parser.add_argument("-t", "--typeOfObs", dest="type_name",
                        help="Type of observable. One of ['Event', 'Tree', ''].")

    # booleans, intentionally not using store_true here so they give None and prompt for text
    # if the user provides no flag at all
    parser.add_argument("-v", "--vectorObs", dest="vector",
                        help="Whether this will be created as a vector type (str, y/n).")
    parser.add_argument("-e", "--expression", dest="expression",
                        help="Whether this should have an `expression` member (str, y/n).")
    parser.add_argument("-a", "--advancedExpression", dest="advanced",
                        help="Whether expression will get advanced string parsing (str, y/n).")
    parser.add_argument("-f", "--factory", dest="factory",
                        help="Whether to add code to provide a factory (str, y/n).")
    parser.add_argument("-l", "--linkdef", dest="linkdef",
                        help="Pass this to add a LinkDef.h entry if applicable (str, y/n).")

    # other bools
    parser.add_argument("--noConfirm", dest="no_confirm", action="store_true",
                        help="Pass this to skip the 'are you sure' message.")

    args = Arguments(parser.parse_args())
    main(args)


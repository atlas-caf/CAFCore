"""Helpers for TQObservable wizard."""
import os
import sys
import fileinput
import signal

try:
    input = raw_input
except NameError:
    pass

class Args:
    """Thing for storing command line args in a convenient format :) """
    def __init__(self, path, pkgname, classname, typename, vector_observable, include_expression, advanced_expression, has_factory):
        self.path = path
        self.pkgname = pkgname
        self.classname = classname
        self.typename = typename
        self.vector_observable = vector_observable
        self.include_expression = include_expression
        self.advanced_expression = advanced_expression
        self.has_factory = has_factory

# stuff so this exits with a nice message on Ctrl+C
def signal_handler(*_):
    """Clean exit on Ctrl-C."""
    print("\nUser pressed Ctrl-C. Exiting.")
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

def input_is_y(question):
    """
    Check if the user input y(or Y) to a y/N question.

    Return True if input is y or yes (ignore case), false otherwise to ensure an affirmative yes.
    """
    return text_is_affirmative(input("%s (y/N) " % question))

def text_is_affirmative(text):
    """Return True if text is y or yes (ignore case), False otherwise to ensure affirmative yes."""
    return text.lower() in ["y", "yes"]

def search_and_replace(code, pkgname, classname, typename):
    """Search and replace the classname in the template files."""
    replace_dict = {
        "ObsName": classname,
        "OBSNAME": classname.upper(),
        "ParentClass": typename,
        "PkgName" if pkgname else "PkgName/": pkgname
    }
    for key, val in replace_dict.items():
        code = code.replace(key, val)
    return code


def write_file(fname, code):
    """Write the string code to file at path fname (to be used for modified template files)."""
    # avoid unintentional overwriting
    if os.path.exists(fname):
        if not input_is_y("File '%s' exists - overwrite?" % fname):
            print("Aborted")
            sys.exit()
    # write
    with open(fname, 'w', encoding="utf-8") as output_file:
        output_file.write(code)
    print("Wrote '%s'!" % fname)


def read_file(fname):
    """Read the contents of a file, return as a string. Wrapper for standard file.read function."""
    with open(fname, "r", encoding="utf-8") as input_file:
        return input_file.read()


def header_and_source_file_text(path,
                                pkgname,
                                classname,
                                typename,
                                vector_observable,
                                include_expression,
                                advanced_expression,
                                has_factory):
    """Produce the text for a .h and .cxx file, return two stings."""
    # open the header files and patch them together
    specific_typename = typename + "_VectorMode" if vector_observable else typename

    # start with the first half of an include guard
    head_path = os.path.join(path, "header")
    hcode = read_file(os.path.join(head_path, "head.h"))

    # add type-specific boilerplate
    hcode += read_file(os.path.join(head_path, specific_typename+".h"))

    # add declarations for initializeSelf and finalizeSelf functions
    hcode += read_file(os.path.join(head_path, "initializefinalize.h"))

    if include_expression:
        # define ObsName and add hasExpression, getExpression setExpression functions
        hcode += read_file(os.path.join(head_path, "expression.h"))
        if advanced_expression:
            # add parseExpression, clearParsedExpression, getActiveExpression functions
            hcode += read_file(os.path.join(head_path, "advancedexpression.h"))
    else:
        # only define ObsName
        hcode += read_file(os.path.join(head_path, "noexpression.h"))

    if has_factory:
        # include a factory
        hcode += read_file(os.path.join(head_path, "factory.h"))
    else:
        # or don't
        hcode += read_file(os.path.join(head_path, "nofactory.h"))

    # end the header
    hcode += read_file(os.path.join(head_path, "foot.h"))

    # replace OBSNAME and such with our actual values
    hcode = search_and_replace(hcode, pkgname, classname, typename)

    # header's done, now do the C++
    imp_path = os.path.join(path, "implementation")
    # start with the basics
    cxxcode = read_file(os.path.join(imp_path, "head.cxx"))
    # add type-specific content
    cxxcode += read_file(os.path.join(imp_path, specific_typename+".cxx"))
    # different cases for whether you have an expression vs advanced_expression vs vector etc
    if not advanced_expression:
        if typename == "Tree":
            if not vector_observable:
                cxxcode += read_file(os.path.join(imp_path, "initializefinalize.tree.cxx"))
            else:
                cxxcode += read_file(os.path.join(imp_path, "initializefinalize.tree_vector.cxx"))
        else:
            if not vector_observable:
                cxxcode += read_file(os.path.join(imp_path, "initializefinalize.event.cxx"))
            else:
                cxxcode += read_file(os.path.join(imp_path, "initializefinalize.event_vector.cxx"))
    if include_expression:
        cxxcode = cxxcode + read_file(os.path.join(imp_path, "expression.cxx"))
        if advanced_expression:
            cxxcode = cxxcode + read_file(os.path.join(imp_path, "advancedexpression.cxx"))
    else:
        cxxcode = cxxcode + read_file(os.path.join(imp_path, "noexpression.cxx"))
    # add factory stuff if needed
    if has_factory:
        cxxcode = cxxcode + read_file(os.path.join(imp_path, "factory.cxx"))
    # replace values like ObsName with the real values
    cxxcode = search_and_replace(cxxcode, pkgname, classname, typename)

    # return strings for both the header and C++ file
    return hcode, cxxcode

def make_link_def_entry(linkdeffile, pkgname, classname):
    """Not super sure what this does but deals with LinkDef.h files."""
    watch_include = False
    watch_pragma = False
    inclstr = '#include "'+os.path.join(pkgname, classname)+'.h"'
    pragmastr = '#pragma link C++ class '+classname+"+;"
    # check if it's already patched
    with open(linkdeffile, 'r', encoding="utf-8") as f:
        done_pragma = any(pragmastr == x.rstrip('\r\n') for x in f)
        done_include = any(inclstr == x.rstrip('\r\n') for x in f)
        # search for the 'right' place to insert it
        for line in fileinput.input(linkdeffile, inplace=1):
            if not done_include and line.lower().startswith('#include'):
                watch_include = True
            if watch_include and not line.lower().startswith('#include'):
                print(inclstr)
                watch_include = False
                done_include = True
            if not done_pragma and line.lower().startswith('#pragma link c++ class'):
                watch_pragma = True
            if watch_pragma and not line.lower().startswith('#pragma link c++ class'):
                print(pragmastr)
                watch_pragma = False
                done_pragma = True
            print(line.strip())

def find_packages(rootdir, list_of_dirs = []):
    # Exclude certain paths
    if "/." in os.path.basename(rootdir):
        # Ignore directories like /.git
        return
    if "RootCoreBin" in os.path.basename(rootdir):
        # RootCoreBin contains names of packages, but not packages themselves
        return

    # Packages must be of the directory form
    # Package_name
    # |--- Package_name (Containing .h Files)
    # |--- Root (Containing .cpp Files)
    # If we find these two folders, we assume this is a package.
    cxxdir_exists = os.path.exists(os.path.join(rootdir, os.path.basename(rootdir)))
    headdir_exists = os.path.exists(os.path.join(rootdir, "Root"))
    if cxxdir_exists and headdir_exists:
        list_of_dirs.append([os.path.basename(rootdir), rootdir])
        return

    subdirs = os.listdir(rootdir)
    for subdir in subdirs:
        # Assume a dictionary hierarchy such that all packages must have a CMakeLists.txt
        # Therefore if a directory has no CMakeLists.txt, we will ignore it and assume 
        # that is it not a package, nor does it contain packages
        if os.path.isfile(os.path.join(rootdir, subdir, "CMakeLists.txt")):
            # Recursive call. We now check if there are packages within this directory.
            # For example, CAFCore has a CMakeList.txt file in 
            # it's directory, and it contains the package "CAFxAODUtils"
            find_packages(os.path.join(rootdir,subdir), list_of_dirs)
        else:
            continue

    return list_of_dirs

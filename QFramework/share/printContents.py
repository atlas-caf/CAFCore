#!/bin/env python

def main(args):
    import QFramework
    sf = QFramework.TQSampleFolder.loadSampleFolder(args.infile+":"+args.name)
    sf.printListOfCounters()
    sf.printListOfHistograms()

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="print contents of a sample folder")
    parser.add_argument("infile",help="path to the file")
    parser.add_argument("--name",default="samples",help="name of the sample folder in the file")
    args = parser.parse_args()
    main(args)

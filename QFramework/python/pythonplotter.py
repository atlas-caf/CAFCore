class Plotter(object):
    """
    Duck-typed hepdata plotter
    """
    
    def __init__(self, samples):
        """Create a new plotter object"""
        from QFramework import TQTaggable
        self.tags = TQTaggable()
        self.samples = samples
        self.clearProcesses()

    def setScheme(self, hist_scheme):
        """Set histogram scheme"""
        #  not actually used
        self.hist_scheme = hist_scheme

    def getReader(self):
        return self.samples

    def clearProcesses(self):
        """Clear internal list of processes"""
        from collections import OrderedDict
        self.processes = OrderedDict()

    def printProcesses(self):
        """Print currently known processes"""
        for name,process in self.processes.items():
            print(name,process.exportTagsAsString())

    def importTags(self,t):
        self.tags.importTags(t)
        
    def importProcessesFromFile(self, hist_processes):
        """Read the process definition file"""
        hist_processes = str(hist_processes)
        with open(hist_processes) as process_file:
            for i, line in enumerate(process_file):
                line = line.strip()
                if not line or line.startswith("#"):
                    # Skip empty lines
                    continue
                from QFramework import TQTaggable
                tags = TQTaggable()
                tags.importTags(line)
                name = tags.getTagStringDefault(".name", tags.getTagStringDefault(".path", "p%d" % i))
                path = tags.getTagStringDefault(".path", "--")
                if str(path) in ["|", "||"]:
                    continue
                name = str(name)
                self.processes[name] = tags
        return True

    def importScheme(self, folder):
        """Read the process definitios"""
        processID = 0
        while folder.hasTagString(".processes.{:d}..name".format(processID)):
            from QFramework import TQTaggable
            tags = TQTaggable()
            tags.importTagsWithoutPrefix(folder,".processes.{:d}.".format(processID))
            name = tags.getTagStandardStringDefault(".name","")
            self.processes[name] = tags
            processID += 1
        return True    

    def getProcessPath(self, process_name):
        """Return the path of a process"""
        return self.processes[process_name].getTagStringDefault(".path", "")

    @staticmethod
    def get_x_label(th1):
        import re
        x_label = th1.GetXaxis().GetTitle()
        x_label = str(x_label)
        match = re.search("^(.*) \[([^\]]+)\]$", x_label)
        if match:
            variable = match.group(1)
            unit = match.group(2)
        else:
            variable = x_label
            unit = None
        return variable,unit
    
    def get_processes_with_tag(self, tag_name):
        """Return ordered dict of processes which have the given tag"""
        from collections import OrderedDict
        return OrderedDict(
            (name, process) for name, process in self.processes.items()
            if process.getTagDefault(tag_name, False)
        )

    def get_process_color(self, process, cut_histname, tags):
        """Extract the color of a process"""
        # Check if set explicitly in process file
        fill_color = str(process.getTagDefault("histFillColor", ""))
        line_color = str(process.getTagDefault("histLineColor", ""))
        if fill_color or line_color:
            return self.resolve_color(fill_color or line_color)

        # Use default fall-back from style tags in sample folder
        path = process.getTagDefault(".path", "/")
        path = self.resolve_path(path, tags)
        th1 = self.samples.getHistogram(path, cut_histname)
        fill_color = th1.GetFillColor()
        line_color = th1.GetLineColor()
        color = fill_color or line_color or None

        return self.resolve_color(color)

    @staticmethod
    def resolve_color(color):
        """Convert ROOT colors (kRed+1) to HTML string"""
        if not color:
            return None
        import ROOT
        if isinstance(color, int):
            value = color
        else:
            import re
            tokens = re.split("([+-])", color)
            value = 0
            mode = 1
            for token in tokens:
                if token == "+":
                    continue
                if token == "-":
                    mode *= -1
                    continue
                try:
                    token = int(token)
                    value += mode * token
                    mode = 1
                    continue
                except ValueError:
                    pass
                value += mode * getattr(ROOT, token.strip())
                mode = 1

        color = ROOT.gROOT.GetColor(value) 
        if not color:
            return None
        red = color.GetRed()
        green = color.GetGreen()
        blue = color.GetBlue()

        rgb = [red, green, blue]
        rgb = [int(255 * c) for c in rgb]
        rgb = ["%02x" % c for c in rgb]
        rgb = "".join(rgb)
        return "#" + rgb

    @staticmethod
    def extract(th1, accessor, scale=1., extraBins=False):
        bin_count = th1.GetNbinsX()
        return [accessor(th1)(i+1-extraBins) * scale for i in range(bin_count + 2*extraBins)]
    
    def process2yield(self, process, cut_hist, tags, extraBins=True):
        """Exract the histogram from the sample folder and convert to Yield"""

        path = process.getTagDefault(".path", ".")
        scale = process.getTagDefault("scale", 1.0)

        path = self.resolve_path(path, tags)
        base_th1 = self.samples.getHistogram(path, cut_hist)

        base = self.extract(base_th1, lambda x: x.GetBinContent, scale=scale, extraBins=extraBins)
        stat = self.extract(base_th1, lambda x: x.GetBinError, scale=scale, extraBins=extraBins)

        return {"base":base, tags.getTagStandardStringDefault("errorType","stat"):stat}

    def resolve_path(self, path, tags):
        """Replace tags and aliases in the path"""
        from QFramework import TQTaggable        
        aliases = TQTaggable()
        aliases.setTagString("lepch", tags.getTagStringDefault("input.lepch", "?"))
        aliases.setTagString("channel", tags.getTagStringDefault("input.channel", "?"))
        aliases.setTagString("eatachannel", tags.getTagStringDefault("input.datachannel", "?"))
        aliases.importTagsWithoutPrefix(tags, "alias.")
        aliases.importTagsWithoutPrefix(tags, "input.")

        return aliases.replaceInText(path)


    @staticmethod
    def tex(text):
        """Convert ROOT pseudo-tex to actual tex"""
        text = str(text)
        text = text.replace("#it", "").replace("#", "\\")
        if set("\\{}^_") & set(text) and "$" not in text:
            return "$%s$" % text
        return text

    def process2stack(self, processes, cut_histname, tags, bartype="stepfilled"):
        """Convert a dict of processes into a stack and return the stack dict"""
        return {
            "type": bartype,
            "error": "stat",
            "content": [
                {
                    "yield": [str(process_name)],
                    "label": self.tex(process_name),
                    "style": {
                        "color": self.get_process_color(process, cut_histname, tags)
                    }
                }
                for process_name, process in processes.items()
            ]
        }

    @staticmethod
    def check_dict_types(self,d):
        if type(d) != dict:
            print(d,type(d))
            return
        for k,v in d.items():
            if type(v) == dict:
                print(k,type(k))
                self.check_dict_types(v)
            elif type(v) == list:
                print(k,type(k))
                self.check_dict_types(v)
            else:
                print(k,type(k),type(v))
    
    @staticmethod
    def get_th1_edges(th1):
        """Extract and return yields (incl. under/over flow) from TH1"""
        x_axis = th1.GetXaxis()
        n_bins = x_axis.GetNbins()
        return [x_axis.GetBinLowEdge(i + 1) for i in range(n_bins)] \
               + [x_axis.GetBinUpEdge(n_bins)]

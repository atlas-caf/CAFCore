
# uhepp plotter -- should be separate module
import re
import os
import json
from datetime import datetime

DEFAULT_API = "https://uhepp.org/api/"

REQUESTS_AVAILABLE = True
try:
    import requests
except ImportError:
    print("WARNING: Could not import requests library. Plots cannot be pushed.")
    REQUESTS_AVAILABLE = False
    pass

class ThinProcess:
    """Thin wrapper to provide a process name"""
    def __init__(self, name):
        self.name = name
    def GetName(self):
        return self.name

from QFramework.pythonplotter import Plotter as BasePlotter
    
class Plotter(BasePlotter):
    """
    Duck-typed uhepp plotter
    
    To use this plotter and push plot to a central hub, add the following
    lines to the visualize config.

    ```
    plotFormats: json
    plotter: uhepp
    # Collection id, e.g. 1
    plotter.uhepp.collection: 1
    ```

    The environment variables UHEPP_API and UHEPP_TOKEN are honored.
    """
    def __init__(self, samples):
        """Create a new plotter object"""
        super(Plotter, self).__init__(samples)

    def setNormalizationInfo(self, path_to_normalization):
        """Set normalization info"""
        #  not actually used
        self.path_to_normalization = path_to_normalization

    def loadSystematics(self, systematics_band):
        """Set systematics band"""
        # not used at the moment
        self.systematics_band = systematics_band

    def setScheme(self, hist_scheme):
        """Set histogram scheme"""
        #  not actually used
        self.hist_scheme = hist_scheme

    def removeProcessesByName(self, pattern):
        """Not yet supported"""

    def getListOfProcessNames(self):
        """Return a list of process objects"""
        return [ThinProcess(name) for name in self.processes]

    def getSystematics(self, channel):
        """Not supported"""
        return False

    @staticmethod
    def get_th1_edges(th1):
        """Extract and return yields (incl. under/over flow) from TH1"""
        x_axis = th1.GetXaxis()
        n_bins = x_axis.GetNbins()
        return [x_axis.GetBinLowEdge(i + 1) for i in range(n_bins)] \
               + [x_axis.GetBinUpEdge(n_bins)]

    @staticmethod
    def tex(text):
        """Convert ROOT pseudo-tex to actual tex"""
        text = str(text)
        text = text.replace("#it", "").replace("#", "\\")
        if set("\\{}^_") & set(text) and "$" not in text:
            return "$%s$" % text
        return text

    def plotAndSaveAs(self, cut_histname, file_path, t):
        """Create uhepp JSON file and push it to central hub"""
        from QFramework import TQTaggable
        tags = TQTaggable()
        tags.importTags(t)
        tags.importTags(self.tags)
        
        cut_name, histname = cut_histname.split("/")
        random_process = next(iter(self.processes.values()))
        random_path = random_process.getTagDefault(".path", ".")
        random_path = self.resolve_path(random_path, tags)
        random_th1 = self.samples.getHistogram(random_path, cut_histname)
        x_label = random_th1.GetXaxis().GetTitle()
        x_label = str(x_label)
        match = re.search("^(.*) \[([^\]]+)\]$", x_label)
        if match:
            variable = match.group(1)
            unit = match.group(2)
        else:
            variable = x_label
            unit = None

        # Build stacks
        stacks = []

        bkg = self.get_processes_with_tag(".isBackground")
        yields = {}
        if bkg:
            stacks.append(self.process2stack(bkg, cut_histname, tags))
            for process_name, process in bkg.items():
                yields[str(process_name)] = self.process2yield(process, cut_histname, tags)

        sig = self.get_processes_with_tag(".isSignal")
        if sig:
            stacks.append(self.process2stack(sig, cut_histname, tags, bartype="step"))
            for process_name, process in sig.items():
                yields[str(process_name)] = self.process2yield(process, cut_histname, tags)

        data = self.get_processes_with_tag(".isData")
        if data:
            stacks.append(self.process2stack(data, cut_histname, tags, bartype="points"))
            for process_name, process in data.items():
                yields[str(process_name)] = self.process2yield(process, cut_histname, tags)

        filename = []

        channel = str(tags.getTagDefault("input.channel", ""))
        if channel:
            filename.append(channel)

        filename.append(str(cut_name))
        filename.append(str(histname))

        data_obj = {
            "version": "0.1",
            "type": "histogram",
            "metadata": {
                "date": datetime.now().isoformat(),
                "filename": "-".join(filename),
                "producer": "CAF",
                "event_selection": str(cut_name),
                "tags": {
                    "cut": str(cut_name),
                    "channel": channel,
                }
            },
            "badge": {
                "brand": "ATLAS",
                "label": str(tags.getTagDefault("labels.atlas", "")),
                "subtext": self.tex(tags.getTagDefault("labels.process", "")),
            },
            "variable": {
                "symbol": self.tex(variable),
                "log": tags.getTagDefault("style.logScale", False)
            },
            "bins": {
                "edges": self.get_th1_edges(random_th1),
                "include_underflow": tags.getTagDefault("style.showUnderflow", False),
                "include_overflow": tags.getTagDefault("style.showOverflow", False),
            },
            "y_axis": {
                "log": tags.getTagDefault("style.logScale", False)
            },
            "stacks": stacks,
            "yields": yields,
            "ratio": [],
        }
        if unit:
            data_obj["variable"]["unit"] = unit

        if (tags.getTagDefault("style.showRatio", True) or "ratio" in tags.getTagVString("style.subPlot")) and data and (sig or bkg):
            data_obj["ratio"] = [
               {
                   "type": "points",
                   "error": "stat",
                   "numerator": [str(name) for name in data],
                   "denominator": [str(name) for name in sig] + [str(name) for name in bkg],
               }
            ]
            data_obj["ratio_axis"] = {
                   "label": "Data / SM"
                }
            if tags.getTagDefault("style.forceRatioLimits", False):
                data_obj["ratio_axis"] = {
                   "min": tags.getTagDefault("style.ratioMin", 0.4),
                   "max": tags.getTagDefault("style.ratioMax", 1.6),
                }

            if tags.hasTag("geometry.sub.height"):
                data_obj["layout"] = {
                    "ratio_fraction": tags.getTagDefault("geometry.sub.height", 0.25)
                }

        # Write to file
        with open(file_path, "w") as file_obj:
            json.dump(data_obj, file_obj)
        
        #@env: [UHEPP_API] address of the UHEPP API, defaults to https://uhepp.org/api/
        api_url = str(tags.getTagDefault(
            "uhepp.api",
            os.environ.get("UHEPP_API", DEFAULT_API))
        )
        #@env: [UHEPP_TOKEN] token to authenticate against the UHEPP API
        api_key = os.environ.get("UHEPP_TOKEN", None)
        collection_id = str(tags.getTagDefault("uhepp.collection", ""))

        if collection_id:
            from QFramework import WARN, INFO
            try:
                response = self.push(data_obj, api_url, api_key, collection_id)
                if response.ok:
                    links = requests.utils.parse_header_links(response.headers["Link"])
                    links = [link for link in links if link["rel"] == "ui"]
                    if links:
                        url = links[0]["url"]
                    else:
                        url = api_url

                    INFO("  pushed to %s" % url)
                else:
                    status_code = response.status_code
                    error = str(response.text )
                    WARN("  pushing failed (status code %d): %s" % (status_code, error))
            except Exception as e:
                WARN("cannot push plots: %s" % (e.message))

        return True

    @staticmethod
    def push(data, api_url, api_key, collection_id):
        """Push data to central hub"""
        headers = {}
        if api_key is not None:
            headers["Authorization"] = "Token " + api_key

        post_data = {
            'uhepp': data,
            'collection': api_url + "collections/" + collection_id + "/"
        }

        if REQUESTS_AVAILABLE:
            response = requests.post(api_url + "plots/",
                                    json=post_data,
                                    headers=headers)
        else:
            raise Exception("cannot push plots because python 'requests' package is not available")

        return response
    

from QFramework.pythonplotter import Plotter as BasePlotter

def get_edges(th1):
    return BasePlotter.get_th1_edges(th1)

def get_yields(th1):
    return BasePlotter.extract(th1, lambda x: x.GetBinContent, scale=1, extraBins=False)

def get_errors(th1):
    return BasePlotter.extract(th1, lambda x: x.GetBinError, scale=1, extraBins=False)

def get_arrays(histograms):
    return [( h.GetTitle(), get_yields_and_errors(h) ) for h in histograms if h]
        
def get_yields_and_errors(th1):
    return { "base":get_yields(th1),"stat":get_errors(th1) }

def arrays2yaml(variable,unit,edges,arrays,yunit=None):
    json = {
        "independent_variables": [
            {
                "header":{"name": variable},
                "values":[
                    {"low":edges[i],"high":edges[i+1]} for i in range(0,len(edges)-1)
                ]
            }
        ],
        "dependent_variables":[
            {
                "header":{"name":name},
                "values":[{"value":elem["base"][i],"errors":[ { "symerror": elem[k][i] } for k in ["stat"] if elem[k][i]>0 ] } for i in range(0,len(edges)-1)]
            } for name,elem in arrays
        ]
    }
    for v in json["independent_variables"]:    
        if unit:
            v["header"]["units"] = unit
    for v in json["dependent_variables"]:
        if yunit:
            v["header"]["units"] = yunit
        elif unit:
            v["header"]["units"] = "Events / "+str((edges[len(edges)-1] - edges[len(edges)-2]))+" "+unit
    return json

def hists2yaml(histograms):
    variable,unit = BasePlotter.get_x_label(histograms[0])
    if not variable: variable="X"
    if not unit: unit = "Unknown"
    edges = get_edges(histograms[0])
    arrays = get_arrays(histograms)
    return arrays2yaml(variable,unit,edges,arrays)   

class Plotter(BasePlotter):
    """
    Duck-typed HEPdata plotter
    """

    def __init__(self, samples, folder="hepdata"):
        """Create a new plotter object"""
        BasePlotter.__init__(self, samples)
        self.folder = folder
        from CommonAnalysisHelpers.common import mkdir
        mkdir(self.folder)
        self.elements = []
    
    def plotAndSaveAs(self, cut_histname, file_path, t):
        """Create uhepp JSON file and push it to central hub"""
        from QFramework import TQTaggable
        tags = TQTaggable()
        tags.importTags(t)
        tags.importTags(self.tags)
        
        cut_name, histname = cut_histname.split("/")
        random_process = next(iter(self.processes.values()))
        random_path = random_process.getTagDefault(".path", ".")
        random_path = self.resolve_path(random_path, tags)
        random_th1 = self.samples.getHistogram(random_path, cut_histname)

        variable,unit = BasePlotter.get_x_label(random_th1)
        yunit = random_th1.GetYaxis().GetTitle()
        edges = self.get_th1_edges(random_th1)

        arrays = [ (str(self.processes[name].getTagStringDefault(".title","")),self.process2yield(process,cut_histname,tags,extraBins=False)) for name,process in self.processes.items() ]

        filename = file_path+".yaml"
        element = {
            "name":tags.getTagStandardStringDefault("name","TODO"),
            "description":tags.getTagStandardStringDefault("description","TODO"),
            "data_file":filename,
            "keywords":[{"name":"cmenergies","values":[tags.getTagDoubleDefault("ecm",13000)]}]
        }

        data_obj = arrays2yaml(variable,unit,edges,arrays,yunit)

        import yaml
        from os.path import join as pjoin
        with open(pjoin(self.folder,filename), "w") as outfile:
            yaml.dump(data_obj, outfile)        

        self.elements.append(element)
            
        return True

    def write_submission(self):

        import yaml
        from os.path import join as pjoin
        with open(pjoin(self.folder,"submission.yaml"), "w") as outfile:
            yaml.dump_all(self.elements, outfile)                
    

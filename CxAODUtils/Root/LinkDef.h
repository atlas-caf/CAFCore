//this file is -*- c++ -*-
#ifndef __CxAODutilsDICT__
#define __CxAODutilsDICT__

#include "CxAODUtils/CxAODReaderAlgorithm.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ class CxAODReaderAlgorithm+;
#pragma link C++ class CxAODSelectors::AcceptanceSelector+;
#pragma link C++ class CxAODSelectors::PassThrough+;
#pragma link C++ typedef CxAODSelectors::IntAccessor+;
#pragma link C++ typedef CxAODSelectors::CharAccessor+;

#endif

#endif

